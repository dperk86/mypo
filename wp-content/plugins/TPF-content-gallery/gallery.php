<?php

    // Get Post IDs entered in from the options page
    $postID1 = get_option('postID1');
    $postID2 = get_option('postID2');
    $postID3 = get_option('postID3');
    $postID4 = get_option('postID4');
    $postID5 = get_option('postID5');
    $postID6 = get_option('postID6');

    $postIDs = array($postID1, $postID2, $postID3, $postID4, $postID5, $postID6);

?>

<div id="iview">

        <?php

            foreach($postIDs as $postID){

                if( !($postID == '') ){

                        if( has_post_thumbnail( $postID ) ){

                            $fullSrc = wp_get_attachment_image_src( get_post_thumbnail_id($postID), 'full', false, '' );

                            echo '<figure data-iview:image="' . $fullSrc[0]. '" data-iview:transition="slice-top-fade,slice-right-fade">';

                                echo '<figcaption class="iview-caption caption' . $postID . '" data-x="450" data-y="100">';
                                echo '<h2>' . get_the_title( $postID ) . '</h2>';

                                echo '<a href="' . get_permalink( $postID ) . '" class="button">';

                                        if( get_post_type( $postID ) == 'project' ){

                                            echo 'Learn more about this project <span>&raquo;</span>';

                                        } elseif( get_post_type( $postID ) == 'post' ){

                                            echo 'Read more <span>&raquo;</span>';

                                        } elseif( get_post_type( $postID ) == 'page' ){

                                            echo 'Learn more <span>&raquo;</span>';

                                        } elseif( get_post_type( $postID ) == 'newsletter' ){

                                            echo 'Read the latest newsletter <span>&raquo;</span>';

                                        } else {

                                            echo 'Click to read <span>&raquo;</span>';

                                        }

                                echo '</a>';

                                echo '</figcaption>';

                            echo '</figure>';

                        } elseif( !( has_post_thumbnail( $postID ) ) ){

                            echo '<figure data-iview:image="' . plugins_url('TPF-content-gallery'). '/library/images/logo_blue.png'. '" data-iview:transition="slice-top-fade,slice-right-fade">';

                            echo '<figcaption class="iview-caption caption' . $postID . '" data-x="450" data-y="100">';
                                echo '<h2>' . get_the_title( $postID ) . '</h2>';

                                echo '<a href="' . get_permalink( $postID ) . '" class="button">';

                                    if( get_post_type( $postID ) == 'project' ){

                                        echo 'Learn more about this project <span>&raquo;</span>';

                                    } elseif( get_post_type( $postID ) == 'post' ){

                                        echo 'Read more <span>&raquo;</span>';

                                    } elseif( get_post_type( $postID ) == 'page' ){

                                        echo 'Get Started <span>&raquo;</span>';

                                    } elseif( get_post_type( $postID ) == 'newsletter' ){

                                        echo 'Read the latest newsletter <span>&raquo;</span>';

                                    } else {

                                        echo 'Click to read <span>&raquo;</span>';

                                    }


                                echo '</a>';

                            echo '</figcaption>';

                            echo '</figure>';

                        }

                } // End if statement

            } // End foreach

        ?>

</div><!-- end #iview -->
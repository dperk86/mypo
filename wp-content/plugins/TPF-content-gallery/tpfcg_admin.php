<?php

    if($_POST['action'] == 'post') {

        //Form data sent
        $postID1 = $_POST['postID1'];
        update_option('postID1', $postID1);

        $postID2 = $_POST['postID2'];
        update_option('postID2', $postID2);

        $postID3 = $_POST['postID3'];
        update_option('postID3', $postID3);

        $postID4 = $_POST['postID4'];
        update_option('postID4', $postID4);

        $postID5 = $_POST['postID5'];
        update_option('postID5', $postID5);

        $postID6 = $_POST['postID6'];
        update_option('postID6', $postID6);

?>
    <div class="updated">
        <p>
            <strong>
                <?php _e('Options saved.' ); ?>
            </strong>
        </p>
    </div><!-- end .updated -->

<?php

    } else {

        //Normal page display
        $postID1 = get_option('postID1');
        $postID2 = get_option('postID2');
        $postID3 = get_option('postID3');
        $postID4 = get_option('postID4');
        $postID5 = get_option('postID5');
        $postID6 = get_option('postID6');

    }


?>


    <div class="wrap">
       <h2>TPF Content Gallery Settings</h2>
    </div><!-- end .wrap -->

    <p>
        <form id="tpfcg_form" name="tpfcg_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

            <input type="hidden" name="action" value="post" />

            <label>Post, Page, or Project ID Numbers:<br/>(Only 6 slots available)</label>

            <br/>
            <br/>

            <input type="text" name="postID1" id="postID1" class="postID" value="<?php echo $postID1; ?>" size="10" />
            <br/>
            <input type="text" name="postID2" id="postID2" class="postID" value="<?php echo $postID2; ?>" size="10" />
            <br/>
            <input type="text" name="postID3" id="postID3" class="postID" value="<?php echo $postID3; ?>" size="10" />
            <br/>
            <input type="text" name="postID4" id="postID4" class="postID" value="<?php echo $postID4; ?>" size="10" />
            <br/>
            <input type="text" name="postID5" id="postID5" class="postID" value="<?php echo $postID5; ?>" size="10" />
            <br/>
            <input type="text" name="postID6" id="postID6" class="postID" value="<?php echo $postID6; ?>" size="10" />

            <br/>
            <br/>

            <input type="submit" name="Submit" value="<?php _e('Update Options') ?>" />

        </form><!-- end #postIDs -->
    </p>
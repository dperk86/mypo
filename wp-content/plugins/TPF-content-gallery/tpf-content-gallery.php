<?php

/* ========================================================================================================================

    Plugin Name: TPF Content Gallery
    Plugin URI: http://dev.tpfredesign.com/
    Description: Turkish Philanthropy Funds content gallery that will appear on the home page.
    Author: Elif Bayrasli (Media Hive)
    Version: 1.0
    Author URI: http://mediahive.com

	======================================================================================================================== */


    /* The Slider */

    function tpf_content_gallery(){
        include('gallery.php');
    }



/* ************************************************** */


    /* The Styles and Scripts */

    function gallery_styles() {

        wp_register_style( 'tpf-content-gallery-style', plugins_url('TPF-content-gallery'). '/library/styles/tpf-content-gallery.css', false, '1.0', 'screen' );
        wp_enqueue_style( 'tpf-content-gallery-style' );

    }

    function gallery_scripts() {
        /* The next lines figures out where the javascripts and images and CSS are installed,
    relative to your wordpress server's root: */

        wp_register_script( 'tpf-content-gallery-script', plugins_url('TPF-content-gallery').'/library/js/tpf-content-gallery.js', array('jquery', 'plugins', 'script'), '1.0', true );
        wp_enqueue_script( 'tpf-content-gallery-script' );

        wp_register_script( 'tpf-content-gallery-script_api', plugins_url('TPF-content-gallery').'/library/js/tpf-content-gallery_api.js', array('jquery_google_api', 'plugins', 'script'), '1.0', true );
        wp_enqueue_script( 'tpf-content-gallery-script_api' );

    }


/* ************************************************** */


    /* The MetaBox */



/* ************************************************** */


    /* The Options Page */

    add_action('admin_menu', 'TPFCG_admin_actions');


    function TPFCG_admin_actions(){
        add_options_page('TPF Content Gallery Options', 'TPF Content Gallery', 'manage_options', 'tpf-content-gallery-settings', 'TPFCG_admin');
    }

    function TPFCG_admin(){

        include('tpfcg_admin.php');

    }


    add_action( 'init', 'gallery_styles' );
    add_action( 'init', 'gallery_scripts' );


?>
<?php
/*
Plugin Name: Footer Contact Widget
Plugin URI: 
Description: TPF Contact and Social Media info for the footer
Author: Patrick Murray
Version: 1
Author URI:
*/
 
 
class FooterContactWidget extends WP_Widget
{
  function FooterContactWidget()
  {
    $widget_ops = array('classname' => 'FooterContactWidget', 'description' => 'Displays contact/social media info in the footer' );
    $this->WP_Widget('FooterContactWidget', 'Contact/Social Media Info', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array('facebookURL' => '',  'twitterURL' => '',  'linkedinURL' => '',  'youtubeURL' => '',  'contactus' => ''  ) );
    $facebookURL = $instance['facebookURL'];
    $twitterURL = $instance['twitterURL'];
    $linkedinURL = $instance['linkedinURL'];
    $youtubeURL = $instance['youtubeURL'];
    $contactus = $instance['contactus'];
       
?>
  <p><label for="<?php echo $this->get_field_id('facebookURL'); ?>">Facebook URL: <input class="widefat" id="<?php echo $this->get_field_id('facebookURL'); ?>" name="<?php echo $this->get_field_name('facebookURL'); ?>" type="text" value="<?php echo esc_attr($facebookURL); ?>" /></label></p>
  <p><label for="<?php echo $this->get_field_id('twitterURL'); ?>">Twitter URL: <input class="widefat" id="<?php echo $this->get_field_id('twitterURL'); ?>" name="<?php echo $this->get_field_name('twitterURL'); ?>" type="text" value="<?php echo esc_attr($twitterURL); ?>" /></label></p>
  <p><label for="<?php echo $this->get_field_id('linkedinURL'); ?>">LinkedIn URL: <input class="widefat" id="<?php echo $this->get_field_id('linkedinURL'); ?>" name="<?php echo $this->get_field_name('linkedinURL'); ?>" type="text" value="<?php echo esc_attr($linkedinURL); ?>" /></label></p>
  <p><label for="<?php echo $this->get_field_id('youtubeURL'); ?>">Youtube URL: <input class="widefat" id="<?php echo $this->get_field_id('youtubeURL'); ?>" name="<?php echo $this->get_field_name('youtubeURL'); ?>" type="text" value="<?php echo esc_attr($youtubeURL); ?>" /></label></p>
  <p><label for="<?php echo $this->get_field_id('contactus'); ?>">Contact Us Text: <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('contactus'); ?>" name="<?php echo $this->get_field_name('contactus'); ?>" type="text" /><?php echo $contactus ?></textarea></p>

<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['facebookURL'] = $new_instance['facebookURL'];
    $instance['twitterURL'] = $new_instance['twitterURL'];
    $instance['linkedinURL'] = $new_instance['linkedinURL'];
    $instance['youtubeURL'] = $new_instance['youtubeURL'];
    $instance['contactus'] = $new_instance['contactus'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    $facebookURL = esc_attr($instance['facebookURL']);
    $twitterURL = esc_attr($instance['twitterURL']);
    $linkedinURL = esc_attr($instance['linkedinURL']);
    $youtubeURL = esc_attr($instance['youtubeURL']);
    $contactus = $instance['contactus'];
 
    echo '<div id="footer_widget">';
        echo '<h3>FIND US ON</h3>';
        echo '<div id="contact_badges">';
        
            if (!empty($facebookURL))
                echo '<a href="'.$facebookURL.'"><img src="'.get_bloginfo('template_directory').'/library/images/fb_badge.png" /></a>';

            if (!empty($twitterURL))
                echo '<a href="'.$twitterURL.'"><img src="'.get_bloginfo('template_directory').'/library/images/tw_badge.png" /></a>';

            if (!empty($linkedinURL))
                echo '<a href="'.$linkedinURL.'"><img src="'.get_bloginfo('template_directory').'/library/images/li_badge.png" /></a>';

            if (!empty($youtubeURL))
                echo '<a href="'.$youtubeURL.'"><img src="'.get_bloginfo('template_directory').'/library/images/yt_badge.png" /></a>';

        echo '</div>';
        echo '<h3>CONTACT US</h3>';
        echo '<div id="contact_us">';
            
            if (!empty($contactus))
                echo $contactus;
            
        echo '</div>';            
   echo '</div>';
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("FooterContactWidget");') );?>
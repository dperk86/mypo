<?php
/* ========================================================================================================================

    Plugin Name: TPF Social Media Plugin
    Plugin URI: http://tpfund.org
    Description: Turkish Philanthropy Funds social media content will appear on the social media page.
    Author: Elif Bayrasli (Media Hive)
    Version: 1.0
    Author URI: http://mediahive.com

	======================================================================================================================== */

    function tpf_social_media(){
        include('social-media.php');
    }

    function sm_styles() {

        wp_register_style( 'tpf-socialmedia-style', plugins_url('TPF-socialmedia-plugin'). '/styles/tpf-socialmedia.css', false, '1.0', 'screen' );
        wp_enqueue_style( 'tpf-socialmedia-style' );

    }

    /* The Options Page */

    add_action('admin_menu', 'TPFSM_admin_actions');


    function TPFSM_admin_actions(){
        add_options_page('TPF Social Media Options', 'TPF Social Media', 'manage_options', 'tpf-social-media-settings', 'TPFSM_admin');
    }

    function TPFSM_admin(){

        include('tpfsm_admin.php');

    }

    add_action( 'init', 'sm_styles' );


?>
<?php

    // Get SM info entered in from the options page
    $twitterUsername      = get_option('twitterUsername');
    $twitterLimit         = get_option('twitterLimit');
    $facebookUsername     = get_option('facebookUsername');
    $facebookLimit        = get_option('facebookLimit');
    $instagramUsername    = get_option('instagramUsername');
    $instagramAccessToken = get_option('instagramAccessToken');
    $instagramLimit       = get_option('instagramLimit');

    if(!$twitterLimit){
        $twitterLimit = 6;
    }
    if(!$facebookLimit){
        $facebookLimit = 6;
    }
    if(!$instagramLimit){
        $instagramLimit = 6;
    }

?>

<section id="social-feed">

    <?php

        if($twitterUsername){

            $username = $twitterUsername;
            $url      = '1.1/statuses/user_timeline';

            require('inc/tmhOAuth.php');
            require('inc/tmhUtilities.php');

            $tmhOAuth = new tmhOAuth(array(
                'consumer_key'    => 'HMdXmzhou76gXp2T7lesOw',
                'consumer_secret' => 'NnANQDNIAgMZjR0k6nls40zCHzEvNmMD0TKjnljWbLA',
                'user_token'      => '255159271-85gnVC5qmhydpIHp5GScftQmCmKsDo1f7sui7zex',
                'user_secret'     => 'sRSNO5q9i6tVLyXKA7Z4C0Gvk5MDJQlBagKocydpw',
            ));

            $code = $tmhOAuth->request('GET', $tmhOAuth->url($url), array(
                'screen_name' => $username));
            $response = $tmhOAuth->response['response'];

            $data = json_decode($response);

            if($data){

                for( $i = 0; $i < $twitterLimit; $i++ ){

                    $text = $data[$i]->text;
                    $date = $data[$i]->created_at;

                    $text = preg_replace('/(http:\/\/|(www\.))(([^\s<]{4,68})[^\s<]*)/', '<a href="$0" rel="external"  target="_blank">$0</a>', $text);
                    $text = preg_replace('/@(\w+)/i', '<a href="http://twitter.com/$1" rel="external"  target="_blank">$0</a>', $text);
                    $text = preg_replace('/#([a-z_0-9]+)/', '<a href="http://twitter.com/search/$1" rel="external"  target="_blank">$0</a>', $text);

                    $profileImg = $data[$i]->user->profile_image_url;

                    echo '<article class="twitter post">';

                        echo '<header>';
                            echo '<a href="http://www.twitter.com/' .$username. '" target="_blank"><img src="' .$profileImg. '" /></a> ';
                            echo '<a href="http://www.twitter.com/' .$username. '" target="_blank">@' .$twitterUsername. '</a>';
                        echo '</header>';
                        echo '<p>';
                            echo $text;
                        echo '</p>';
                        echo '<footer>';
                            echo '<p>' .substr($date, 0, 10). '</p>';
                            echo '<a href="http://www.twitter.com/' .$username. '" target="_blank">';
                                echo '<img src="' .plugins_url('TPF-socialmedia-plugin'). '/images/twitter-bird-light-bgs.png" width="35" height="35" /> ';
                            echo '</a>';
                        echo '</footer>';
                    echo '</article>';

                }

            } else {

                echo '<article class="post">';
                    echo '<p>';
                        echo 'Twitter is down. Come back soon to see the latest tweets from <a href="http://twitter.com/' .$username. '" target="_blank">' .'@' .$username. '</a>.';
                    echo '</p>';
                    echo '<footer>';
                        echo '<a href="http://www.twitter.com/' .$username. '" target="_blank">';
                            echo '<img src="' .plugins_url('TPF-socialmedia-plugin'). '/images/twitter-bird-light-bgs.png" width="35" height="35" /> ';
                        echo '</a>';
                    echo '</footer>';
                echo '</article>';

            }

        }

        if($facebookUsername){

            require_once('inc/src/facebook.php');

            $config = array();
            $config['appId'] = '254344718024732';
            $config['secret'] = '6040db9c528f6a9f6d3a3b388946254c';

            $facebook = new Facebook($config);
            $accessToken = $facebook->getAccessToken();
            $user = $facebook->getUser();
            $fbPosts = $facebook->api('/' .$facebookUsername. '/posts?limit=' .$facebookLimit. '&access_token=' .$accessToken,'GET');

            if($fbPosts){

                foreach($fbPosts['data'] as $post){


                    if( isset($post['message']) ){

                        $message = $post['message'];
                        $message = preg_replace('/(http:\/\/|(www\.))(([^\s<]{4,68})[^\s<]*)/', '<a href="$0" rel="external"  target="_blank">$0</a>', $message);

                        $description = '';
                        if(isset($post['description'])){
                            $description = $post['description'];
                            $description = preg_replace('/(http:\/\/|(www\.))(([^\s<]{4,68})[^\s<]*)/', '<a href="$0" rel="external"  target="_blank">$0</a>', $description);
                        }

                        $timestamp = strtotime($post['created_time']);

                        echo '<article class="facebook post">';

                            echo '<p>';
                                echo $message;
                            echo '</p>';

                            if( $post['type'] === 'status' ){

                            } else if( $post['type'] === 'photo' ){

                                if(isset($post['picture'])){
                                    echo '<figure>';
                                        echo '<a href="' .$post['link']. '" target="_blank" rel="facebook">';
                                            echo '<img src="' .$post['picture']. '" />';
                                        echo '</a>';
                                    echo '</figure>';
                                }
                                echo '<p>';
                                    echo $description;
                                echo '</p>';

                            } else if( $post['type'] === 'video' ){

                                if(isset($post['picture'])){
                                    echo '<figure>';
                                        echo '<a href="' .$post['link']. '" target="_blank" rel="facebook">';
                                            echo '<img src="' .$post['picture']. '" />';
                                        echo '</a>';
                                    echo '</figure>';
                                }

                                echo '<figcaption>';
                                    if(isset($post['caption'])){
                                        echo $post['caption'];
                                        echo '<br/>';
                                    }
                                    if(isset($post['source'])){
                                        echo $post['source'];
                                    }
                                echo '</figcaption>';

                                echo '<p>';
                                    echo $description;
                                echo '</p>';

                            }

                            echo '<footer>';
                                echo '<p>';
                                    echo date('D M d', $timestamp);
                                echo '</p>';
                                echo '<p>';
                                    echo '<a href="http://www.facebook.com/' .$facebookUsername. '" target="_blank">';
                                        echo '<img src="' .plugins_url('TPF-socialmedia-plugin'). '/images/asset.facebook.logo.lg.png" width="76" height="25" /> ';
                                    echo '</a>';
                                echo '</p>';
                            echo '</footer>';

                        echo '</article>';


                    }

                }

            } else {

                echo '<article class="post">';
                    echo '<p>';
                        echo 'The FaceBook feed is down. Come back soon to see the latest posts from Turkish Philanthropy Funds.';
                    echo '</p>';
                    echo '<footer>';
                        echo '<a href="http://www.facebook.com/' .$facebookUsername. '" target="_blank">';
                            echo '<img src="' .plugins_url('TPF-socialmedia-plugin'). '/images/asset.facebook.logo.lg.png" width="76" height="25" /> ';
                        echo '</a>';
                    echo '</footer>';
                echo '</article>';

            }
        }

        if($instagramUsername){

            function getUrlContent($url){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,  FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                $data = curl_exec($ch);
                curl_close($ch);
                return $data;
            }

            function getInstaID($username, $token){

                $username = strtolower($username); // sanitization
                $url      = 'https://api.instagram.com/v1/users/search?q=' .$username. '&access_token=' .$token;
                $get      = getUrlContent($url);
                $json     = json_decode($get);

                foreach($json->data as $user){
                    if($user->username == $username){
                        return $user->id;
                    }
                }

                return '00000000'; // return this if nothing is found
            }

            $instagramAPI  = 'https://api.instagram.com/v1/users/' .getInstaID($instagramUsername, $instagramAccessToken). '/media/recent/?count=' .$instagramLimit. '&access_token=' .$instagramAccessToken;
            $instagramGet  = getUrlContent($instagramAPI);
            $instagramData = json_decode($instagramGet);

            if($instagramData){
                foreach($instagramData->data as $data){

                    $imageURL    = $data->images->thumbnail->url;
                    $imageWidth  = $data->images->thumbnail->width;
                    $imageHeight = $data->images->thumbnail->height;
                    $fullImgURL  = $data->images->standard_resolution->url;
                    $igTimeStamp = $data->created_time;

                    echo '<article class="post instagram">';

                        echo '<figure>';
                            echo '<a href="' .$fullImgURL. '" rel="instagram">';
                                echo '<img src="' .$imageURL. '" width="' .$imageWidth. '" height="' .$imageHeight. '" />';
                            echo '</a>';
                        echo '</figure>';

                        echo '<footer>';
                            echo '<p>' .date('D M d', $igTimeStamp). '</p>';
                            echo '<a href="http://www.instagram.com/' .$instagramUsername. '" target="_blank">';
                                echo '<img src="' .plugins_url('TPF-socialmedia-plugin'). '/images/Instagram_Logo_Small.png" width="90" height="32" /> ';
                            echo '</a>';
                        echo '</footer>';

                    echo '</article>';

                }
            } else {

                echo '<article class="post">';
                    echo '<p>';
                        echo 'The Instagram feed is down. Come back soon to see the latest photos from Turkish Philanthropy Funds.';
                    echo '</p>';
                    echo '<footer>';
                        echo '<a href="http://www.instagram.com/' .$instagramUsername. '" target="_blank">';
                            echo '<img src="' .plugins_url('TPF-socialmedia-plugin'). '/images/Instagram_Logo_Small.png" width="90" height="32" /> ';
                        echo '</a>';
                    echo '</footer>';
                echo '</article>';

            }

        }


    ?>
</section><!-- end #social-feed -->
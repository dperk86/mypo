<?php

if($_POST['action'] == 'post') {

    //Form data sent
    $twitterUsername = $_POST['twitterUsername'];
    update_option('twitterUsername', $twitterUsername);

    $twitterLimit = $_POST['twitterLimit'];
    update_option('twitterLimit', $twitterLimit);

    $facebookUsername = $_POST['facebookUsername'];
    update_option('facebookUsername', $facebookUsername);

    $facebookLimit = $_POST['facebookLimit'];
    update_option('facebookLimit', $facebookLimit);

    $instagramUsername = $_POST['instagramUsername'];
    update_option('instagramUsername', $instagramUsername);

    $instagramAccessToken = $_POST['instagramAccessToken'];
    update_option('instagramAccessToken', $instagramAccessToken);

    $instagramLimit = $_POST['instagramLimit'];
    update_option('instagramLimit', $instagramLimit);


?>
    <div class="updated">
        <p>
            <strong>
                <?php _e('Options saved.' ); ?>
            </strong>
        </p>
    </div><!-- end .updated -->

<?php

    } else {

        //Normal page display
        $twitterUsername      = get_option('twitterUsername');
        $twitterLimit         = get_option('twitterLimit');
        $facebookUsername     = get_option('facebookUsername');
        $facebookLimit        = get_option('facebookLimit');
        $instagramUsername    = get_option('instagramUsername');
        $instagramAccessToken = get_option('instagramAccessToken');
        $instagramLimit       = get_option('instagramLimit');

}


?>

<div class="wrap">
    <h2>TPF Social Media Settings</h2>
</div><!-- end .wrap -->

<p>
    <form id="tpfsm_form" name="tpfsm_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

        <input type="hidden" name="action" value="post" />

        <label>Twitter Username:</label>

        <br/>
        <br/>

        <span>@</span><input type="text" name="twitterUsername" id="twitterUsername" class="twitterUsername" value="<?php echo $twitterUsername; ?>" />

        <br/>
        <br/>

        <label>Limit of Tweets (By default the limit will be set to 6 if this field is left empty):</label>

        <br/>
        <br/>

        <input type="text" name="twitterLimit" id="twitterLimit" class="twitterLimit" value="<?php echo $twitterLimit; ?>" />

        <br/>
        <br/>

        <label>Facebook Username:</label>

        <br/>
        <br/>

        <input type="text" name="facebookUsername" id="facebookUsername" class="facebookUsername" value="<?php echo $facebookUsername; ?>" />

        <br/>
        <br/>

        <label>Limit of Facebook Posts (By default the limit will be set to 6 if this field is left empty):</label>

        <br/>
        <br/>

        <input type="text" name="facebookLimit" id="facebookLimit" class="facebookLimit" value="<?php echo $facebookLimit; ?>" />

        <br/>
        <br/>

        <label>Instagram Username:</label>

        <br/>
        <br/>

        <input type="text" name="instagramUsername" id="instagramUsername" class="instagramUsername" value="<?php echo $instagramUsername; ?>" />

        <br/>
        <br/>

        <label>Instagram Access Token:</label>

        <br/>
        <br/>

        <label>Instagram Access Token:</label>

        <a href="https://instagram.com/oauth/authorize/?client_id=f62a2e664e1f4eb9a4c4ae603268af58&redirect_uri=http://www.tpfund.org/wp-admin/options-general.php?page=tpf-social-media-settings&response_type=token">Generate New Instagram Access Token</a><br/>
        <input type="text" name="instagramAccessToken" id="instagramAccessToken" class="instagramAccessToken" value="<?php echo $instagramAccessToken; ?>" />
        <script type="text/javascript">
            if(window.location.hash){
                var hash = window.location.hash.substring(14);
                document.getElementById('instagramAccessToken').value = hash;
            }
        </script>

        <br/>
        <br/>

        <label>Limit of Instagram Photos (By default the limit will be set to 6 if this field is left empty):</label>

        <br/>
        <br/>

        <input type="text" name="instagramLimit" id="instagramLimit" class="instagramLimit" value="<?php echo $instagramLimit; ?>" />

        <br/>
        <br/>

        <input type="submit" name="Submit" value="<?php _e('Update Options') ?>" />

    </form><!-- end #postIDs -->
</p>

<?php

    /*
    Plugin Name: TPF Admin Theme
    Plugin URI: http://tpfund.org/
    Description: TPF Admin Theme - Upload and Activate.
    Author: Elif Bayrasli (Media Hive)
    Version: 1.0
    Author URI: http://mediahive.com
    */


/* ========================================================================================================================

    Actions and Filters

    ======================================================================================================================== */


    // 01. Adding Admin CSS
    add_action('admin_head', 'TPF_admin_head');
    add_action('wp_head', 'TPF_admin_head');


    // 02. Adding Login CSS
    add_action('login_head', 'TPF_login_css');


    // 03. Adding Admin Favicon
    add_action('admin_head', 'admin_favicon');


    // 04. Changing Howdy Message
    add_filter('gettext', 'change_howdy', 10, 3);


    // 05. Changing Footer Text
    add_filter('admin_footer_text', 'left_admin_footer_text_output'); //left side

    // 06. Removing Search from Admin Bar
    add_action( 'wp_before_admin_bar_render', 'disable_bar_search' );

    // 07. Add to My Account Sub-Menu
    add_action( 'wp_before_admin_bar_render', 'add_my_account_links' );



/* ========================================================================================================================

    Functions

    ======================================================================================================================== */

    // 01. Admin CSS
    function TPF_admin_head() {
        echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/wp-admin.css', __FILE__). '">';

        if( is_super_admin() ){
            echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/wp-super-admin.css', __FILE__). '">';
        }

    }



    // 02. Login CSS
    function TPF_login_css() {
        echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/login.css  ', __FILE__). '">';
    }



    // 03. Admin Favicon
    function admin_favicon() {
        echo '<link rel="shortcut icon" type="image/x-icon" href="' .plugins_url('images/favicon.ico  ', __FILE__). '" />';
    }



    // 04. Change Howdy Message
    function change_howdy($translated, $text, $domain) {
        if (!is_admin() || 'default' != $domain)
            return str_replace('Howdy', 'Merhaba', $translated);
        if (false !== strpos($translated, 'Howdy'))
            return str_replace('Howdy', 'Merhaba', $translated);
        return $translated;
    }



    // 05. Footer Text
    function left_admin_footer_text_output($text) {
        $text = '&copy; 2012 Turkish Philanthropy Funds. All rights reserved.';
        return $text;
    }


    // 06. Removing Search from Admin Bar
    function disable_bar_search() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('search');
    }

    // remove links/menus from the admin bar
    function remove_edit_profile_adminbar() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('edit-profile');
        $wp_admin_bar->remove_menu('my-account-xprofile-edit');
    }


    // 07. Add to My Account Sub-Menu
    function add_my_account_links () {
        global $wp_admin_bar;

        global $current_user;
        get_currentuserinfo();

        if(current_user_can( 'start_a_campaign' )){

            global $bp;

            //Add a link called 'Campaigns'...
            $wp_admin_bar->add_menu( array(
                'parent' => 'my-account-buddypress',
                'id'     => 'campaigns',
                'title'  => 'Campaigns',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/?component=campaigns'
            ));

            $wp_admin_bar->add_menu( array(
                'parent' => 'campaigns',
                'id'     => 'active-campaigns',
                'title'  => 'Active',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/?component=campaigns'
            ));

            $wp_admin_bar->add_menu( array(
                'parent' => 'campaigns',
                'id'     => 'completed-campaigns',
                'title'  => 'Completed',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/?component=completed-campaigns'
            ));

            $wp_admin_bar->add_menu(array(
                'id'    => 'my-basic-info',
                'title' => 'Edit Basic Profile Information',
                'parent'=> 'my-account',
                'href'  => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/1/'
            ));

            $wp_admin_bar->add_menu(array(
                'id'    => 'my-campaign-info',
                'title' => 'Edit Campaign Profile Information',
                'parent'=> 'my-account',
                'href'  => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/3/'
            ));

            $wp_admin_bar->add_menu(array(
                'parent'=> 'my-account-xprofile',
                'id'    => 'my-account-basic-info',
                'title' => 'Edit Basic Profile Info',
                'href'  => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/1/'
            ));

            $wp_admin_bar->add_menu(array(
                'id'    => 'my-account-campaign-info',
                'title' => 'Edit Campaign Profile Information',
                'parent'=> 'my-account-xprofiler',
                'href'  => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login .'/profile/edit/group/3/'
            ));


            // remove links/menus from the admin bar
            add_action( 'wp_before_admin_bar_render', 'remove_edit_profile_adminbar', 0 );

        }

        if(current_user_can( 'edit_projects' )){

            global $bp;

            $wp_admin_bar->add_menu(array(
                'id'     => 'my-partner-info',
                'title'  => 'Edit Partner Profile Information',
                'parent' => 'my-account',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/2/'
            ));

            $wp_admin_bar->add_menu(array(
                'id'     => 'my-basic-info',
                'title'  => 'Edit Basic Profile Information',
                'parent' => 'my-account',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/1/'
            ));


            $wp_admin_bar->add_menu(array(
                'id'     => 'my-member-options',
                'title'  => 'Edit Member Options (Password, Personal Options, etc)',
                'parent' => 'my-account',
                'href'   => get_bloginfo( 'url' ). '/wp-admin/profile.php'
            ));

            //Add a link called 'Campaigns'...
            $wp_admin_bar->add_menu( array(
                'parent'=> 'my-account-buddypress',
                'id'    => 'projects',
                'title' => 'Projects',
                'href'  => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/?component=projects'
            ));

            $wp_admin_bar->add_menu( array(
                'parent' => 'projects',
                'id'     => 'active-projects',
                'title'  => 'Active',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/?component=projects'
            ));

            $wp_admin_bar->add_menu( array(
                'parent' => 'projects',
                'id'     => 'completed-projects',
                'title'  => 'Completed',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/?component=completed-projects'
            ));

            $wp_admin_bar->add_menu( array(
                'id'    => 'partners-manual',
                'title' => 'Partners Manual',
                'href'  => get_bloginfo( 'url' ). '/partners/partners-manual/'
            ));

            $wp_admin_bar->add_menu(array(
                'id'    => 'partner-info',
                'title' => 'Edit Partner Profile Information',
                'href'  => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/2/'
            ));

            $wp_admin_bar->add_menu(array(
                'parent' => 'my-account-xprofile',
                'id'     => 'my-account-basic-info',
                'title'  => 'Edit Basic Profile Info',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/1/'
            ));

            $wp_admin_bar->add_menu(array(
                'parent' => 'my-account-xprofile',
                'id'     => 'my-account-partner-info',
                'title'  => 'Edit Partner Profile Info',
                'href'   => get_bloginfo( 'url' ). '/my-tpf/' .$current_user->user_login. '/profile/edit/group/2/'
            ));

            // remove links/menus from the admin bar
            add_action( 'wp_before_admin_bar_render', 'remove_edit_profile_adminbar', 0 );

        }

    }




/* ========================================================================================================================

    Adding Project Meta Boxes

   ======================================================================================================================== */


    /* Begin Timeline meta boxes */

    // Scripts for Timeline
    function verbose_calendar_admin_scripts() {
        global $post;

        wp_register_script('jqueryUICore', plugins_url('scripts/jquery.ui.core.min.js', __FILE__));
        wp_enqueue_script('jqueryUICore');

        wp_register_script('jqueryUIWidget', plugins_url('scripts/jquery.ui.widget.min.js', __FILE__));
        wp_enqueue_script('jqueryUIWidget');

        wp_register_script('jqueryUIDate', plugins_url('scripts/jquery.ui.datepicker.min.js', __FILE__));
        wp_enqueue_script('jqueryUIDate');

        wp_register_script('verboseCalAdmin', plugins_url('scripts/verboseCalAdmin.js', __FILE__));
        wp_enqueue_script('verboseCalAdmin');
    }

    add_action('admin_enqueue_scripts', 'verbose_calendar_admin_scripts');



    // add custom data fields
    add_action('add_meta_boxes', 'time_line_fields_box');

    function time_line_fields_box() {
        add_meta_box('timeline_start', 'Timeline Start Date', 'timeline_start', 'project', 'side', 'low');
        add_meta_box('timeline_end', 'Timeline End Date', 'timeline_end', 'project', 'side', 'low');
    }



    // Timeline Start input box
    function timeline_start() {
        global $post;

        $values = get_post_custom($post->ID);
        $eve_start_date = isset( $values['timeline_start'] ) ? esc_attr( $values['timeline_start'][0] ) : '';


        wp_nonce_field('event_frm_nonce', 'event_frm_nonce');

        $html = "<label>Start Date</label><br/><input id='datepickerStart' type='text' name='datepickerStart' value='$eve_start_date' />";

        echo $html;
    }



    // Timeline End input box
    function timeline_end() {
        global $post;

        $values = get_post_custom($post->ID);
        $eve_end_date = isset( $values['timeline_end'] ) ? esc_attr( $values['timeline_end'][0] ) : '';


        wp_nonce_field('event_frm_nonce', 'event_frm_nonce');

        $html = "<label>End Date</label><br/><input id='datepickerEnd' type='text' name='datepickerEnd' value='$eve_end_date' />";

        echo $html;
    }



    // Saving timeline information
    add_action('save_post', 'time_line_information');

    function time_line_information($post_id) {

        // Bail if we're doing an auto save
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;

        // if our nonce isn't there, or we can't verify it, bail
        if (!isset($_POST['event_frm_nonce']) || !wp_verify_nonce($_POST['event_frm_nonce'], 'event_frm_nonce'))
            return;

        // if our current user can't edit this post, bail
        if (!current_user_can('edit_post'))
            return;


        if (isset($_POST['datepickerStart']))
            update_post_meta($post_id, 'timeline_start', esc_attr($_POST['datepickerStart']));
        if (isset($_POST['datepickerEnd']))
            update_post_meta($post_id, 'timeline_end', esc_attr($_POST['datepickerEnd']));
    }



    // Verbse pop-up calendar
    function verbose_calendar() {
        global $post;

        return '<div id="main-container"></div>
                <div id="popup_events">
                    <div class="pop_cls"></div>
                    <div id="popup_events_list">
                        <div id="popup_events_head"></div>
                        <div id="popup_events_bar"></div>
                        <div id="event_row_panel" class="event_row_panel"></div>
                        <div id="popup_events_bottom"></div>
                    </div>

                </div>';
    }

    add_shortcode("verbose_calendar", "verbose_calendar");

    /* End Project Timeline metaboxes */


    /* Begin Project Info metaboxes */
    add_action('add_meta_boxes', 'project_information');

    function project_information(){
        add_meta_box('problem_addressed', 'The Problem Addressed by the Project', 'problem_addressed', 'project');
        //add_meta_box('goal', 'The Goal of the Project', 'goal', 'project');
        add_meta_box('impact', 'The Impact of the Project', 'impact', 'project');
        add_meta_box('donor_list', 'Donor List', 'donor_list', 'project');
        add_meta_box('updates', 'Updates From the Field', 'updates', 'project');
        add_meta_box('additional_information', 'Additional Information', 'additional_information', 'project');
        //add_meta_box('financial_goal', 'Financial Goal', 'financial_goal', 'project', 'normal', 'high');
        //add_meta_box('current_total', 'Current Total', 'current_total', 'project', 'normal', 'high');
        add_meta_box('project_author', 'Project Author', 'project_author', 'project', 'side', 'high' );
        add_meta_box('complete', 'Project Status', 'complete', 'project', 'side', 'high');
        add_meta_box('coordinates', 'Location Coordinates', 'coordinates', 'project', 'side', 'high');
    }


    // The Problem Addressed by the Project editor box
    function problem_addressed() {
        global $post;

        $values = get_post_custom($post->ID);
        $problem_addressed = isset( $values['problem_addressed'] ) ? esc_attr( $values['problem_addressed'][0] ) : '';

        wp_editor(
            $problem_addressed,
            'project_problem',
            $settings = array(
                'textarea_rows' => '5'
            )
        );

    }


    // The Goal of the Project editor box
//    function goal(){
//        global $post;
//
//        $values = get_post_custom($post->ID);
//        $goal = $values['goal'][0];
//
//        wp_editor(
//            $goal,
//            'project_goal',
//            $settings = array(
//                'textarea_rows' => '5'
//            )
//        );
//
//    }


    // The Impact of the Project editor box
    function impact(){
        global $post;

        $values = get_post_custom($post->ID);
        $impact = isset( $values['impact'] ) ? esc_attr( $values['impact'][0] ) : '';

        wp_editor(
            $impact,
            'project_impact',
            $settings = array(
                'textarea_rows' => '5'
            )
        );

    }

    // Donor List editor box
    function donor_list(){
        global $post;

        $values = get_post_custom($post->ID);
        $donor_list = isset($values['donor_list']) ? esc_attr($values['donor_list'][0]) : '';

        wp_editor(
            $donor_list,
            'project_donorlist',
            $settings = array(
                'textarea_rows' => '5',
                'media_buttons' => false
            )
        );

    }

    // Updates from Field
    function updates(){
        global $post;

        $values = get_post_custom($post->ID);
        $updates = isset($values['updates']) ? esc_attr($values['updates'][0]) : '';

        wp_editor(
            $updates,
            'project_updates',
            $settings = array(
                'textarea_rows' => '10'
            )
        );

    }

    // Additional Information editor box
    function additional_information(){
        global $post;

        $values = get_post_custom($post->ID);
        $additional_information = isset($values['additional_information']) ? esc_attr($values['additional_information'][0]) : '';

        wp_editor(
            $additional_information,
            'project_addinfo',
            $settings = array(
                'textarea_rows' => '10'
            )
        );

    }

//    function financial_goal(){
//        global $post;
//
//        $values = get_post_custom($post->ID);
//        $financial_goal = $values['financial_goal'][0];
//
//        wp_nonce_field('financial_frm_nonce', 'financial_frm_nonce');
//
//        $html = "<span>We want to raise $ </span><input id='financial_goal' type='text' name='financial_goal' value='$financial_goal' /> <span>for this project.</span>";
//
//        echo $html;
//
//    }

//    function current_total(){
//        global $post;
//
//        $values = get_post_custom($post->ID);
//        $current_total = $values['current_total'][0];
//
//        wp_nonce_field('current_frm_nonce', 'current_frm_nonce');
//
//        $html = "<span>We have currently raised $ </span><input id='current_total' type='text' name='current_total' value='$current_total' /> <span>for this project.</span>";
//
//        echo $html;
//
//    }

    function project_author(){
        global $post;

        global $current_user;
        get_currentuserinfo();

//        function get_current_user_role () {
//            global $current_user;
//            get_currentuserinfo();
//            $user_roles = $current_user->roles;
//            $user_role = array_shift($user_roles);
//            return $user_role;
//        }


        if( get_current_user_role () == 'partner' || get_current_user_role () == 'trustedpartner'  ){

            $values = get_post_custom($post->ID);
            $project_author = isset( $values['project_author'] ) ? esc_attr( $values['project_author'][0] ) : '';

            wp_nonce_field('author_frm_nonce', 'author_frm_nonce');


            echo get_the_author_meta( 'display_name', $current_user->ID);

            $html = "<br/><input id='project_author' class='hide' type='text' name='project_author' value='$current_user->ID' />";

        } elseif( get_current_user_role () == 'administrator' ){

            global $wpdb;

            $values = get_post_custom($post->ID);
            $project_author = isset( $values['project_author'] ) ? esc_attr($values['project_author'][0]) : '';

            $author = $wpdb->get_row("SELECT post_author FROM $wpdb->posts WHERE ID = '$post->ID' ");

            echo get_the_author_meta( 'display_name', $author->post_author);

            $html = "<br/><input id='project_author' class='hide' type='text' name='project_author' value='$author->post_author' />";


        }

        echo $html;

    }

    function coordinates(){
        global $post;

        $values    = get_post_custom($post->ID);
        $locations = get_field('location', $post->ID);

        function getUrlContent($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,  FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }

        if( $locations ){


            for( $i = 0; $i < count($locations); $i++ ){

                $province = $locations[$i]['province'];
                $country  = $locations[$i]['country'];

                if( $province !== '' && $country !== '' ){

                    $address  = $province. ', ' .$country;

                    $url     = sprintf('http://dev.virtualearth.net/REST/v1/Locations?q=%s&key=ApsdunJWnH0y5BOm-RhLUgub5TQe3S5ESjzEfuACZY0VJCcNzSsEmmMk5SxW_SDR',rawurlencode($address));

                    $json    = getUrlContent($url);
                    $results = json_decode($json);

                    $latitude = isset( $values['project_latitude'][$i] ) ? esc_attr( $values['project_latitude'][$i] ) : '';
                    $longitude = isset( $values['project_longitude'][$i] ) ? esc_attr( $values['project_longitude'][$i] ) : '';

                    wp_nonce_field('coordinates_frm_nonce', 'coordinates_frm_nonce');

                    $locality  = $results->resourceSets[0]->resources[0]->address->locality;
                    $latitude  = $results->resourceSets[0]->resources[0]->point->coordinates[1];
                    $longitude = $results->resourceSets[0]->resources[0]->point->coordinates[0];

                    $html = "<br/><input id='project_coordinates[$i]' class='hide' type='text' name='project_coordinates[$i]' value='[&#39;$province&#39;, &#39;$locality&#39;, $longitude, $latitude]' />";

                    echo $html;

                }
            }

        }

    }

    function projectID(){
        global $post;

        echo $post->ID;

    }


    function complete(){
        global $post;

        $values = get_post_custom($post->ID);
        $complete = isset($values['complete']) ? esc_attr($values['complete'][0]) : '';

        wp_nonce_field('complete_frm_nonce', 'complete_frm_nonce');

        ?>

        <input type="checkbox" name="complete" <?php if( $complete == true ) { ?>checked="checked"<?php } ?> />  Check this box if fundraising for this project is complete.

        <?php
    }


    add_action('save_post', 'save_details');

    function save_details(){
        global $post;

        update_post_meta( $post->ID, 'problem_addressed', $_POST['project_problem'] );
        //update_post_meta( $post->ID, 'goal', $_POST['project_goal'] );
        update_post_meta( $post->ID, 'impact', $_POST['project_impact'] );
        update_post_meta( $post->ID, 'donor_list', $_POST['project_donorlist'] );
        update_post_meta( $post->ID, 'additional_information', $_POST['project_addinfo'] );
        update_post_meta( $post->ID, 'updates', $_POST['project_updates'] );
        //update_post_meta( $post->ID, 'financial_goal', $_POST['financial_goal']);
        //update_post_meta( $post->ID, 'current_total', $_POST['current_total'] );
        update_post_meta( $post->ID, 'project_author', $_POST['project_author'] );
        update_post_meta( $post->ID, "complete", $_POST["complete"] );

        update_post_meta( $post->ID, "coordinates", $_POST["project_coordinates"] );
    }


    /* Begin Project Info metaboxes */


    // Project edit columns
    add_action('manage_posts_custom_column',  'project_custom_columns');
    add_filter('manage_edit-project_columns', 'project_edit_columns');



    function project_edit_columns($columns){
        $columns = array(
            'cb'                     => '',
            'title'                  => 'Project Title',
            'project_author'         => 'Partner',
            'goal'                   => 'The Goal',
            'impact'                 => 'The Impact',
            'timeline_start'         => 'Timeline Start Date',
            'timeline_end'           => 'Timeline End Date',
            'causes'                 => 'Causes',
            'ID'                     => 'ID'
        );

        return $columns;
    }

    function ttruncat( $text, $numb ) {
        if (strlen($text) > $numb) {
            $text = substr($text, 0, $numb);
            $text = substr($text,0,strrpos($text," "));
            $etc = " ...";
            $text = $text.$etc;
        }
        return $text;
    }

    function project_custom_columns($column){
        global $post;

        switch ($column) {
            case 'cb':
                echo '<input type="checkbox" />';
                break;
            case 'project_author':
                $values = get_post_custom();
                echo  get_the_author_meta('display_name', isset( $values['project_author'] ) ? esc_attr( $values['project_author'][0] ) : '');
                break;
            case 'goal':
                $values = get_post_custom();
                echo ttruncat( isset( $values['goal'] ) ? esc_attr( $values['goal'][0] ) : '', 100 );
                break;
            case 'impact':
                $values = get_post_custom();
                echo ttruncat( isset( $values['impact'] ) ? esc_attr( $values['impact'][0] ) : '', 100 );
                break;
            case 'timeline_start':
                $values = get_post_custom();
                echo isset( $values['timeline_start'] ) ? esc_attr( $values['timeline_start'][0] ) : '';
                break;
            case 'timeline_end':
                $values = get_post_custom();
                echo isset( $values['timeline_end'] ) ? esc_attr( $values['timeline_end'][0] ) : '';
                break;
            case 'causes':
                echo get_the_term_list($post->ID, 'causes', '', ', ','');
                break;
            case 'ID':
                projectID();
                break;
        }
    }



/* ========================================================================================================================

    Adding New Tables to WP Database

    ======================================================================================================================== */

    global $campaign_version;
    $campaign_version = '1.0';

    function campaign_install(){

        global $wpdb;
        global $campaign_version;

        $table_name = 'campaigns';

        $sql = "DROP TABLE IF EXITS $table_name;
            CREATE TABLE $table_name (
                id int(11) NOT NULL AUTO_INCREMENT,
                user_id int(11) NOT NULL,
                user_name text NOT NULL,
                amount int(11) NOT NULL,
                personal_message text NOT NULL,
                project_title text NOT NULL,
                project_url text NOT NULL,
                partner text NOT NULL,
                partner_url text NOT NULL,
                project_content text NOT NULL,
                completed text NOT NULL,
                current_total text NOT NULL,
                update_message text NOT NULL,
                project_id int(11) NOT NULL,
                PRIMARY KEY (id)
            );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option("campaign_version", $campaign_version);

    }

    register_activation_hook(__FILE__,'campaign_install');

    function insertAmountRow(){
        global $wpdb;
        $wpdb->insert( 'campaigns', 'amount', '%d' );
    }

    register_activation_hook(__FILE__,'insertAmountRow');

    function insertMessageRow(){
        global $wpdb;
        $wpdb->insert( 'campaigns', 'personal_message', '%s' );
    }

    register_activation_hook(__FILE__,'insertMessageRow');

    function insertCompletedRow(){
        global $wpdb;
        $wpdb->insert( 'campaigns', 'completed', '%s' );
    }

    register_activation_hook(__FILE__,'insertCompletedRow');


    function insertCurrentTotalRow(){
        global $wpdb;
        $wpdb->insert( 'campaigns', 'current_total', '%s' );
    }

    register_activation_hook(__FILE__,'insertCurrentTotalRow');


    function insertUpdateMessageRow(){
        global $wpdb;
        $wpdb->insert( 'campaigns', 'update_message', '%s' );
    }

    register_activation_hook(__FILE__,'insertUpdateMessageRow');

    function insertProjectID(){
        global $wpdb;
        $wpdb->insert( 'campaigns', 'project_id', '%d' );
    }

    register_activation_hook(__FILE__,'insertProjectID');


?>
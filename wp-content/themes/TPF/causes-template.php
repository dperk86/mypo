<?php
/**
 * Template Name: Causes Main Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/maps/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
 
    <div class="wrapper">
        <div id="causes_page">

            <?php

                    function projectList(){

                        global $wpdb;

                        $projects = $wpdb->get_results(" SELECT * FROM $wpdb->posts WHERE post_type = 'project' AND post_status = 'publish' ");


                        $cities    = get_field_object('province');

                        foreach( $projects as $project ){

                            $ID              = $project->ID;
                            $locations       = get_field('location', $ID);
                            $projectGoal     = get_field('goal', $ID);
                            $getProject      = get_post($ID);
                            $partnerID       = $getProject->post_author;
                            $partner         = get_user_by('id', $partnerID);
                            $partnerName     = $partner->display_name;
                            $partnerProfile  = get_bloginfo('wpurl'). '/my-tpf/' .get_the_author_meta( 'user_login', $partnerID ). '/profile';

                            if( $locations ){

                                foreach($locations as $location){

                                    $province = mb_strtolower($location['province'], 'UTF-8');

                                    echo '<li class="project-' .$ID. ' project ' .$province. ' clearfix">';

                                        echo '<article>';
                                            echo '<header>';
                                                echo '<figure>';

                                                    echo '<a href="' .get_permalink( $ID ). '">';

                                                        $featuredImg = get_the_post_thumbnail( $ID, 'thumbnail' );

                                                        if( $featuredImg == null ){
                                                            echo '<img src="' . get_bloginfo( 'wpurl' ) . '/wp-content/themes/TPF/library/images/turkPhilFunds.jpg" />';
                                                        } else {
                                                            echo $featuredImg;
                                                        }

                                                    echo '</a>';

                                                echo '</figure>';
                                                echo '<h6><a href="' .get_permalink( $ID ). '">';
                                                    echo get_the_title($ID);
                                                echo '</a></h6>';
                                                echo '<div class="author">';
                                                    echo 'Partner: ';
                                                    echo '<a href="' .$partnerProfile. '">';
                                                        echo $partnerName;
                                                    echo '</a>';
                                                echo '</div>';
                                            echo '</header>';
                                            echo '<p>';
                                                $projectContent = strip_tags($projectGoal);
                                                $content = ttruncat( $projectContent, 250 );
                                                echo $content;
                                            echo '</p>';
                                            echo '<a class="button" href="' .get_permalink( $ID ). '">';
                                                echo 'View Project &raquo;';
                                            echo '</a>';
                                        echo '</article>';
                                    echo '</li>';

                                }

                            }

                        }

                    }



            ?>

            <?php

                function projectIDs(){
                    global $wpdb;
                    $projects = $wpdb->get_results(" SELECT * FROM $wpdb->posts WHERE post_type = 'project' AND post_status = 'publish' ");

                    //print_r($projects);

                    foreach($projects as $project){
                        echo $project->ID;
                    }
                }

                //projectIDs();

            ?>

            <div id="map_canvas" style="width:940px; height:425px;"></div>
            <div id="projects_list">
                <h3>Click on a marker to view the list of projects in that area</h3>
                <ul>
                    <?php projectList(); ?>
                </ul>
            </div><!-- end #projects_list -->

            <?php if (have_posts()) while (have_posts()) : the_post(); ?>

                <div class="content">
                    <?php the_content(); ?>
                </div><!-- end .content -->

            <?php endwhile; ?>

            <?php
            $tax_terms = get_terms('causes', 'orderby=count&hide_empty=1');
            $tag_extra_fields = get_option(CAUSES_FIELDS);
            foreach ($tax_terms as $tax_term) {
                $tax_term_id = $tax_term->term_id;
                $images = get_option('taxonomy_image_plugin');
                $image_attributes = wp_get_attachment_image_src( $images[$tax_term_id] );

                ?>
                       <div class="single_cause">
                           <div class="single_cause-content">
                               <div class="cause_info">
                                   <div class="single_cause-image">
                                       <a href="<?php echo get_term_link($tax_term,$tax_term->taxonomy); ?>">
                                           <img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>">
                                       </a>
                                   </div>
                                   <div class="single_cause-text">
                                       <h3>
                                           <?php
                                           echo $tag_extra_fields[$tax_term_id ]['title_partone'];
                                           ?>
                                       </h3>
                                       <h3 class="blue">
                                           <?php
                                           echo $tag_extra_fields[$tax_term_id]['title_parttwo'];
                                           ?>
                                       </h3>
                                       <div class="single_cause-excerpt">
                                           <?php echo $tax_term->description; ?>
                                       </div>
                                       <div class="single_cause-donate">
                                             <a href="<?php echo get_bloginfo('url'); ?>/blog/project/<?php echo $tax_term->slug; ?>"><h6>Support <?php echo $tax_term->name ?> in Turkey</h6></a>                   
                                       </div>
                                   </div>
                               </div>
                               <div class="recent_projects">
                                   <?php
                                   $projects = query_posts(array(
                                       'post_type' => 'project',
                                       'showposts' => 7,
                                       'tax_query' => array(array(
                                           'taxonomy' => 'causes',
                                           'terms' => $tax_term_id,
                                           'field' => 'term_id'
                                       )),
                                       'orderby' => 'date',
                                       'order' => 'DESC'
                                   ));

                                   if ($projects) {
                                       echo '<p>Recent Projects:</p>';
                                       echo '<ul>';
                                           foreach ($projects as $project){
                                               echo '<li><span><a href="'. get_permalink($project->ID) .'">'. $project->post_title . '</a></span></il>';
                                           }

                                       echo '</ul>';
                                   }
                                   echo '</div>';
                                   echo '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '"
                                                title="' . sprintf( __( "View all projects in %s" ), $tax_term->name ) . '" ' . '>' . 'Learn More &raquo; '.
                                       '</a>'
                                   ?>
                               </div>
                           </div>
                <?php
            }
            ?>
        </div><!-- end #causes -->
    </div><!-- end .wrapper -->
         
 
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
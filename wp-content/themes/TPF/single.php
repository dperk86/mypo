<?php
/**
 * Template Name: Single
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
    <div class="wrapper">
        <div class="column">
            <section id="primary">


                <article class="article">

                    <header>
                        <time datetime="<?php the_time( 'Y-m-D' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>

                    </header>

                    <div class="content">
                        <?php the_content(); ?>
                    </div><!-- end .content -->

                    <footer>

                        <?php if ( get_the_author_meta( 'description' ) ) : ?>
                        <?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>

                        <h3>About <?php echo get_the_author() ; ?></h3>

                        <?php the_author_meta( 'description' ); ?>
                        <?php endif; ?>

                        <?php comments_template( '', true ); ?>

                    </footer>

                </article><!-- end .article -->

                <?php endwhile; ?>

                <div id="share">
                    <div id="shareme" data-url="<?php echo get_permalink(get_the_ID()); ?>" data-text="<?php the_title(); ?>"><h6>Share this post via social networks:</h6></div>
                    <!-- <div id="twitter" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="Tweet"></div>-->
                    <!-- <div id="facebook" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="Like"></div>-->
                    <!-- <div id="googleplus" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="+1"></div>-->
                </div><!-- end #share -->

            </section><!-- end #primary -->
        </div><!-- end .column -->
    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php
/**
 * Template Name: Share Campaign via Email Form Template
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php if(isset( $_GET['action']) && isset($_GET['id']) && isset($_GET['slug'])) : ?>


    <?php

        $id = $_GET['id'];
        $slug = $_GET['slug'];

        global $wpdb;
        $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $id");

    ?>

    <?php if($campaign) : ?>

        <?php

            $user_info     = get_userdata( $campaign->user_id );
            $user          = $user_info->user_login;
            $firstName     = $user_info->first_name;
            $lastName      = $user_info->last_name;
            $campaignID    = $campaign->id;
            $financialGoal = $campaign->amount;
            // This is for the current Total of donations. This will be updated by paypal.
            $currentTotal  = $campaign->current_total;

        ?>

        <div id="share-contact-form" class="clearfix">
            <h3>Share this campaign</h3>
            <?php
                //init variables
                $cf = array();
                $sr = false;

                if(isset($_SESSION['cf_returndata'])){
                    $cf = $_SESSION['cf_returndata'];
                    $sr = true;
                }
            ?>
            <ul id="errors" class="<?php echo ($sr && !$cf['form_ok']) ? 'visible' : ''; ?>">
                <li id="info">There were some problems with your form submission:</li>
                <?php
                if(isset($cf['errors']) && count($cf['errors']) > 0) :
                    foreach($cf['errors'] as $error) :
                        ?>
                        <li><?php echo $error ?></li>
                        <?php
                    endforeach;
                endif;
                ?>
            </ul>
            <p id="success" class="<?php echo ($sr && $cf['form_ok']) ? 'visible' : ''; ?>">
                Your message has been successfully sent! Send another.
            </p>
            <?php
                global $current_user;
                get_currentuserinfo();
            ?>

            <?php if ( $current_user->ID == $campaign->user_id ) : ?>

                <form method="post" action="<?php echo get_bloginfo( 'template_directory' ) ?>/library/email/user-process.php">

            <?php else : ?>

                <form method="post" action="<?php echo get_bloginfo( 'template_directory' ) ?>/library/email/process.php">

            <?php endif; ?>

                <div id="email-form">

                    <label for="name">Friend's Name: <span class="required">*</span></label>
                    <input type="text" id="name" name="name" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['name'] : '' ?>" required autofocus />

                    <label for="email">Friend's Email: <span class="required">*</span><span class="info">friendemail@example.com</span></label>
                    <input type="email" id="email" name="email" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['email'] : '' ?>" required />

                    <?php if (is_user_logged_in()) : ?>

                        <label for="your_name">Your Name: <span class="required">*</span></label>
                        <input type="text" id="your_name" name="your_name" value="<?php if($current_user->user_firstname == null && $current_user->user_lastname == null){ echo $user; } else { echo $firstName. ' ' .$lastName; } ?>" required autofocus />

                        <label for="your_email">Your Email: <span class="required">*</span><span class="info">youremail@example.com</span></label>
                        <input type="email" id="your_email" name="your_email" value="<?php echo $current_user->user_email; ?>" required />

                    <?php else : ?>

                        <label for="your_name">Your Name: <span class="required">*</span></label>
                        <input type="text" id="your_name" name="your_name" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['your_name'] : '' ?>" required autofocus />

                        <label for="your_email">Your Email: <span class="required">*</span><span class="info">youremail@example.com</span></label>
                        <input type="email" id="your_email" name="your_email" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['your_email'] : '' ?>" required />

                    <?php endif; ?>

                    <?php if ( $current_user->ID == $campaign->user_id ) : ?>

                        <label for="subject">Subject: <span class="required">*</span></label>
                        <input type="text" id="subject" name="subject" value="Help me raise $<?php echo $financialGoal; ?> for <?php echo $campaign->project_title; ?>" />

                        <label for="raising">I'll be raising money by: <span class="info">i.e running a race, baking, hiking</span> </label>
                        <input type="text" id="raising" name="raising" value="<?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['raising'] : '' ?>" />

                        <label for="date">Date you'd like to complete this campaign:</label>
                        <select id="month" name="month" class="date">
                            <option value="" class="selectoption" label="Month">Month</option>
                            <option value="Jan" class="selectoption" label="Jan">Jan</option>
                            <option value="Feb" class="selectoption" label="Feb">Feb</option>
                            <option value="Mar" class="selectoption" label="Mar">Mar</option>
                            <option value="Apr" class="selectoption" label="Apr">Apr</option>
                            <option value="May" class="selectoption" label="May">May</option>
                            <option value="Jun" class="selectoption" label="Jun">Jun</option>
                            <option value="Jul" class="selectoption" label="Jul">Jul</option>
                            <option value="Aug" class="selectoption" label="Aug">Aug</option>
                            <option value="Sep" class="selectoption" label="Sep">Sep</option>
                            <option value="Oct" class="selectoption" label="Oct">Oct</option>
                            <option value="Nov" class="selectoption" label="Nov">Nov</option>
                            <option value="Dec" class="selectoption" label="Dec">Dec</option>
                        </select>
                        <select id="day" name="day" class="date">
                            <option value="" class="selectoption" label="Day">Day</option>
                            <option value="01" class="selectoption" label="01">01</option>
                            <option value="02" class="selectoption" label="02">02</option>
                            <option value="03" class="selectoption" label="03">03</option>
                            <option value="04" class="selectoption" label="04">04</option>
                            <option value="05" class="selectoption" label="05">05</option>
                            <option value="06" class="selectoption" label="06">06</option>
                            <option value="07" class="selectoption" label="07">07</option>
                            <option value="08" class="selectoption" label="08">08</option>
                            <option value="09" class="selectoption" label="09">09</option>
                            <option value="10" class="selectoption" label="10">10</option>
                            <option value="11" class="selectoption" label="11">11</option>
                            <option value="12" class="selectoption" label="12">12</option>
                            <option value="13" class="selectoption" label="13">13</option>
                            <option value="14" class="selectoption" label="14">14</option>
                            <option value="15" class="selectoption" label="15">15</option>
                            <option value="16" class="selectoption" label="16">16</option>
                            <option value="17" class="selectoption" label="17">17</option>
                            <option value="18" class="selectoption" label="18">18</option>
                            <option value="19" class="selectoption" label="19">19</option>
                            <option value="20" class="selectoption" label="20">20</option>
                            <option value="21" class="selectoption" label="21">21</option>
                            <option value="22" class="selectoption" label="22">22</option>
                            <option value="23" class="selectoption" label="23">23</option>
                            <option value="24" class="selectoption" label="24">24</option>
                            <option value="25" class="selectoption" label="25">25</option>
                            <option value="26" class="selectoption" label="26">26</option>
                            <option value="27" class="selectoption" label="27">27</option>
                            <option value="28" class="selectoption" label="28">28</option>
                            <option value="29" class="selectoption" label="29">29</option>
                            <option value="30" class="selectoption" label="30">30</option>
                            <option value="31" class="selectoption" label="31">31</option>
                        </select>
                        <select id="year" name="year" class="date">
                            <option value="" class="selectoption" label="Year">Year</option>
                            <option value="2013" class="selectoption" label="2013">2013</option>
                            <option value="2014" class="selectoption" label="2014">2014</option>
                            <option value="2015" class="selectoption" label="2015">2015</option>
                            <option value="2016" class="selectoption" label="2016">2016</option>
                            <option value="2017" class="selectoption" label="2017">2017</option>
                            <option value="2018" class="selectoption" label="2018">2018</option>
                            <option value="2019" class="selectoption" label="2019">2019</option>
                            <option value="2020" class="selectoption" label="2020">2020</option>
                            <option value="2021" class="selectoption" label="2021">2021</option>
                            <option value="2022" class="selectoption" label="2022">2022</option>
                            <option value="2023" class="selectoption" label="2023">2023</option>
                            <option value="2024" class="selectoption" label="2024">2024</option>
                        </select>

                    <?php else : ?>

                        <label for="subject">Subject: <span class="required">*</span></label>
                        <input type="text" id="subject" name="subject" value="Help <?php if($firstName == null && $lastName== null){ echo $user; } else { echo $firstName; } ?> raise $<?php echo $financialGoal; ?> for <?php echo $campaign->project_title; ?>" />

                    <?php endif; ?>

                    <input type="hidden" id="link" name="link" value="<?php echo get_bloginfo( 'wpurl' ) ?>/campaigns/campaign/?action=view&id=<?php echo $id; ?>&slug=<?php echo $slug; ?>" />

                    <?php if ( $current_user->ID == $campaign->user_id ) : ?>
                        <input type="hidden" id="amount" name="amount" value="<?php echo $financialGoal; ?>" />
                        <input type="hidden" id="project" name="project" value="<?php echo $campaign->project_title; ?>" />
                    <?php endif; ?>

                    <label for="message">Personal Message: <span class="required">*</span><span class="info">Your message must be greater than 20 characters</span></label>
                    <textarea id="message" name="message" required data-minlength="20"><?php echo ($sr && !$cf['form_ok']) ? $cf['posted_form_data']['message'] : '' ?></textarea>

                    <span id="loading"></span>

                    <p id="req-field-desc"><span class="required">*</span> indicates a required field</p>

                    <input type="submit" value="Send" id="submit-button" />
                    <button type="button" id="preview">Preview this email</button>


                </div><!-- end .email-form -->
                <div id="preview-email" style="display: none;">

                    <div class="content">

                    </div><!-- end .content -->

                    <input type="submit" value="Submit" id="submit-button" />
                    <button type="button" id="edit-email">Edit this email</button>

                </div><!-- end #preview-email -->

            </form>
            <?php unset($_SESSION['cf_returndata']); ?>

        </div><!-- end #share-contact-form -->

        <script type="text/javascript">
            jQuery(document).ready(function( $ ) {

                //set global variables and cache DOM elements for reuse later
                var form = $('#share-contact-form').find('form'),
                    formElements = form.find('input[type!="submit"],textarea'),
                    formSubmitButton = form.find('[type="submit"]'),
                    errorNotice = $('#errors'),
                    successNotice = $('#success'),
                    loading = $('#loading'),
                    errorMessages = {
                        required: ' is a required field',
                        email: 'You have not entered a valid email address for the field: ',
                        minlength: ' must be greater than '
                    };

                //feature detection + polyfills
                formElements.each(function(){

                    //if HTML5 input placeholder attribute is not supported
                    if(!Modernizr.input.placeholder){
                        var placeholderText = this.getAttribute('placeholder');
                        if(placeholderText){
                            $(this)
                                    .addClass('placeholder-text')
                                    .val(placeholderText)
                                    .bind('focus',function(){
                                        if(this.value == placeholderText){
                                            $(this)
                                                    .val('')
                                                    .removeClass('placeholder-text');
                                        }
                                    })
                                    .bind('blur',function(){
                                        if(this.value == ''){
                                            $(this)
                                                    .val(placeholderText)
                                                    .addClass('placeholder-text');
                                        }
                                    });
                        }
                    }

                    //if HTML5 input autofocus attribute is not supported
                    if(!Modernizr.input.autofocus){
                        if(this.getAttribute('autofocus')) this.focus();
                    }

                });

                //to ensure compatibility with HTML5 forms, we have to validate the form on submit button click event rather than form submit event.
                //An invalid html5 form element will not trigger a form submit.
                formSubmitButton.bind('click',function(){
                    var formok = true,
                            errors = [];

                    formElements.each(function(){
                        var name = this.name,
                            nameUC = name.ucfirst(),
                            value = this.value,
                            placeholderText = this.getAttribute('placeholder'),
                            type = this.getAttribute('type'), //get type old school way
                            isRequired = this.getAttribute('required'),
                            minLength = this.getAttribute('data-minlength');

                        //if HTML5 formfields are supported
                        if( (this.validity) && !this.validity.valid ){
                            formok = false;

                            //if there is a value missing
                            if(this.validity.valueMissing){
                                errors.push(nameUC + errorMessages.required);
                            }
                            //if this is an email input and it is not valid
                            else if(this.validity.typeMismatch && type == 'email'){
                                errors.push(errorMessages.email + nameUC);
                            }

                            this.focus(); //safari does not focus element an invalid element
                            return false;
                        }

                        //if this is a required element
                        if(isRequired){
                            //if HTML5 input required attribute is not supported
                            if(!Modernizr.input.required){
                                if(value == placeholderText){
                                    this.focus();
                                    formok = false;
                                    errors.push(nameUC + errorMessages.required);
                                    return false;
                                }
                            }
                        }

                        //if HTML5 input email input is not supported
                        if(type == 'email'){
                            if(!Modernizr.inputtypes.email){
                                var emailRegEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                if( !emailRegEx.test(value) ){
                                    this.focus();
                                    formok = false;
                                    errors.push(errorMessages.email + nameUC);
                                    return false;
                                }
                            }
                        }

                        //check minimum lengths
                        if(minLength){
                            if( value.length < parseInt(minLength) ){
                                this.focus();
                                formok = false;
                                errors.push(nameUC + errorMessages.minlength + minLength + ' characters');
                                return false;
                            }
                        }
                    });

                    //if form is not valid
                    if(!formok){

                        //animate required field notice
                        $('#req-field-desc')
                                .stop()
                                .animate({
                                    marginLeft: '+=' + 5
                                },150,function(){
                                    $(this).animate({
                                        marginLeft: '-=' + 5
                                    },150);
                                });

                        //show error message
                        showNotice('error',errors);

                    }
                    //if form is valid
                    else {
                        loading.show();
                        $.ajax({
                            url: form.attr('action'),
                            type: form.attr('method'),
                            data: form.serialize(),
                            success: function(){
                                showNotice('success');
                                form.get(0).reset();
                                loading.hide();
                                $('#preview-email').hide();
                                $('#email-form').show();
                                $('#preview-email .content').empty();
                            }
                        });
                    }

                    return false; //this stops submission off the form and also stops browsers showing default error messages

                });

                //other misc functions
                function showNotice(type,data)
                {
                    if(type == 'error'){
                        successNotice.hide();
                        errorNotice.find("li[id!='info']").remove();
                        for(x in data){
                            errorNotice.append('<li>'+data[x]+'</li>');
                        }
                        errorNotice.show();
                    }
                    else {
                        errorNotice.hide();
                        successNotice.show();
                    }
                }

                String.prototype.ucfirst = function() {
                    return this.charAt(0).toUpperCase() + this.slice(1);
                }

                <?php if ( $current_user->ID == $campaign->user_id ) : ?>

                        $('#preview').click(function(){

                            var $name      = $('input#name').val(),
                                $email     = $('input#email').val(),
                                $yourname  = $('input#your_name').val(),
                                $youremail = $('input#your_email').val(),
                                $subject   = $('input#subject').val(),
                                $raising   = $('input#raising').val(),
                                $month     = $('select#month').val(),
                                $day       = $('select#day').val(),
                                $year      = $('select#year').val(),
                                $link      = $('input#link').val(),
                                $amount    = $('input#amount').val(),
                                $project   = $('input#project').val(),
                                $message   = $('textarea#message').val();

                            var $preview   = "<p><strong>To:</strong><br/>" + $email + "</p>" +
                                             "<p><strong>From:</strong><br/>" + $youremail + "</p>" +
                                             "<p><strong>Subject:</strong><br/>" + $subject + "</p>" +
                                             "<p><strong>Email:</strong><br/>Dear " + $name + ",</p>" +
                                             "<p>I'm raising money for &#8220;" + $project + "&#8221; by " + $raising + ".</p>" +
                                             "<p>Help me raise $" + $amount + " by " + $month + " " + $day + ", " + $year + ". Donate to my campaign: " + $link + "</p>" +
                                             "<p>" + $message + "</p>" +
                                             "<p>What’s great about Turkish Philanthropy Funds is that they hand pick partner organizations in Turkey, making sure each organization is transparent and trustworthy. They also provide donors with progress reports to see exactly where their money has gone.</p>" +
                                             "<p>Thank you!</p>" +
                                             "<p>" + $yourname + "</p>";

                            $('#preview-email').show().find('.content').prepend($preview);
                            $('#email-form').hide();

                        });

                <?php else : ?>

                        $('#preview').click(function(){

                            var $name      = $('input#name').val(),
                                $email     = $('input#email').val(),
                                $yourname  = $('input#your_name').val(),
                                $youremail = $('input#your_email').val(),
                                $subject   = $('input#subject').val(),
                                $link      = $('input#link').val(),
                                $message   = $('textarea#message').val();

                            var $preview   = "<p><strong>To:</strong><br/>" + $email + "</p>" +
                                             "<p><strong>From:</strong><br/>" + $youremail + "</p>" +
                                             "<p><strong>Subject:</strong><br/>" + $subject + "</p>" +
                                             "<p><strong>Email:</strong><br/>Hi " + $name + ",</p>" +
                                             "<p>" + $message + "</p>" +
                                             "<p>Donate to the campaign here: <br/>" + $link + "</p>" +
                                             "<p>" + $yourname + "</p>";

                            $('#preview-email').show().find('.content').prepend($preview);
                            $('#email-form').hide();

                        });

                <?php endif; ?>

                $('#edit-email').click(function(){

                    $('#preview-email .content').empty();
                    $('#preview-email').hide();
                    $('#email-form').show();

                });


            });
        </script>

    <?php endif; ?>

<?php endif; ?>

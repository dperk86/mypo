<?php
/**
 * Template Name: Single Newsletter
 *
 * @package     WordPress
 * @subpackage  Starkers
 * @since       Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
 
        <div class="wrapper">
            <section class="primary">
 
                <article class="newsletter">

                    <header>
                        <?php
                        // TEXT: Newsletter Subheader
                        $subheader = get_post_custom_values('subheader');
                        foreach ( $subheader as $key => $value ) {

                            if( isset($value[0]) ){

                                echo '<h5>';
                                echo "$value ";
                                echo '</h5>';

                            }


                        }
                        ?>
                    </header>

                    <?php the_content(); ?>

                    <?php
                    if( function_exists( 'attachments_get_attachments' ) ){
                        $attachments = attachments_get_attachments();
                        $total_attachments = count( $attachments );
                        if( $total_attachments ) : ?>
                            <footer>
                                Download the latest TPF newsletter:<br/>
                                <?php for( $i=0; $i<$total_attachments; $i++ ) : ?>
                                <a href="<?php echo $attachments[$i]['location']; ?>"><?php echo $attachments[$i]['title']; ?></a>
                                <p>
                                    <?php echo $attachments[$i]['caption']; ?><br/>
                                    <strong>File Type:</strong> <?php echo $attachments[$i]['mime']; ?> <span>|</span> <strong>File Size:</strong> <?php echo $attachments[$i]['filesize']; ?>
                                </p>
                                <?php endfor; ?>
                            </footer>
                            <?php endif; ?>
                        <?php } ?>
                    <div id="share">
                        <div id="shareme" data-url="<?php echo get_permalink(get_the_ID()); ?>" data-text="<?php the_title(); ?>"><h6>Share this newsletter via social networks:</h6></div>
                        <!-- <div id="twitter" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="Tweet"></div>-->
                        <!-- <div id="facebook" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="Like"></div>-->
                        <!-- <div id="googleplus" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="+1"></div>-->
                    </div><!-- end #share -->
                </article><!-- end .newsletter -->

    <?php endwhile; ?>
 
            </section><!-- end .primary -->
 
        </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php
/**
 * Template Name: Page
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts(array('parts/shared/html-header', 'parts/shared/header')); ?>

<header id="hero">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
    </div>
</header><!-- end #hero -->

<div class="wrapper">
    <div class="column">
        <section id="primary" class="page">

            <?php if (isset($_GET) && isset($_GET['thank']) && $_GET['thank'] = true): ?>

                <div id="thanks">
                    <h4>Thank You For Your Donation!</h4>
                </div>

            <?php endif; ?>

            <?php if (have_posts()) while (have_posts()) : the_post(); ?>

                    <div class="content">
                        <?php the_content(); ?>
                    </div><!-- end .content -->

            <?php endwhile; ?>

        </section><!-- end #primary -->
        
        <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

    </div>
</div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

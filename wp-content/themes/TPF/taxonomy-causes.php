<?php
/**
 * Template Name: Taxonomy Causes
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ): ?>


    <?php
        $slug = basename($_SERVER['REQUEST_URI']);
        $termID = get_term_by('slug', $slug, 'causes');
    ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php echo single_cat_title( '', false ); ?></h1>
        </div><!-- end .wrapper -->
        <nav id="projects-nav" class="page-sub-nav">
            <div class="wrapper">
                <ul>
                    <li class="active"><a href="<?php echo get_bloginfo('url'); ?>/blog/causes-category/<?php echo $slug; ?>/">Active</a></li>
                    <li><a href="<?php echo get_bloginfo('url'); ?>/causes-page/completed-projects/?catid=<?php echo $catID; ?>&slug=<?php echo $slug; ?>">Completed</a></li>
                </ul>
            </div>
        </nav>
        <div class="general_cause">
            <div class="wrapper">
                <a href="<?php echo get_bloginfo('url'); ?>/blog/project/<?php echo $slug; ?>"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/TPF/library/images/dollaricon.png" alt="TPF Donate" height="50" width="50"><h5>Support <?php echo single_cat_title( '', false ); ?> in Turkey</h5></a>
            </div>
        </div>
        
    </header><!-- end #hero -->

    <div class="wrapper">

        <section id="primary" class="category">

            <div id="tiles">

                <?php $count = 0; ?>

                <?php
                    $args = array(
                        'post_type'      => 'project',
                        'post_status'    => 'publish',
                        'posts_per_page' => -1, // = all of 'em
                        'meta_key'       => 'complete',
                        'meta_value'     => ' ',
                        'tax_query'      => array(

                            array(
                                'taxonomy'   => 'causes',
                                'terms'      => $slug,
                                'field'      => 'slug'
                            )

                        ),
                    );
                ?>

                <?php $loop = new WP_Query( $args ); ?>

                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <?php $count++; ?>

                        <?php if ($count == 1) : ?>
                        <article class="article project first">

                        <?php else : ?>
                        <article class="article project">
                        <?php endif; ?>

                            <figure>

                                <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">

                                    <?php

                                        $featuredImg = wp_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'medium', false, '' );

                                        if( $featuredImg == null ){
                                            echo '<img src="' . get_bloginfo( 'wpurl' ) . '/wp-content/themes/TPF/library/images/logo_feat_medium.png" />';
                                        } else {
                                            echo $featuredImg;
                                        }

                                    ?>

                                </a>

                            </figure>

                            <header>

                                <h4>
                                    <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>

                                <span class="author">
                                    Organization:
                                    <a href="<?php bloginfo( 'url' ); ?>/my-tpf/<?php echo get_the_author_meta( 'user_login' ); ?>/profile">
                                        <?php echo get_the_author(); ?>
                                    </a>

                                </span>

                            </header>

                            <div class="content">

                                <p>
                                    <?php

                                        $goal_values = get_post_custom_values('goal');

                                        foreach ( $goal_values as $key => $value ) {

                                            //echo "$value";
                                            echo ttruncat( $value, 225 );
                                        }

                                    ?>
                                </p>
                            </div><!-- end .content -->

                            <footer>

                                <?php

                                    $start_values = get_post_custom_values('timeline_start');
                                    foreach ( $start_values as $key => $value ) {

                                        if( isset($value[0]) ){

                                            echo '<p>';

                                            echo '<span>Timeline:</span> ';
                                            echo "$value ";
                                            echo ' - ';

                                        }

                                    }

                                ?>


                                <?php

                                    $end_values = get_post_custom_values('timeline_end');
                                        foreach ( $end_values as $key => $value ) {

                                            if( isset($value[0]) ){

                                            echo "$value ";
                                            echo '</p>';

                                        }
                                    }

                                ?>

                                <?php

                                    echo '<section class="start-campaign-button">';

                                        locate_template(array('members/single/campaigns/start-loop-campaign-button.php'), true, false);

                                    echo '</section>';

                                ?>

                            </footer>

                        </article><!-- end .article -->


                <?php endwhile; ?>

                <?php else: ?>
                <h2>No projects to display in <?php echo single_cat_title( '', false ); ?></h2>
                <?php endif; ?>

            </div><!-- end #tiles -->
        </section><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
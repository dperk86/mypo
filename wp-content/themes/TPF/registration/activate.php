<?php get_template_parts( array( 'parts/profile/html-header', 'parts/profile/header' ) ); ?>

    <div class="wrapper">
        <section id="primary" class="page">

            <?php do_action( 'bp_before_activation_page' ); ?>

            <div class="page" id="activate-page">

                <?php if ( bp_account_was_activated() ) : ?>

                    <h2 class="widgettitle"><?php _e( 'Account Activated', 'buddypress' ); ?></h2>

                    <?php do_action( 'bp_before_activate_content' ); ?>

                    <?php if ( isset( $_GET['e'] ) ) : ?>
                        <p><?php _e( 'You have successfully created your account! To begin using TPF site you will need to activate your account via the email we have just sent to your address.', 'buddypress' ); ?></p>
                    <?php else : ?>
                        <p><?php _e( 'Your account was activated successfully! You can now log in with the username and password you provided when you signed up.', 'buddypress' ); ?></p>
                    <?php endif; ?>

                <?php else : ?>

                    <h3><?php _e( 'Activate your Account', 'buddypress' ); ?></h3>

                    <?php do_action( 'bp_before_activate_content' ); ?>

                    <p><?php _e( 'Please provide a valid activation key.', 'buddypress' ); ?></p>

                    <form action="" method="get" class="standard-form" id="activation-form">

                        <label for="key"><?php _e( 'Activation Key:', 'buddypress' ); ?></label>
                        <input type="text" name="key" id="key" value="" />

                        <p class="submit">
                            <input type="submit" name="submit" value="<?php _e( 'Activate', 'buddypress' ); ?>" />
                        </p>

                    </form>

                <?php endif; ?>

                <?php do_action( 'bp_after_activate_content' ); ?>

            </div><!-- .page -->

            <?php do_action( 'bp_after_activation_page' ); ?>

        </section><!-- end #primary -->
    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

<?php
/**
 * Template Name: Search
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <section id="primary">

        <?php if ( have_posts() ): ?>
        <header>
            <h2>Search Results for '<?php echo get_search_query(); ?>'</h2>
        </header>
        <ol>
            <?php while ( have_posts() ) : the_post(); ?>
            <li>
                <article class="article">
                    <header>
                        <h2>
                            <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                <?php the_title(); ?>
                            </a>
                        </h2>

                        <time datetime="<?php the_time( 'Y-m-D' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
                    </header>

                    <div id="content">
                        <?php the_content(); ?>
                    </div><!-- end .content -->

                </article><!-- end .article -->
            </li>
            <?php endwhile; ?>
        </ol>
        <?php else: ?>
        <h2>No results found for '<?php echo get_search_query(); ?>'</h2>
        <?php endif; ?>

    </section><!-- end #primary -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
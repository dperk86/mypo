<?php
/**
 * Template Name: Two Column
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts(array('parts/shared/html-header', 'parts/shared/header')); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>

        <?php 
	$title = get_the_title();
	//Cem - Nov 11, 2013: hard coding a page title to prevent the thumbnail from showing
	if( has_post_thumbnail() && $title != '24-Hr Music Marathon for Education Dec. 3rd' ) { ?>

            <div id="gallery">
                <figure class="wrapper">
                    <?php the_post_thumbnail(); ?>
                </figure><!-- end .wrapper -->
            </div><!-- end #gallery -->

        <?php } ?>

    </header><!-- end #hero -->

    <div class="wrapper">
        <section id="primary" class="page">

            <?php if (isset($_GET) && isset($_GET['thank']) && $_GET['thank'] = true): ?>

                <div id="thanks">
                    <h4>Thank You For Your Donation!</h4>
                </div>

            <?php endif; ?>


            <section id="column-one" class="column">

                <div class="content">
                    <?php echo get_field('column_one'); ?>
                </div><!-- end .content -->

            </section><!-- end #column-one -->

            <section id="column-two" class="column">

                <div class="content">
                    <?php echo get_field('column_two'); ?>
                </div><!-- end .content -->

            </section><!-- end #column-two -->

         </section><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

<?php
/**
 * Template Name: Donate Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->
    <div class="wrapper">
            <section id="primary">
                <div id="donate_page">
      
                   
                    <div id="donateAccordion">     
                         <ol>
                    <?php 
                        if ( have_posts() ) while ( have_posts() ) : the_post();
                        
                                 
                            for ($i=1; $i<=5; $i++){
                                $slide_bg = get_field('slide_bg_'.$i);
                                $slide_text = get_field('slide_text_'.$i); 
                                $slide_label = get_field('slide_label_'.$i);
                       ?>
                                <li>
                                    <h2><span><?php echo $slide_label  ?></span></h2>
                                    <div>
                                        <figure>
                                            <img src="<?php echo $slide_bg['sizes']['large'];  ?>" alt="image" />
                                            <figcaption><?php echo $slide_text; ?></figcaption>
                                        </figure>
                                    </div>
                                </li>
                       <?php
                            }
                           
                        endwhile;
                    ?>
                           
          
                            </ol>
                            <noscript>
                                <p>Please enable JavaScript to get the full experience.</p>
                            </noscript>
                        </div>             
                </div><!-- end #donate -->
                <div class="content">
                    
                       <?php if ( have_posts() ) while ( have_posts() ) : the_post();
                           the_content();
                        endwhile; ?>
                 </div><!-- end .content -->
            </section>

    </div><!-- end .wrapper -->

<script type="text/javascript">
  //Cem 10/27/2013: trying to get rid of that animation
  try
  {
    var elements = document.getElementsByClassName("wpsc_loading_animation");
    if(elements.count == 1)
        elements[0].style.display = "none";
    }
  }
  catch(err)
  {}
</script>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
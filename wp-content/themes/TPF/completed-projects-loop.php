<?php
/**
 * Template Name: Completed Causes Template
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php if(isset($_GET['catid']) && isset($_GET['slug'])) : ?>

    <?php

        $catID = $_GET['catid'];
        $slug = $_GET['slug'];

    ?>

    <?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

        <?php if ( have_posts() ): ?>


            <header id="hero">
                <div class="wrapper">
                    <?php $term = get_term_by('id', $catID, 'causes'); ?>
                    <h1>Completed <?php echo $term->name; ?></h1>
                </div><!-- end .wrapper -->
                <nav id="projects-nav" class="page-sub-nav">
                    <div class="wrapper">
                        <ul>
                            <li><a href="<?php echo get_bloginfo('url'); ?>/blog/causes-category/<?php echo $slug; ?>/">Active</a></li>
                            <li class="active"><a href="<?php echo get_bloginfo('url'); ?>/causes-page/completed-projects/?catid=<?php echo $catID; ?>&slug=<?php echo $slug; ?>">Completed</a></li>
                        </ul>
                    </div><!-- end .wrapper -->
                </nav>
            </header><!-- end #hero -->

            <div class="wrapper">

                <section id="primary" class="category">

                    <div id="tiles">

                        <?php $count = 0; ?>

                        <?php
                            $args = array(
                                'post_type'      => 'project',
                                'post_status'    => 'publish',
                                'posts_per_page' => -1, // = all of 'em
                                'meta_key'       => 'complete',
                                'meta_value'     => 'on',
                                'tax_query'      => array(

                                    array(
                                        'taxonomy'   => 'causes',
                                        'terms'      => $slug,
                                        'field'      => 'slug'
                                    )

                                ),
                            );
                        ?>

                        <?php $loop = new WP_Query( $args ); ?>

                        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                            <?php $count++; ?>

                            <?php if ($count == 1) : ?>
                                <article class="article project first">

                            <?php else : ?>
                                <article class="article project">
                            <?php endif; ?>

                                <figure>

                                    <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">

                                        <?php

                                            $featuredImg = wp_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'medium', false, '' );

                                            if( $featuredImg == null ){
                                                echo '<img src="' . get_bloginfo( 'wpurl' ) . '/wp-content/themes/TPF/library/images/logo_feat_medium.png" />';
                                            } else {
                                                echo $featuredImg;
                                            }

                                        ?>

                                    </a>

                                </figure>

                                <header>

                                    <h4>
                                        <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                            <?php the_title(); ?>
                                        </a>
                                    </h4>

                                    <span class="author">
                                        Organization:
                                        <a href="<?php bloginfo( 'url' ); ?>/my-tpf/<?php echo get_the_author_meta( 'user_login' ); ?>/profile">
                                            <?php echo get_the_author(); ?>
                                        </a>

                                    </span>

                                </header>

                                <div class="content">

                                    <p>
                                        <?php

                                            $goal_values = get_post_custom_values('goal');

                                            foreach ( $goal_values as $key => $value ) {

                                                //echo "$value";
                                                echo ttruncat( $value, 225 );
                                            }

                                        ?>
                                    </p>
                                </div><!-- end .content -->

                                <footer>

                                    <?php

                                        $start_values = get_post_custom_values('timeline_start');
                                        foreach ( $start_values as $key => $value ) {

                                            if( isset($value[0]) ){

                                                echo '<p>';

                                                echo '<span>Timeline:</span> ';
                                                echo "$value ";
                                                echo ' - ';

                                            }

                                        }

                                    ?>


                                    <?php

                                        $end_values = get_post_custom_values('timeline_end');
                                        foreach ( $end_values as $key => $value ) {

                                            if( isset($value[0]) ){

                                                echo "$value ";
                                                echo '</p>';

                                            }
                                        }

                                    ?>

                                    <?php

                                        //echo '<section class="start-campaign-button">';

                                        //locate_template(array('members/single/campaigns/start.php'), true);

                                        //echo '</section>';

                                    ?>

                                </footer>

                            </article><!-- end .article -->

                        <?php endwhile; ?>

            <?php else: ?>
                <h2>No completed projects.</h2>
            <?php endif; ?>

                    </div><!-- end #tiles -->
                </section><!-- end #primary -->

            </div><!-- end .wrapper -->

    <?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

<?php endif; ?>
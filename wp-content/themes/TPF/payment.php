<?php
/**
 * Template Name: Payments
 *
 * Selectable from a dropdown menu on the edit page screen.
 *
 * This page also includes two important functions: Email admin on FundOwner donation and PostToProductID which converts the id in the referrecing link to a
 * wp-ecommerce product id that can be donated to.
 */
?>
<?php

 session_start();

 function PostToProductID($refererid, $campaignstatus) {
                            global $wpdb;

                            if ($campaignstatus == 'true') {
                                $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $refererid");
                                $campaignTitle = $campaign->project_title;
                                $user_info = get_userdata( $campaign->user_id );
                                $firstName = $user_info->first_name;
                                $post_exists = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE `meta_key` =  '_wpsc_sku' AND  `meta_value` =  '00'.$refererid'"); //Find any post with a matching SKU (SKU is the original post's ID)
                                if ($post_exists) // If it it exists, use the already created product ID
                                            {
                                                    $ogproductID = $post_exists[0]->post_id;
                                                    return $ogproductID; //Return originally created product ID
                                                } else {
                                                    $post = array(
                                                        'post_status'    => 'publish' ,
                                                        'post_title'     => 'Help '.$firstName.' raise money for '.$campaignTitle,
                                                        'post_type'      => 'wpsc-product',
                                                    );
                                                    $newproductID= wp_insert_post( $post );
                                                    add_post_meta($newproductID, '_wpsc_sku' , '00'.$refererid);
                                                    add_post_meta($newproductID, '_wpsc_is_donation', 1);
                                                    return $newproductID;
                                                    }

                            } elseif (get_post_type($refererid) == 'project') {
                            	$post_exists = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE `meta_key` =  '_wpsc_sku' AND  `meta_value` =  '.$refererid'");
                                  //Find any post with a matching SKU (SKU is the original post's ID)
                                    if ($post_exists) // If it it exists, use the already created product ID
                                            {
                                                    $ogproductID = $post_exists[0]->post_id;
                                                    return $ogproductID; //Return originally created product ID
                                                } else {
                                                    $post = array(
                                                        'post_status'    => 'publish' ,
                                                        'post_title'     => 'Donation To: '.get_the_title($refererid),
                                                        'post_type'      => 'wpsc-product',
                                                    );
                                                    $newproductID= wp_insert_post( $post );
                                                    add_post_meta($newproductID, '_wpsc_sku' , $refererid);
                                                    add_post_meta($newproductID, '_wpsc_is_donation', 1);
                                                    return $newproductID;
                                                    }
                            } else {
                                    $post_exists = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE `meta_key` =  '_wpsc_sku' AND  `meta_value` =  '99999'"); //Find any post with a matching SKU (SKU is the original post's ID)
                                    if ($post_exists) // If it it exists, use the already created product ID
                                            {
                                                    $ogproductID = $post_exists[0]->post_id;
                                                    return $ogproductID; //Return originally created product ID
                                                } else {
                                                    $post = array(
                                                        'post_status'    => 'publish' ,
                                                        'post_title'     => 'Donation To: Turkish Philanthopy Funds ',
                                                        'post_type'      => 'wpsc-product',
                                                    );
                                                    $newproductID= wp_insert_post( $post );
                                                    add_post_meta($newproductID, '_wpsc_sku' , '99999');
                                                    add_post_meta($newproductID, '_wpsc_is_donation', 1);
                                                    return $newproductID;
                                                    }
                            };
                        }


?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->
    <div class="wrapper">
        <div class="column">
            <section id="primary">
                <div class="donatecontent">

                    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                         <?php the_content(); ?>
                    <?php endwhile; ?>
                </div>
                <div class="donateside">
                    <h6>Make a one time donation...</h6>
                    <div class="donationbg">

               <?php
                 global $current_user;
                 get_currentuserinfo();
                    if (current_user_can('fundowner')) { //Email admin if a fundowner attempts a donation
                        if (isset($_POST['submitted'])) {
                            $fundowner_donation = $_POST['fundowner_donation_price'];
                            $fundowner_productid = $_POST['fundowner_productid'];
                            echo '<span class="cart_message">As a Fund Owner, your donation requests are sent to TPF for approval before the amount can be credited.</span>';
                            //get_bloginfo('admin_email'); // Get blog admin email
                            $to = get_settings('admin_email');
                            $subject = "www.tpfund.org: Fund Owner donation";
                            $message = "A Fund Owner has requested a donation be made: "."\n".
                                    "Fund Owner: ".$current_user->user_login."\n".
                                    "Fund Owner Email: ".$current_user->user_email."\n".
                                    "Destination: ".get_the_title($fundowner_productid)."\n".
                                    "ID: ".$fundowner_productid."\n".
                                    "Donation Amount: ".$fundowner_donation
                                    ;


                            $from = "noreply@tpfund.org";
                            $headers = "From:" . $from . "\r\n";
                            $headers .= "MIME-Version: 1.0\r\n";
                            $headers .= "Content-type: text/plain; charset=utf-8\r\n";
                            $headers .="Content-Transfer-Encoding: 8bit";
                            if (mail($to,$subject,$message,$headers)) {
                                 echo "<br>";
                                 echo "A donation request has been sent and is pending TPF approval.";
                            } else {
                                echo "There was a problem with your donation request. Please contact info@tpfund.org";
                            };
                        } else {
                        $postid = $_GET['id'];
                        $productID = PostToProductID($postid, $_GET['campaign']);
                        ?>
                        <span class="cart_message">As a Fund Owner, your donation requests are sent to TPF for approval before the amount can be credited.</span>
                    	<div class="wpsc_default_product_list">
                            <div class="default_product_display all-categories group">
                                <h4 class="prodtitle entry-title"><?php echo get_the_title($productID); ?></h4>
                                <div class="productcol">
                                    <form class="product_form"  enctype="multipart/form-data" action="" method="post" name="fundowner_donation" id="fundowner_donation"
                                        <div class="wpsc_product_price" id="wpsc_donations">
                                            <label class="donatelabel" for="fundowner_donation_price">Donation: $</label>
                                            <input type="number" id="fundowner_donation_price" name="fundowner_donation_price" value="" size="6" style="width: 72px;"  />
                                        </div>
                                        <div class="wpsc_buy_button_container">
                                            <input type="submit" value="Add Donation" name="Buy" class="wpsc_buy_button" id="fundowner_submit_button"/>
                                        </div>
                                        <input type="hidden" name="fundowner_productid" id="fundowner_productid" value="<?php echo $productID; ?>" />
                                        <input type="hidden" name="submitted" id="submitted" value="true" />
                                    </form>
                                </div>
                           </div>
                        <?php
                        };
                    } else {
                        if (wpsc_cart_item_count() < 1){
                                $postid = $_GET['id'];
                                $productID = PostToProductID($postid, $_GET['campaign']);
                                //Save Session Data

                                echo apply_filters('the_content', '[wpsc_products product_id='.$productID.' ]');
                                $_SESSION['productid']= (float) $productID;
                                $_SESSION['postid']= (float) $_GET['id'];
                                $_SESSION['campaign']= $_GET['campaign'];

                                //echo 'Productid' . $_SESSION['productid']= (float) $productID . '<br/>';
                                //echo 'PostID' . $_SESSION['postid']= (float) $_GET['id'] . '<br/>';
                                //echo 'Capaign' . $_SESSION['campaign']= $_GET['campaign'] . '<br/>';
                        } else {
                            echo '<h4>Please complete your first donation before starting another.</h4>';
                        };
                        echo wpsc_shopping_cart();
                    };

                    if (!current_user_can('fundowner')){
                    ?>
                    </div>
                        <h6>... or make a monthly recurring donation!</h6>
                        <div class="recurringdonations donationbg">

                         <h4 class="prodtitle entry-title">Recurring <?php echo get_the_title($productID); ?></h4>
                         <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

                                            <!-- Identify your business so that you can collect the payments. -->
                                            <input type="hidden" name="business" value="senay@tpfund.org">

                                            <!-- Specify a Subscribe button. -->
                                            <input type="hidden" name="cmd" value="_xclick-subscriptions">
                                            <!-- Identify the subscription. -->
                                            <input type="hidden" name="item_name" value="<?php echo get_the_title($productID); ?>">
                                            <input type="hidden" name="item_number" value="<?php echo $_GET['id'] ?>">

                                            <!-- Set the terms of the regular subscription. -->
                                            <label class="donatelabel" for="a3">Monthly Donation: $</label>
                                            <input type="number" id="fundowner_donation_price" name="a3" value="" size="6" style="width: 72px;" />
                                            <input type="hidden" name="currency_code" value="USD">
                                            <input type="hidden" name="p3" value="1">
                                            <input type="hidden" name="t3" value="M">

                                            <!-- Set recurring payments until canceled. -->
                                            <input type="hidden" name="src" value="1">
                                            <input type="hidden" name="usr_manage" value="1">

                                            <!-- Display the payment button. -->

                                            <div class="wpsc_buy_button_container">
                                                <input type="submit" value="Add Monthly Donation" name="Buy" class="wpsc_buy_button" id="fundowner_submit_button"/>
                                            </div>

                            </form>
                    </div>
                        <?php } ?>

                </div>
            </section>

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

        </div> <!--- end .column -->
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php
/**
 * Template Name: Newsletter Main Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
            <a class="rss" href="<?php echo bloginfo('rss2_url'); ?>?post_type=newsletter">
                <img src="<?php echo bloginfo('wpurl'); ?>/wp-content/themes/TPF/library/images/rss.png" width="73" height="73"/>
                Subscribe
            </a>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
 
    <div class="wrapper">
        <div class="column">
            <section id="primary">

                <?php

                $slug = basename(get_permalink());

                $args = array(
                    'post_type'      => 'newsletter',
                    'post_status'    => 'publish',
                    'posts_per_page' => -1 // = all of 'em
                );

                $loop = new WP_Query( $args );

                while ( $loop->have_posts() ) : $loop->the_post();

                    ?>

                    <article class="newsletter">

                        <header>
                            <h3>
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </h3>

                            <?php
                            // TEXT: Newsletter Subheader
                            $subheader = get_post_custom_values('subheader');
                            foreach ( $subheader as $key => $value ) {

                                if( isset($value[0]) ){

                                    echo '<h6>';
                                    echo "$value ";
                                    echo '</h6>';

                                }


                            }
                            ?>

                        </header>

                        <div class="content">
                            <?php the_excerpt(); ?>
                        </div><!-- end .content -->

                        <footer>
                            <a href="<?php the_permalink(); ?>">Continue Reading &raquo;</a>

                            <?php
                            if( function_exists( 'attachments_get_attachments' ) ){
                                $attachments = attachments_get_attachments();
                                $total_attachments = count( $attachments );
                                if( $total_attachments ) : ?>
                                    <div class="download">
                                        Download the latest TPF newsletter:<br/>
                                        <?php for( $i=0; $i<$total_attachments; $i++ ) : ?>
                                        <a href="<?php echo $attachments[$i]['location']; ?>"><?php echo $attachments[$i]['title']; ?></a>
                                        <p>
                                            <?php echo $attachments[$i]['caption']; ?><br/>
                                            <strong>File Type:</strong> <?php echo $attachments[$i]['mime']; ?> <span>|</span> <strong>File Size:</strong> <?php echo $attachments[$i]['filesize']; ?>
                                        </p>
                                        <?php endfor; ?>
                                    </div><!-- end .download -->
                                    <?php endif; ?>
                                <?php } ?>

                        </footer>

                    </article><!-- end .newsletter -->

                    <?php
                endwhile;
                ?>


            </section><!-- end #primary -->

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>
            <aside id="secondary">

            </aside><!-- end #secondary -->
        </div><!-- end .wrapper -->
 
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
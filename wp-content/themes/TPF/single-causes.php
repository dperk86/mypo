<?php
/**
 * Template Name: Single Project
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <h1><?php the_title(); ?></h1>

    <?php

        $slug = basename(get_permalink());

        $args = array(
            'post_type'      => 'project',
            'post_status'    => 'publish',
            'posts_per_page' => -1, // = all of 'em
            'tax_query'      => array(

                array(
                    'taxonomy'   => 'causes',
                    'terms'      => $slug,
                    'field'      => 'slug'
                )

            ),
        );

        $loop = new WP_Query( $args );

        while ( $loop->have_posts() ) : $loop->the_post();

            echo '<article class="article project">';


                echo '<header>';

                echo '<h2>';
                    echo '<a href="';
                    the_permalink();
                    echo '" title="';
                    the_title_attribute();
                    echo '">';

                        the_title();
                    echo '</a>';
                echo '</h2>';

                echo '<span class="author">';
                    echo '<a href="';
                    echo get_author_posts_url(get_the_author_meta( 'ID' ));
                    echo '">';
                        bp_member_name();

                    echo '</a>';
                echo '</span>';

                echo '</header>';



                echo '<div class="content">';

                echo '<p>';
                $start_values = get_post_custom_values('goal');
                foreach ( $start_values as $key => $value ) {
                    echo "$value ";
                }
                echo '</p>';


                echo '</div>';


                echo '<footer>';


                $start_values = get_post_custom_values('timeline_start');
                foreach ( $start_values as $key => $value ) {

                    if( isset($value[0]) ){

                        echo '<p>';

                        echo '<span>Timeline:</span> ';
                        echo "$value ";
                        echo ' - ';

                    }

                }



                $end_values = get_post_custom_values('timeline_end');
                foreach ( $end_values as $key => $value ) {

                    if( isset($value[0]) ){

                        echo "$value ";
                        echo '</p>';

                    }
                }



                echo '</footer>';

            echo '</article>';

        endwhile;

    ?>


    <aside id="secondary">

    </aside><!-- end #secondary -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php
/**
 * Template Name: Reports Template
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
        <div class="column">

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

            <section id="primary" class="page project-reports">

                <h2>Project Reports</h2>

                <?php
                $args = array( 'post_type' => 'report', 'post_status' => 'publish',);
                $loop = new WP_Query( $args );

                if( !$loop->have_posts() ){
                    echo '<div>';
                        echo 'There are currently no reports to display.';
                    echo '</div>';
                } else{
                    echo '<div>';
                        while ( $loop->have_posts() ) : $loop->the_post();

                            $report_project = get_field('report_project');
                            $project_selection = get_post_meta($post->ID, 'project_selection', true);
                            $report_project_title = get_the_title($project_selection);
                            $report_date = date('F d, Y', strtotime(get_field('report_date')));
                            $report_title = get_the_title();
                            $report_ID = get_the_ID();

                                echo '<h5><a href="'.get_permalink( $report_ID ).'">Report: '.$report_title.'</a></h5>';
                                echo '<h6>Project: ' .$report_project_title. '</h6>';
                                echo $report_date;


                        endwhile;
                    echo '</div>';
                    wp_reset_query();
                }
                ?>

            </section><!-- end #primary -->
        </div>
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php

    if($_POST['action'] == 'post') {

        //Form data sent
        $fundraiserGuideEditor = stripslashes($_POST['fundraiser_guide_editor']);
        $fundraiserGuideColumnOne = stripslashes($_POST['fundraiser_guide_ColumnOne']);
        $fundraiserGuideColumnTwo = stripslashes($_POST['fundraiser_guide_ColumnTwo']);
        $fundraiserGuideColumnThree = stripslashes($_POST['fundraiser_guide_ColumnThree']);

        update_option('fundraiser_guide_editor', $fundraiserGuideEditor);
        update_option('fundraiser_guide_ColumnOne', $fundraiserGuideColumnOne);
        update_option('fundraiser_guide_ColumnTwo', $fundraiserGuideColumnTwo);
        update_option('fundraiser_guide_ColumnThree', $fundraiserGuideColumnThree);

?>
    <div class="updated">
        <p>
            <strong>
                <?php _e('Options saved.' ); ?>
            </strong>
        </p>
    </div><!-- end .updated -->

<?php

    } else {

        //Normal page display
        $fundraiserGuideEditor = get_option('fundraiser_guide_editor');
        $fundraiserGuideColumnOne = get_option('fundraiser_guide_ColumnOne');
        $fundraiserGuideColumnTwo = get_option('fundraiser_guide_ColumnTwo');
        $fundraiserGuideColumnThree = get_option('fundraiser_guide_ColumnThree');
    }


?>


    <div class="wrap">
        <h2>Edit Being a Fundraiser Rockstar Guide</h2>
    </div><!-- end .wrap -->

    <p>
        <form id="fundraiser_guide_form" name="fundraiser_guide_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

            <input type="hidden" name="action" value="post" />

            <h4>Embed Code</h4>
            <?php the_editor($fundraiserGuideEditor, 'fundraiser_guide_editor'); ?>

            <br/>
            <br/>

            <h4>Column One</h4>
            <?php the_editor($fundraiserGuideColumnOne, 'fundraiser_guide_ColumnOne'); ?>

            <br/>
            <br/>

            <h4>Column Two</h4>
            <?php the_editor($fundraiserGuideColumnTwo, 'fundraiser_guide_ColumnTwo'); ?>

            <br/>
            <br/>

            <h4>Column Three</h4>
            <?php the_editor($fundraiserGuideColumnThree, 'fundraiser_guide_ColumnThree'); ?>

            <br/>
            <br/>

            <input type="submit" name="Submit" value="<?php _e('Update Options') ?>" />

        </form><!-- end #postIDs -->
    </p>
<?php
/**
 * Template Name: Category
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php 
$the_page = get_query_var('paged');
$caturl = $_SERVER["REQUEST_URI"];
$catslug = explode('/',$caturl); 
$cat = get_category_by_slug($catslug[3]);
query_posts(array('posts_per_page'=>5, 'paged' => $the_page, 'cat' => $cat->cat_ID));


if ( have_posts() ): ?>
    <header id="hero">
        <div class="wrapper">
            <h1>Category Archive: <?php echo single_cat_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
    
    <div class="wrapper">
            <section id="primary" class="category">

                <ol>
                <?php while ( have_posts() ) : the_post(); ?>
                    <li>
                        <article class="article">

                            <header>

                                <h2>
                                    <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                        <?php the_title(); ?>
                                    </a>
                                </h2>
                                <time datetime="<?php the_time( 'Y-m-D' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>

                            </header>

                            <div class="content">
                                <?php the_content(); ?>
                            </div><!-- end .content -->

                        </article><!-- end .article -->
                    </li>

                <?php endwhile; ?>
                </ol>
                <?php posts_nav_link( '', 'Previous', 'Next' ); ?> 
                
                <?php else: ?>
                <h2>No posts to display in <?php echo single_cat_title( '', false ); ?></h2>
                <?php endif; ?>

            </section><!-- end #primary -->
    </div>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
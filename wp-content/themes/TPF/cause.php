<?php
/**
 * Template Name: Cause Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <div class="wrapper">
        <div id="primary" class="page">

            <?php query_posts( 'post_type=causes'); ?>

            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <header>
                <h2><?php the_title(); ?></h2>
            </header>

            <div class="content">
                <?php the_content(); ?>
            </div><!-- end .content -->

            <?php the_post_thumbnail(); ?>
            <?php endwhile; ?>

        </div><!-- end #primary -->
    </div><!-- end .wrapper -->
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php
/**
 * Template Name: Video Template
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
        <div class="column">

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

            <section id="primary" class="page">


                <?php echo apply_filters('the_content', '[tubepress mode="user" userValue="TurkishPhilanthropy" theme="youtube" playerLocation="shadowbox" orderBy="published" resultCountCap="16" title="true" views="false" length="false" thumbHeight="190" thumbWidth="140"]'); ?>


            </section><!-- end #primary -->
        </div>
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
<?php
/**
 * Template Name: Partners Main Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>


    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
    <div class="wrapper">
        <section id="primary">
            <?php if (have_posts()) while (have_posts()) : the_post(); ?>

            <div class="content">
                <?php the_content(); ?>
            </div><!-- end .content -->

            <?php endwhile; ?>

            <div id="partners">

                <h2>Current Partners</h2>

                <ul>
                    <?php

                        function partners(){

                            global $wpdb;

                            $partnerMeta        = 'partner';
                            $trustedPartnerMeta = 'trustedpartner';

                            //$args = array(
                            //    'blog_id'      => 1
                            //);

                            $users = get_users();

                            $i = 0;

                            foreach($users as $user){

                                $userID = $user->ID;
                                $userInfo = get_userdata($userID);

                                $userRoles = $userInfo->roles;

                                if( $userRoles[0] == $partnerMeta || $userRoles[0] ==  $trustedPartnerMeta ):

                                    $username = $userInfo->user_login;
                                    $userDisplayName = $userInfo->display_name;
                                    $userEmail = $userInfo->user_email;

                                    if( $username == 'tpfund' ):
                                        return;
                                    endif;

                                    if( $i % 5 === 0 ):
                                        echo '<li class="first">';
                                    else:
                                        echo '<li>';
                                    endif;

                                        echo '<figure>';
                                            echo '<a href="' . get_bloginfo('wpurl') . '/my-tpf/' . $username . '/profile">';
                                                echo bp_core_fetch_avatar ( array( 'item_id' => $userID, 'type' => 'full' ) );
                                            echo '</a>';
                                        echo '</figure>';
                                        echo '<p>';
                                            echo '<a href="' . get_bloginfo('wpurl') . '/my-tpf/' . $username . '/profile">';
                                                echo '<strong>' . $userDisplayName . '</strong>';
                                            echo '</a>';
                                        echo '</p>';
                                    echo '</li>';

                                    $i++;

                                endif;

                            }

                        }

                        partners();

                    ?>
                </ul>
            </div>

        </section><!-- end #primary -->
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

<?php if(isset($cart_messages) && count($cart_messages) > 0) { ?>
	<?php foreach((array)$cart_messages as $cart_message) { ?>
	  <span class="cart_message"><?php echo esc_html( $cart_message ); ?></span>
	<?php } ?>
<?php } ?>
<?php if(wpsc_cart_item_count() > 0): ?>
    <div class="shoppingcart">
	<table>
		</thead>
		<tbody>
		<?php while(wpsc_have_cart_items()): wpsc_the_cart_item(); ?>
			<tr>
					<td  class='product-name'><?php echo wpsc_cart_item_name(); ?></td>
					<td><?php echo wpsc_cart_item_price(); ?></td>
                    <td class="cart-widget-remove"><form action="" method="post" class="adjustform">
					<input type="hidden" name="quantity" value="0" />
					<input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
					<input type="hidden" name="wpsc_update_quantity" value="true" />
					<input type="submit" value="remove" />
				</form></td>
			</tr>
		<?php endwhile; ?>
		</tbody>
		<tfoot>
			<tr>
				<td id='cart-widget-links' class="checkoutbtn" colspan="5">
                    <h4>
					<a target="_parent" href="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" title="<?php esc_html_e('Checkout', 'wpsc'); ?>" class="gocheckout"><?php esc_html_e('Checkout', 'wpsc'); ?></a>
                    </h4>
                </td>
			</tr>
		</tfoot>
	</table>
	</div><!--close shoppingcart-->
<?php else: ?>
	<p class="empty">
    </p>
<?php endif; ?>

<?php
wpsc_google_checkout();


?>

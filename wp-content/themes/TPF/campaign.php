<?php
/**
 * Template Name: Campaign Template
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php get_template_parts( array( 'parts/campaign/html-header', 'parts/campaign/header' ) ); ?>


    <?php if(isset($_GET['action']) && isset($_GET['id']) && isset($_GET['slug'])) : ?>

    <?php
        function curPageURL() {
            $pageURL = 'http';
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }
    ?>

        <?php

                $id = $_GET['id'];
                $slug = $_GET['slug'];
                global $wpdb;
                global $bp;
                $project_url = get_bloginfo( 'url' ) . '/blog/project/' . $slug;
                $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $id");
        ?>

        <?php if($campaign) : ?>

            <?php

                $user_info      = get_userdata( $campaign->user_id );
                $user           = $user_info->user_login;
                $firstName      = $user_info->first_name;
                $lastName       = $user_info->last_name;
                $campaignID     = $campaign->id;
                $financialGoal  = $campaign->amount;
                // This is for the current Total of donations. This will be updated by paypal.
                $currentTotal   = $campaign->current_total;
                $projectID      = $campaign->project_id;
                $campaignUpdate = $campaign->update_message;
            ?>

            <header id="hero">
                <div class="wrapper">
                    <h1>
                        <?php if( $campaign->completed == 'completed' ) { ?>
                            <span>Raised $<?php echo $campaign->amount; ?></span>
                        <?php } else{ ?>
                            <span>Help
                                <?php
                                    if($firstName == null && $lastName== null){
                                        echo $user;
                                    } else {
                                        echo $firstName;
                                    }
                                ?>
                                raise $<?php echo $financialGoal; ?> for</span>
                        <?php } ?>
                        <?php echo $campaign->project_title; ?>
                    </h1>
                </div><!-- end .wrapper -->
            </header><!-- end #hero -->

            <div class="wrapper">

                <div id="primary">

                    <article id="campaign">

                        <?php if( $campaign->completed == 'completed' ) : ?>

                            <h5>This campaign has been completed!</h5>

                        <?php endif; ?>

                        <div class="info">
                            
                           
                            <?php 
                            if ($financialGoal>0 && $currentTotal>=0) {
                                $percent_complete = ($currentTotal/$financialGoal)*100; 
                            }
                            ?>

                            <?php if(isset($_GET) && isset($_GET['thank']) && $_GET['thank'] = true): ?>

                                <div id="thanks">
                                    <h4>Thank You For Your Donation!</h4>
                                </div>

                            <?php endif;  ?>

                                <div class="progress">
                                    <h5 class="raise-amount">My Campaign Goal: $<?php echo number_format($financialGoal); ?></h5>
                                    <span id="progress_bar">
                                        <?php if( $currentTotal == NULL ){ ?>
                                            <span id="total"><?= sprintf("$0 raised", $currentTotal); ?></span>
                                        <?php } else if( $currentTotal > 0 ) { ?>
                                            <span id="total"><?= sprintf("$%s raised", number_format($currentTotal)); ?></span>
                                        <?php } ?>
                                        <span id="progress_raised" style="width:<?= sprintf("%s%%", $percent_complete) ?>"></span>
                                    </span>
                                </div><!-- end .progress -->
                        </div><!-- end .info -->

                        <div class="info">
                            <p>
                                <?php echo stripslashes($campaign->personal_message); ?>
                            </p>
                            <?php if( get_current_user_id() == $campaign->user_id ) : ?>
                                <section class="edit-campaign">
                                    <a href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $campaign->user_name; ?>/?component=campaigns&action=edit-campaign&id=<?php echo $campaign->id; ?>" class="button">Edit Your Personal Message.</a>
                                </section>
                            <?php endif; ?>
                        </div><!-- end .info -->

                        <div class="info">
                            <h5>Goal of the Project</h5>
                            <strong>Organization: </strong>
                                <a href="<?php echo $campaign->partner_url; ?>">
                                    <?php echo $campaign->partner; ?>
                                </a>
                            <p>

                                <?php

                                    if( $projectID == 0 ):

                                        echo stripslashes($campaign->project_content);

                                    else :

                                        // TEXT: The Goal of the Project
                                        $goal_values = get_post_custom_values('goal', $projectID);
                                        foreach ($goal_values as $key => $value) {

                                            if (isset($value[0])) {

                                                $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";

                                                echo '<div>';

                                                echo "$new_meta_value ";

                                                echo '</div>';
                                            }
                                        }

                                    endif;
                                ?>

                            </p>
                            <?php if( $projectID == 0 ): ?>
                                <a href="<?php echo $campaign->project_url; ?>">Learn more about this project &raquo;</a>
                            <?php else: ?>
                                <a href="<?php echo get_permalink( $projectID ); ?>">Learn more about this project &raquo;</a>
                            <?php endif; ?>
                        </div><!-- end .info -->

                        <?php

                                if( $campaignUpdate != NULL ):

                                    echo '<h5>Campaign Update</h5>';

                                    $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $campaignUpdate ) ) . "</p>";

                                    echo '<div>';
                                        echo $new_meta_value;
                                    echo '</div>';

                                endif;

                        ?>

<!--                        <div class="info">-->
<!--                            <h5>My Photos:</h5>-->
<!--                            <p>User photos go here.</p>-->
<!--                        </div><!-- end .info -->

                        <div class="info">
                            <h5>My Videos:</h5>


                            <?php

                                // GALLERY: Video Gallery
                                function campaignVideo($campaignID){
                                    global $wpdb;

                                    $campaignVideos = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE meta_key = 'cf_campaign_id'");

                                    if($campaignVideos){
                                        foreach($campaignVideos as $campaignVideo){

                                            if ($campaignVideo->meta_value == $campaignID && get_post_status( $campaignVideo->post_id ) == 'publish' ){
                                                $yt_url     = get_post_meta($campaignVideo->post_id, 'cf_youtubeID_field', true);
                                                $host       = parse_url($yt_url,  PHP_URL_HOST);
                                                $query      = parse_url($yt_url, PHP_URL_QUERY);
                                                $path       = parse_url($yt_url,  PHP_URL_PATH);
                                                $spiltPath  = substr($path, 1);
                                                $splitQuery = substr($query, 2);
                                                $videoID    = str_split($splitQuery, 11);

                                                if( $host == null ){
                                                    echo '</br><iframe width="420" height="315" src="http://www.youtube.com/embed/'.$yt_url.'" frameborder="0" allowfullscreen></iframe>';
                                                } elseif( $host == 'youtu.be' ){
                                                    echo '</br><iframe width="420" height="315" src="http://www.youtube.com/embed/'.$spiltPath.'" frameborder="0" allowfullscreen></iframe>';
                                                } elseif( $host == 'www.youtube.com' ){
                                                    echo '</br><iframe width="420" height="315" src="http://www.youtube.com/embed/'.$videoID[0].'" frameborder="0" allowfullscreen></iframe>';

                                                }

                                            }

                                        }
                                    }

                                }

                                campaignVideo($campaignID);

                            ?>


                            <?php
                                if( get_current_user_id() == $campaign->user_id ) :
                                    echo '<section class="add-video-button">';

                                    echo '<a class="button" href="new-video?&campaign_id='.$campaignID.'">Add A Video</a>';

                                    echo '</section>';
                                endif;
                            ?>
                        </div><!-- end .info -->

                        <?php if( $campaign->completed == '' ) : ?>

                            <h5>Share this campaign:</h5>
                            <div id="social">

                                <div id="shareme" data-url="<?php echo curPageURL(); ?>" data-text="Check out this @tphilanthropy campaign. Help <?php if($firstName == null && $lastName== null){ echo $user; } else { echo $firstName; } ?> raise $<?php echo $financialGoal; ?> for <?php echo $campaign->project_title; ?>"></div>

                                <a id="share-campaign" href="<?php echo get_bloginfo( 'wpurl' ) ?>/share-campaign-via-email-form?action=view&id=<?php echo $id; ?>&slug=<?php echo $slug; ?>" class="fancybox.ajax" type="ajax">Share via Email</a>
                                <p>
                                    Or copy and paste this link:
                                    <a href="<?php echo curPageURL(); ?>"><?php echo curPageURL(); ?></a>
                                </p>
                            </div><!-- end #social -->
                        <?php endif; ?>


                    </article>

                </div><!-- end #primary -->

                <aside id="secondary">

                    <?php
                        if( get_current_user_id() == $campaign->user_id ) :
                            if( $campaign->completed == '' ) :
                                echo '<form id="complete-campaigns" name="complete-campaign" method="post" action="' .get_bloginfo( 'url' ). '/my-tpf/' .$user .'/?component=campaigns&action=completed-campaign&id=' .$campaignID.'">';

                                    echo '<input type="checkbox" name="complete-campaign" value="completed"> ';
                                    echo '<label for="complete-campaign">If you have reached your campaign goal, mark this campaign complete</label><br/>';
                                    echo '<button id="submit" type="submit" name="save">Save</button>';

                                echo '</form>';

                            endif;
                        endif;
                    ?>

                    <?php if( get_current_user_id() == $campaign->user_id ) : ?>
                        <section class="edit-campaign">
                            <a href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $campaign->user_name; ?>/?component=campaigns&action=edit-campaign&id=<?php echo $campaign->id; ?>" class="button">Edit Campaign</a>
                        </section>
                    <?php endif; ?>

                    <section class="author-info">

                        <?php

                            $user = $user_info->user_login;

                            echo '<figure>';
                                echo '<a href="' .get_bloginfo( 'wpurl' ). '/my-tpf/' .$user. '/profile">';
                                    echo bp_core_fetch_avatar ( array( 'item_id' => $campaign->user_id, 'type' => 'full' ) );
                                echo '</a>';
                            echo '</figure>';

                            if($firstName == null && $lastName== null){
                                echo '<h4>' .$user. '</h4>';
                            } else {
                                echo '<h4>' .$firstName . ' ' . $lastName. '</h4>';
                            }


                        ?>

                    </section><!-- end .author-info -->

                </aside>

                <?php else : ?>

                        <article id="campaign" class="error">
                            <p>This Campaign doesn't exist anymore.</p>
                        </article>

                    </div><!-- end #primary -->

            <?php endif; ?>

         <?php endif; ?>

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
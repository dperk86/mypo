<?php
    if( isset($_POST) ){

        //form validation vars
        $formok = true;
        $errors = array();

        //sumbission data
        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $date = date('d/m/Y');
        $time = date('H:i:s');

        //form data
        $name      = $_POST['name'];
        $email     = $_POST['email'];
        $yourname  = $_POST['your_name'];
        $youremail = $_POST['your_email'];
        $subject   = $_POST['subject'];
        $link      = $_POST['link'];
        $message   = stripslashes($_POST['message']);

        //validate form data

        //validate name is not empty
        if(empty($name)){
            $formok = false;
            $errors[] = "You have not entered a name";
        }

        //validate email address is not empty
        if(empty($email)){
            $formok = false;
            $errors[] = "You have not entered an email address";
            //validate email address is valid
        }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $formok = false;
            $errors[] = "You have not entered a valid email address";
        }

        //validate name is not empty
        if(empty($yourname)){
            $formok = false;
            $errors[] = "You have not entered a name";
        }

        //validate email address is not empty
        if(empty($youremail)){
            $formok = false;
            $errors[] = "You have not entered an email address";
            //validate email address is valid
        }elseif(!filter_var($youremail, FILTER_VALIDATE_EMAIL)){
            $formok = false;
            $errors[] = "You have not entered a valid email address";
        }

        //validate message is not empty
        if(empty($message)){
            $formok = false;
            $errors[] = "You have not entered a message";
        }
        //validate message is greater than 20 charcters
        elseif(strlen($message) < 20){
            $formok = false;
            $errors[] = "Your message must be greater than 20 characters";
        }

        //send email if all is ok
        if($formok){

            $baseURL          = $_SERVER['HTTP_HOST'];

            $headers = "From: " .$youremail. "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $emailBody = "<table id='TPF' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' width='100%' style='width:100% !important; margin:0; padding:0;'>
                            <tr>
                                <td>
                                    <table id='email' width='500' cellpadding='0' cellspacing='0' border='0' align='center' style='margin-top:0; margin-bottom:0; margin-left:auto; margin-right:auto; padding:0;'>
                                        <tr>
                                            <td>
                                                <table id='header' width='550' cellpadding='0' cellspacing='0' border='0' style='margin:0; padding:0;'>
                                                    <tr>
                                                        <td valign='top' style='margin: 0; padding: 0;'>

                                                            <a href='http://www.tpfund.org' target='_blank' style='margin: 0; padding: 0; border: none;'>
                                                                <img src='http://{$baseURL}/wp-content/themes/TPF/library/images/email/logo.gif' width='500' height='105' alt='Turkish Philanthropy Funds' style='border: none; margin: 0; padding: 0;' />
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table id='email-body' width='500' cellpadding='0' cellspacing='0' border='0' style='margin:0; padding:0;'>
                                                    <tr>
                                                        <p style='font-size: 11px; line-height: 15px; font-family: Verdana, Arial, Sans-serif; color: #666666;'>Hi {$name}, </p>
                                                        <p style='font-size: 11px; line-height: 15px; font-family: Verdana, Arial, Sans-serif; color: #666666;'>{$message} </p>
                                                        <p style='font-size: 11px; line-height: 15px; font-family: Verdana, Arial, Sans-serif; color: #666666;'>Donate to this campaign: <br/><a href='{$link}' style='color: #4F81BD !important; text-decoration: none !important; border: none !important;'>{$link}</a></p>
                                                        <p style='font-size: 11px; line-height: 15px; font-family: Verdana, Arial, Sans-serif; color: #666666;'>{$yourname}</p>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table id='footer' width='500' cellpadding='0' cellspacing='0' border='0' style='margin:0; padding:0;'>
                                                    <tr>
                                                        <td>
                                                            <p style='font-size: 9px; line-height: 13px; font-family: Verdana, Arial, Sans-serif; color: #999999; text-align: center;'>
                                                                &copy; 2012 Turkish Philanthropy Funds. All rights reserved.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                          </table>";

                mail($email,$subject,$emailBody,$headers);

        }

        //what we need to return back to our form
        $returndata = array(
            'posted_form_data' => array(
                'name'       => $name,
                'email'      => $email,
                'your_name'  => $yourname,
                'your_email' => $youremail,
                'subject'    => $subject,
                'link'       => $link,
                'message'    => $message
                            ),
            'form_ok' => $formok,
            'errors'  => $errors
        );


        //if this is not an ajax request
        if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest'){
            //set session variables
            session_start();
            $_SESSION['cf_returndata'] = $returndata;

            //redirect back to form
            header('location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

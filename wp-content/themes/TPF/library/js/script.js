/* Author:
    Elif Bayrasli
    Patrick Murray

*/

jQuery(document).ready(function( $ ) {

    var $isProject           = $('body').hasClass('single-project'),
        $isProjects          = $('body').hasClass('projects'),
        $isCompletedProjects = $('body').hasClass('completed-projects'),
        $isXProfile          = $('body').hasClass('xprofile'),
        $isCampaign          = $('body').hasClass('campaign'),
        $isCampaigns         = $('body').hasClass('campaigns'),
        $isNewsletter        = $('body').hasClass('single-newsletter'),
        $isPost              = $('body').hasClass('single-post'),
        $isBlog              = $('header').hasClass('blog'),
        $isCauses            = $('body').hasClass('tax-causes'),
        $isStartCampaign     = $('body').hasClass('start-a-campaign'),
        $isCompleted         = $('body').hasClass('completed'),
        $isSocialMedia       = $('body').hasClass('social-media');


    $('input#printbutton').click(function(){
        w=window.open();
        w.document.write($('.content').html());
        w.print();
        w.close();
    });
    
  

    var textClicked = false;

    $('input#amountinputlabel').click(function(){
        textClicked = true;
    });
    
    $('.normal').click(function(){
        textClicked = false;
        $("#amountinput").attr("disabled", true);
        $("#amountinput").attr("value", 'Other Amount');
    });
    
    
    $('#wpsc_donations :input').click(function()
    {
        if (textClicked)
        {
            $("#amountinput").attr("disabled", false);
            $("#amountinput").attr("value", '');
        }
    });


    $("#donateAccordion").liteAccordion({
        containerWidth : 950,                   // fixed (px)
        containerHeight : 320,                  // fixed (px)
        headerWidth: 60,                        // fixed (px)
        "theme":"light",
        "rounded":true
    });

    $("#donateAccordion").show();


    $(document).on('click', '.gocheckout', function(e){
        e.preventDefault();
        var newLocation = $(this).attr('href') + window.location.search;
        window.location = newLocation;
    });

    function getQueryParams(qs) {
        qs = qs.split("+").join(" ");

        var params = {}, tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])]
                = decodeURIComponent(tokens[2]);
        }

        return params;
    }

    $('input[name="collected_data[22]"], input[name="collected_data[23]"]').parents('tr').addClass('hide');

    $('input[name="collected_data[22]"]').val(getQueryParams(window.location.search)['id']);
    $('input[name="collected_data[23]"]').val(getQueryParams(window.location.search)['campaign']);

    if( window.location.pathname === '/products-page/transaction-results/' ){
        setTimeout(function(){ window.location = window.location.origin; }, 4000);
    }

    function validate_currency() {
        var currency = document.getElementById("amount").value;
        var pattern = /^\d+(?:\.\d{0,2})$/ ;
        if (pattern.test(currency)) {
            return true;
        } 
        alert("Please enter an amount formatted for American currency: 00.00");
        return false;

    }
            
    $('#personalize_campaign #submit').bind('click', validate_currency);

    $('.fancybox, #photos li a, article.instagram figure a').fancybox();

    $("#share-campaign").fancybox({
        maxWidth    : 465,
        maxHeight   : 750,
        fitToView   : false,
        autoSize    : true,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });

    $("#videos li a").fancybox({
        'transitionIn'	: 'none',
        'transitionOut'	: 'none',
        'type'		    : 'iframe'
    });
    
    // Remove two classes from xprofile li if campaigns is selected
    if($('body').hasClass('xprofile')){

        if( $('#campaigns-personal-li').hasClass('current') || $('#projects-personal-li').hasClass('selected') || $('#photos-personal-li').hasClass('selected') ||  $('#videos-personal-li').hasClass('selected') ){

            $('li#xprofile-personal-li').removeClass('selected');
            $('li#xprofile-personal-li').removeClass('current');

        }

    }

    if( $('body.bp-user').hasClass('my-account') ){

        $('#settings-personal-li').show();

    } else {

        $('#settings-personal-li').hide();

    }


    if( $isProject || $isCampaign || $isNewsletter || $isPost ){


        $('#shareme').sharrre({
            share: {
                googlePlus: true,
                facebook: true,
                twitter: true
            },
            buttons: {
                googlePlus: {
                    size: 'tall',
                    annotation: 'bubble'
                },
                facebook: {layout: 'box_count'},
                twitter: {
                    count: 'vertical',
                    via: 'tphilanthropy'
                }
            },
            enableHover: false,
            enableCounter: false,
            enableTracking: true
        });

    }
    
    if($isBlog){


        $('.shareme').sharrre({
            share: {
                googlePlus: true,
                facebook: true,
                twitter: true
            },
            buttons: {
                googlePlus: {size: 'tall'},
                facebook: {layout: 'box_count'},
                twitter: {
                    count: 'vertical',
                    via: 'tphilanthropy'
                }
            },
            enableHover: false,
            enableCounter: false,
            enableTracking: true
        });

    }



    if( $isCauses || $isCampaigns || $isStartCampaign || $isProjects || $isCompletedProjects || $isCompleted || $isSocialMedia ){

        var $offset;
        var $itemWidth;
        var $container;

        if($isSocialMedia){
            $offset = 10;
            $itemWidth = 310;
            $container = $('#social-feed');
        } else {
            $offset = 0;
            $itemWidth = 320;
            $container = $('#tiles');
        }

        // Prepare layout options.
        var options = {
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: $container, // Optional, used for some extra CSS styling
            offset: $offset, // Optional, the distance between grid items
            itemWidth: $itemWidth // Optional, the width of a grid item
        };

        // Get a reference to your grid items.
        var handler = $('article.project, article.campaign, article.post');

        // Call the layout function.
        handler.wookmark(options);

        // Capture clicks on grid items.
        handler.click(function(){
            // Randomize the height of the clicked item.
            var newHeight = $('img', this).height() + Math.round(Math.random()*300+30);
            $(this).css('height', newHeight+'px');

            // Update the layout.
            handler.wookmark();
        });

    }


    $('.delete').click(function(e){
        e.preventDefault();

        var $answer = confirm('Do you Really want to do delete this Campaign?   -    ' + $(this).parents('article.campaign').find('.title').attr('title'));
        if ($answer == true) {
            window.location = $(this).parents('article.campaign').find('.delete').attr('href');
        } else {
            window.location;
        }

    });


    function validateCampaign(){

        var $form               = $('form.campaign_form'),
            $formElements       = $form.find('input[type="text"], textarea'),
            $amount             = $form.find('input#amount'),
            $amountValue        = $amount.val(),
            $personalMessage    = $form.find('textarea#projectmessage'),
            $personalMessageVal = $personalMessage.val(),
            $ckPersonalMess     = $('#cke_projectmessage iframe').contents().find('p'),
            $ckPersonalMessText = $ckPersonalMess.text(),
            $submit             = $form.find('button#submit'),
            $errorList          = $('ul#errorlist');


        if( $amountValue.length === 0 ){

            $submit.attr('disabled', 'disabled').addClass('disabled');

        } else if( $amountValue.length > 0 ){

            if( $submit.hasClass('disabled') ){

                $submit.removeAttr('disabled');
                $submit.removeClass('disabled');

            }

            if( $amount.val() < 500.00 ){

                // Disable submit button if amount is too low
                $submit.attr('disabled', 'disabled').addClass('disabled');

                $amount.addClass('error');

                // Add error message if amount is too low
                $errorList.append('<li class="low-amount">The amount you entered is too small.</li>');

            } else if( $amount.val() >= 500.00 ) {

                // Remove error message if amount is equal to or greater than 500.00
                $('.low-amount').remove();

                $amount.removeClass('error');

                // Enable submit button
                $submit.removeAttr('disabled');
                $submit.removeClass('disabled');

            }

        }


        $amount.change(function(){

            var $form            = $('form.campaign_form'),
                $amount          = $form.find('input#amount'),
                $personalMessage = $form.find('textarea#projectmessage'),
                $submit          = $form.find('button#submit'),
                $errorList       = $('ul#errorlist');

            if( $amount.val().length == 0 ){

                // Disable submit button if no amount is found
                $submit.attr('disabled', 'disabled').addClass('disabled');

                // Add error message if no amount is found
                $errorList.append('<li class="no-amount">You&#42891;ve not entered in an amount.</li>');

            } else if( $amount.val().length > 0 ){

                // Remove error message if an amount is typed in
                $('.no-amount').remove();

                if( $amount.val() < 500.00 ){

                    // Disable submit button if amount is too low
                    $submit.attr('disabled', 'disabled').addClass('disabled');

                    $amount.addClass('error');

                    // Add error message if amount is too low
                    $errorList.append('<li class="low-amount">The amount you entered is too small.</li>');

                } else if( $amount.val() >= 500.00 ) {

                    // Remove error message if amount is equal to or greater than 500.00
                    $('.low-amount').remove();

                    $amount.removeClass('error');

                    // Enable submit button
                    $submit.removeAttr('disabled');
                    $submit.removeClass('disabled');

                }

            }

        });


        $ckPersonalMess.change(function(){

            var $form            = $('form.campaign_form'),
                $amount          = $form.find('input#amount'),
                $personalMessage = $form.find('textarea#projectmessage'),
                $submit          = $form.find('button#submit'),
                $errorList       = $('ul#errorlist');


            if( $ckPersonalMess.text().length == 0 ){

               // Disable submit button if no message is found
               $submit.attr('disabled', 'disabled').addClass('disabled');

                // Add error message if no message is found
                $errorList.append('<li class="no-message">You&#42891;ve not entered in a personal message.</li>');

            } else if( $ckPersonalMess.text().length > 0 ){

                // Remove error message if an amount is typed in
                $('.no-message').remove();

            }


        });


        $formElements.change(function(){

            var $form               = $('form.campaign_form'),
                $amount             = $form.find('input#amount'),
                $personalMessage    = $form.find('textarea#projectmessage'),
                $ckPersonalMess     = $('#cke_projectmessage iframe').contents().find('p'),
                $ckPersonalMessText = $ckPersonalMess.text(),
                $submit             = $form.find('button#submit');

            if( $amount.val().length === 0 || $ckPersonalMessText.text().length === 0 ){

                $submit.attr('disabled', 'disabled').addClass('disabled');

            } else if( $amount.val().length > 0 && $ckPersonalMessText.text().length > 0 ){

                if( $submit.hasClass('disabled') ){

                    $submit.removeAttr('disabled');
                    $submit.removeClass('disabled');

                }

            }
        });


    }

    if( $('body').hasClass('profile') ){

        validateCampaign();

    }

});






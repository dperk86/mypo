<?php
/**
 * Template Name: Partner Eligibilty Template
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
acf_form_head();
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
 
    <div class="wrapper">
        <div class="column">
            <section id="primary">
                
                <?php
                if(have_posts()): while(have_posts()): the_post();

                echo '<p>';
                echo the_content();
                echo '</p>';                
                endwhile; endif;
                
                $options = array(
                    'post_id' => 'new-custom-post', // no post will be found and no default field groups will be loaded.
                    'field_groups' => array(2121), // this will find the field groups for this post (post ID's of the acf post objects)
                    'submit_value' => 'Submit Application', // value for submit field
                    'updated_message' => 'Thank you for your submission!', // default updated message. Can be false to show no message
                );
                
                acf_form($options); ?>

            </section><!-- end #primary -->

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>
        </div>
     </div><!-- end .wrapper -->
 
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>


<header id="header">

        <?php
        $post_meta = get_post_meta(get_the_ID());

        if ($post_meta['_wp_page_template'][0] == "campaign.php") {
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $id");
            }
        }
        ?>
    <div id="donate">
      <?php $params = array ('id' => get_the_ID(), 'campaign' => 'false' ); ?>
       <a href="<?php echo add_query_arg( $params , get_permalink( get_page_by_title( 'Donations' ) )); ?>">
        <img type="image"  src="<?php bloginfo('template_directory'); ?>/library/images/donate.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
          <input type="submit" value="<?php _e('Add To Cart', 'wpsc'); ?>" name="Buy" class="hidebuybutton" id="product_<?php echo wpsc_the_product_id(); ?>"/>					
        </a>
    </div>




    <div class="wrapper">

        <ul class="logocontainer">
            <li>
                <h1 id="logo">
                    <a href="/"><?php bloginfo('name'); ?></a>
                </h1>
            </li>
            <li class="logosubheader">
                 <a href="<?php echo get_permalink( get_page_by_title( 'infographic' ) );?>">Making a Difference, One Philanthropist at a time</a>
            </li>
        </ul>

        <div id="top_login">
            <?php
            if (!is_user_logged_in()) { // Display WordPress login form:
                $args = array(
                    'form_id' => 'loginform-custom',
                    'label_username' => __('Username'),
                    'label_password' => __('Password'),
                    'label_remember' => __('Remember Me'),
                    'label_log_in' => __('Log In'),
                    'remember' => false
                );
                wp_login_form($args);
            } else { // If logged in:
                global $current_user;
                echo '<div id="logged_in">';
                get_currentuserinfo();
                echo 'Welcome ' . $current_user->user_login . "! </br>";
                echo '<a href=';
                echo '/my-tpf/' . $current_user->user_login . ' /”> Profile </a> ';
                echo " | ";
                wp_loginout(get_permalink()); // Display "Log Out" link.
                //echo " | ";
                //
                            //wp_register('', ''); // Display "Site Admin" link.
                echo '</div>';
            }
            ?>

        </div>





        <!--            --><?php //get_search_form();    ?>

    </div><!-- end .wrapper -->

    <nav id="access" role="navigation">
        <?php wp_nav_menu(array('menu' => 'main_nav', 'menu_class' => 'wrapper')); ?>
    </nav><!-- #access -->

</header>

<section id="main">

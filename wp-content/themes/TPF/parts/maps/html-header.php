<!DOCTYPE HTML>
<!--[if IEMobile 7 ]><html class="no-js iem7 ie" manifest="default.appcache?v=1"><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6 ie" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7 ie" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8 ie" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9 ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
	<head>

		<title>
            <?php bloginfo( 'name' ); ?><?php wp_title( '|' ); ?>
        </title>
        

		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"><!-- Remove if you're not building a responsive site. (But then why would you do such a thing?) -->
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/library/images/favicon.ico"/>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css"/>

        <style type="text/css">
            html { height: 100% }
            body { height: 100%; margin: 0; padding: 0 }
            #map_canvas { height: 100% }
        </style>

        <?php wp_head(); ?>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAv4e6kBKeTH-QV-Ea76oaqyLlmW2H2Ikw&sensor=false"></script>
        <script type="text/javascript" src="<?php echo get_bloginfo('wpurl'). '/wp-content/themes/TPF/library/js/libs/infobox.js'; ?>"></script>
        <script type="text/javascript">

            <?php


                function projectAddress(){

                    global $wpdb;

                    $projects = $wpdb->get_results(" SELECT * FROM $wpdb->posts WHERE post_type = 'project' AND post_status = 'publish' ");


                    foreach( $projects as $project ){

                        $ID        = $project->ID;
                        $locations = get_field('location', $ID);

                        if( $locations ){

                            $coordinates  = get_post_meta($ID, 'coordinates', false);

                            if($coordinates){

                                foreach($coordinates as $coordinate){
                                    if($coordinate){
                                        foreach( $coordinate as $c ){
                                            echo $c. ',';
                                        }
                                    }
                                }

                            } else {

                                foreach($locations as $location){

                                    $province = $location['province'];
                                    $country  = $location['country'];

                                    if( $province !== '' && $country !== '' ){

                                        $address  = $province. ', ' .$country;

                                        $url     = sprintf('http://dev.virtualearth.net/REST/v1/Locations?q=%s&key=ApsdunJWnH0y5BOm-RhLUgub5TQe3S5ESjzEfuACZY0VJCcNzSsEmmMk5SxW_SDR',rawurlencode($address));

                                        $json    = file_get_contents( $url, 0, null, null );
                                        $results = json_decode($json);

                                        $locality  = $results->resourceSets[0]->resources[0]->address->locality;
                                        $latitude  = $results->resourceSets[0]->resources[0]->point->coordinates[1];
                                        $longitude = $results->resourceSets[0]->resources[0]->point->coordinates[0];

                                        echo "['" .$province. "', '" .$locality. "', ".$longitude. ", " .$latitude.  "],";

                                    }
                                }
                            }
                        }

                    }

                }

            echo 'var markersArray = [';
                projectAddress();
            echo '];';


        ?>

            function uniq(items, key){
                var set = {};
                return items.filter(function(item){
                    var k = key ? key.apply(item) : item;
                    return k in set ? false : set[k] = true;
                });
            }

            var markers = uniq(markersArray, [].join);

            var Turkey = new google.maps.LatLng(38.9637450, 35.2433220);
            var marker;
            var map;

            function initialize() {


                // Create an array of styles.
                var styles = [
                    {
                        stylers: [
                            { color: "#333333" },
//                            { saturation: 60 },
                            { lightness: 20 }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [
                            { visibility: "off" },
                        ]
                    }, {
                        featureType: "road",
                        elementType: "labels.text.fill",
                        stylers: [
                            { visibility: "simplified" },
                            { textColor: "#ffffff" }
                        ]
                    }, {
                        featureType: "water",
                        stylers: [
                            { color: "#4F81BD" },
                            { lightness: 50 }
                        ]
                    }, {
                        featureType: "landscape",
                        elementType: "geometry",
                        stylers: [
                            { color: "#333333" },
//                            { saturation: 60 },
                            { lightness: 20 },
                            { visibility: "simplified" }
                        ]
                    }, {
                        featureType: "administrative.province",
                        elementType: "geometry.stroke",
                        stylers: [
                            { visibility: "off" }
                        ]
                    }, {
                        featureType: "administrative",
                        elementType: "labels",
                        stylers: [
                            { visibility: "off" }
                        ]
                    }, {
                        featureType: "administrative.country",
                        elementType: "labels",
                        stylers: [
                            { visibility: "on" },
                            { color: "#ffffff" },
                            { weight: 1 }
                        ]
                    }, {
                        featureType: "administrative.country",
                        elementType: "geometry.stroke",
                        stylers: [
                            { color: "#bbbbbb" },
                            { weight: 1 }
                        ]
                    }, {
                        featureType: "administrative.country",
                        elementType: "labels.text.stroke",
                        stylers: [
                            { color: "#333333" },
                            { weight: 1 }
                        ]
                    }

                ];

                // Create a new StyledMapType object, passing it the array of styles,
                // as well as the name to be displayed on the map type control.
                var styledMap = new google.maps.StyledMapType(styles,
                        {name: "Styled Map"});


                var mapOptions = {
                    zoom: 6,
                    center: Turkey,
                    mapTypeControl: false

                };

                map = new google.maps.Map(document.getElementById("map_canvas"),
                        mapOptions);


                for( i = 0; i < markers.length; i++){


                    var image = '<?php echo get_bloginfo('wpurl'). '/wp-content/themes/TPF/library/images/marker.png'; ?>';

                    var myOptions = {
                        disableAutoPan: false,
                        maxWidth: 0,
                        pixelOffset: new google.maps.Size(-127, -115),
                        zIndex: null,
                        boxStyle: {
                            background: "url('<?php echo get_bloginfo('wpurl'). '/wp-content/themes/TPF/library/images/map-tooltip.png'; ?>') no-repeat",
                            width: "225px",
                            color: "#ffffff",
                            padding: "13px 15px 33px 15px",
                            textTransform: "uppercase",
                            textAlign: "center",
                            letterSpacing: "0.07em",
                            fontSize: "14px",
                            lineHeight: "16px",
                            fontFamily: "Helvetica, Arial, Sans-serif"
                        },
                        closeBoxMargin: "0 2px 10px 2px",
                        closeBoxURL: "",
                        infoBoxClearance: new google.maps.Size(1, 1),
                        isHidden: false,
                        pane: "floatPane",
                        enableEventPropagation: false
                    };


                    var ib = new InfoBox(myOptions);

                    var marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(markers[i][2], markers[i][3]),
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: image
                    });


                    google.maps.event.addListener(marker, 'mouseover', (function(marker, i){
                        return function(){

                            var projectProvince   = markers[i][0],
                                provinceName      = markers[i][1],
                                provinceLowerCase = projectProvince.toLowerCase(),
                                projectClass      = document.getElementsByClassName(provinceLowerCase);

                            var name;
                            if(provinceName === ''){
                                 name = projectProvince;
                            } else {
                                name = provinceName;
                            }

                            ib.setContent(name + '<br/><span style="font-size: 10px; color: #ccc; line-height: 12px;">' + projectClass.length + ' project(s)</span>');

                            ib.show();
                            ib.open(map, marker);

                        }
                    })(marker, i));


                    google.maps.event.addListener(marker, 'mouseout', (function(marker, i){
                        return function(){

                            var provinceName = markers[i][1];

                            ib.setContent(provinceName);
                            ib.hide();

                        }
                    })(marker, i));


                    google.maps.event.addListener(marker, 'click', (function(marker, i){
                        return function(){

                            var project           = document.getElementsByClassName('project'),
                                projectProvince   = markers[i][0],
                                provinceLowerCase = projectProvince.toLowerCase(),
                                projectClass      = document.getElementsByClassName(provinceLowerCase);

                                for( a = 0; a < project.length; a++ ){
                                    project[a].style.display='none';

                                    for( b = 0; b < projectClass.length; b++ ){
                                        projectClass[b].style.display='block';
                                    }
                                }



                        }
                    })(marker, i));

                }


                //Associate the styled map with the MapTypeId and set it to display.
                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');

            }


        </script>

	</head>

	<body onload="initialize()" <?php body_class(); ?>>

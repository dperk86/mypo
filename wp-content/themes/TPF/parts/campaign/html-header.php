<!DOCTYPE HTML>
<!--[if IEMobile 7 ]><html class="no-js iem7 ie" manifest="default.appcache?v=1"><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6 ie" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7 ie" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8 ie" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9 ie" lang="en"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#"><!--<![endif]-->
	<head>

    <?php if(isset($_GET['action']) && isset($_GET['id']) && isset($_GET['slug'])) : ?>

        <?php
            $id = $_GET['id'];
            $slug = $_GET['slug'];
            global $wpdb;
            global $bp;
            $project_url = get_bloginfo( 'url' ) . '/blog/project/' . $slug;
            $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $id");

        ?>

        <?php if($campaign) { ?>

            <?php

                $user_info          = get_userdata( $campaign->user_id );
                $user               = $user_info->user_login;
                $firstName          = $user_info->first_name;
                $lastName           = $user_info->last_name;
                $campaignID         = $campaign->id;
                $financialGoal      = $campaign->amount;
                // This is for the current Total of donations. This will be updated by paypal.
                $currentTotal       = $campaign->current_total;
                $description        = $campaign->project_content;

            ?>

            <title>Help <?php if($firstName->first_name == null && $lastName== null){ echo $user; } else { echo $firstName; } ?> raise $<?php echo $financialGoal; ?> for <?php echo $campaign->project_title; ?> | <?php bloginfo( 'name' ); ?></title>


            <meta property="og:title" content="Help <?php if($firstName->first_name == null && $lastName== null){ echo $user; } else { echo $firstName; } ?> raise $<?php echo $financialGoal; ?> for <?php echo $campaign->project_title; ?>"/>
            <meta property="og:type" content="non_profit"/>
            <meta property="og:url" content="<?php echo get_bloginfo('wpurl'); ?><?php echo $_SERVER['REQUEST_URI']; ?>" />

            <?php

//                $partner = $campaign->partner;
//                $partnerURL = $campaign->partner_url;
//                $partnerBase = parse_url($partnerURL, PHP_URL_PATH);
//                $partnerLogin = substr($partnerBase, 8, -8);
//                $getUser = get_user_by('login', $partnerLogin);
//                $getUserID = $getUser->ID;
//                $avatarURL = bp_core_fetch_avatar ( array( 'item_id' => $getUserID, 'type' => 'full', 'width' => 200, 'height' => 200, 'html' => false ) );
//                $avatarURLParse = parse_url($avatarURL, PHP_URL_QUERY);
//                $avatarURLSubstr = substr($avatarURLParse, 2, -18);
            ?>

            <?php //if ( $avatarURLSubstr == get_bloginfo('wpurl').'/wp-content/plugins/buddypress/bp-core/images/mystery-man.jpg' ){ ?>


            <?php //} else { ?>
            <?php //} ?>
            <meta property="og:image" content="<?php echo get_bloginfo('wpurl'); ?>/wp-content/themes/TPF/library/images/logoOG_tpf.png" />
            <meta property="og:site_name" content="Turkish Philanthropy Funds" />
            <meta property="fb:admins" content="27601544" />
            <meta property="og:description" content="<?php echo $description; ?>"/>

        <?php } else { ?>

            <title><?php bloginfo( 'name' ); ?><?php wp_title( '|' ); ?></title>

            <meta property="og:title" content="<?php wp_title(); ?>"/>
            <meta property="og:type" content="non_profit"/>
            <meta property="og:url" content="<?php echo get_bloginfo('wpurl'); ?><?php echo $_SERVER['REQUEST_URI']; ?>" />
            <meta property="og:image" content="<?php echo get_bloginfo('wpurl'); ?>/wp-content/themes/TPF/library/images/logoOG_tpf.png" />
            <meta property="og:site_name" content="Turkish Philanthropy Funds" />
            <meta property="fb:admins" content="27601544" />
            <meta property="og:description" content=""/>

        <?php } ?>

    <?php endif; ?>
        

		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"><!-- Remove if you're not building a responsive site. (But then why would you do such a thing?) -->
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/library/images/favicon.ico"/>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css"/>
        <link rel="canonical" href="<?php echo get_bloginfo('wpurl'); ?><?php echo $_SERVER['REQUEST_URI']; ?>">
  
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

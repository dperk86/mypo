
    </section><!-- end #main -->
	<footer id="footer">
        
        <div class="wrapper">

            <nav class="footer-links">
                <?php wp_nav_menu( array('menu' => 'footer' )); ?>
            </nav><!-- end .footer-links -->
            
            <?php 
                if ( dynamic_sidebar('footer_right') ) : 
                else : 
            ?>
            <?php endif; ?>
            
		    <p class="copy">&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?>. All rights reserved.</p>
        </div><!-- end .wrapper -->

	</footer><!-- end #footer -->

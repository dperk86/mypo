<?php
/**
 * Template Name: Front-Page
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <!-- CONTENT SLIDER -->

    <section id="hero">


        <div class="wrapper">
            <?php tpf_content_gallery(); ?>
        </div><!-- end .wrapper -->

    </section><!-- end #hero -->

    <!-- END: CONTENT SLIDER -->

        <!-- VIDEOS -->

        <section id="latestvideo_container">

            <div class="overlay">

                <div class="wrapper">

                    <div id="latestvideo_header">
                        <h2>Latest <span class="blue">Videos</span></h2>
                       <a href="http://www.youtube.com/user/TurkishPhilanthropy" target="_blank"> <h4>See All &raquo;</h4></a>
                    </div>
                    <?php echo apply_filters('the_content', '[tubepress mode="user" userValue="TurkishPhilanthropy" theme="youtube" playerLocation="shadowbox" orderBy="published" resultCountCap="4" title="false" views="false" length="false" thumbHeight="190" thumbWidth="140"]'); ?>

                </div><!-- end .wrapper -->

            </div>

        </section><!-- end #latestvideo_container -->

        <!-- END: VIDEOS -->

        <!-- FEATURED NAV -->

        <div class="wrapper">

            <section id="primary">

                <!-- CAUSES -->

                <div id="featured">

                         <?php
                         wp_nav_menu( array(
                            'menu' => 'featured',
                            'container' =>false,
                            'menu_class' => 'featured_spot',
                            'echo' => true,
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'depth' => 0,
                            'walker' => new description_walker())
                            );                                      
                         ?>

                </div><!-- end #featured -->

                <!-- END: FEATURED NAV -->

                <!-- Infographics -->

                <div id="infographic">

                    <?php

                        function causeCount(){
                            global $wpdb;

                            $causes = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'causes' ");
                            echo count($causes);
                        }

                        function projectCount(){
                            global $wpdb;

                            $projects = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_type = 'project' AND post_status = 'publish' ");
                            echo count($projects);
                        }

                        function partnerCount(){

                            global $wpdb;

                            $partnerMeta         = 'a:1:{s:7:"partner";s:1:"1";}';
                            $trustedPartnerMeta  = 'a:1:{s:14:"trustedpartner";s:1:"1";}';

                            $partners            = $wpdb->get_results(" SELECT user_id FROM $wpdb->usermeta WHERE meta_value = '$partnerMeta' ");
                            $trustedPartners     = $wpdb->get_results(" SELECT user_id FROM $wpdb->usermeta WHERE meta_value = '$trustedPartnerMeta' ");

                            $partnerCount        = count($partners);
                            $trustedPartnerCount = count($trustedPartners);
                            $officialCount       = $partnerCount + $trustedPartnerCount;

                            echo $officialCount;

                        }

                        function campaignCount(){

                            global $wpdb;

                            $campaigns = $wpdb->get_results("SELECT id FROM campaigns");

                            echo count($campaigns);

                        }


                    ?>

                    <h3><a href="<?php echo get_bloginfo('url'); ?>/partners"><span><?php partnerCount(); ?></span> Partners</a></h3>
                    <h3><a href="<?php echo get_bloginfo('url'); ?>/causes-page"><span><?php causeCount(); ?></span> Causes</a></h3>
                    <h3><a href="<?php echo get_bloginfo('url'); ?>/causes-page"><span><?php projectCount(); ?></span> Projects</a></h3>
                    <h3><a href="<?php echo get_bloginfo('url'); ?>/campaigns"><span><?php campaignCount(); ?></span> Campaigns</a></h3>
                    <h3 class="last"><span>Pick a project &#38;</span> <a href="<?php echo get_bloginfo('url'); ?>/the-philanthropist/start-a-campaign/">Start a Campaign</a> </h3>


                </div><!-- end #infographic -->

                <!-- end Infographics -->

                <div class="capture_email">
                    <h4>STAY INFORMED</h4>
                    <?php
                        $snsf_args = array(
                            "name"=>1,
                            "button"=>"sign up",
                            "thanks" => "Thank you for signing up."
                        );
                        echo do_newsletter($snsf_args);
                    ?>
                </div>

                <?php if (have_posts()) while (have_posts()) : the_post(); ?>

                    <div class="content">
                        <?php the_content(); ?>
                    </div><!-- end .content -->

                <?php endwhile; ?>

            </section><!-- end #primary -->

        </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>
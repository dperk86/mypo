<?php
/**
 * Template Name: Single Project
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


        <header id="hero">

            <div class="wrapper">

                <h1><?php the_title(); ?></h1>

            </div><!-- end .wrapper -->

            <?php

                function curl($url){
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    return $data;
                }

                // GALLERY: Project Gallery
                function project_gallery(){

                    global $post;
                    global $wpdb;

                    $projectID     = $post->ID;
                    $projectImages = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'attachment' AND post_mime_type = 'image/jpeg' AND post_parent = '$projectID'");
                    $youtubeRows   = get_field('youtube_videos');
                    $vimeoRows     = get_field('vimeo_videos');

                    if($youtubeRows || $projectImages || $vimeoRows ){

                        echo '<div id="iview">';

                            if($vimeoRows){

                                foreach($vimeoRows as $vimeoRow){

                                    $url     = $vimeoRow['cf_vimeoID_field'];
                                    $host    = parse_url($url);
                                    $path    = parse_url($url,  PHP_URL_PATH);
                                    $vimeoID = substr($path, 1);

                                    $videoFeed  = curl('http://vimeo.com/api/v2/video/' .$vimeoID. '.xml');
                                    $xml        = simplexml_load_string($videoFeed);
                                    $videoTitle = $xml->video->title;
                                    $videoImg   = $xml->video->thumbnail_large;

                                    echo '<figure data-iview:image="'.$videoImg.'" data-iview:type="video">';
                                        echo '<!-- Video iFrame -->';
                                        echo '<iframe src="http://player.vimeo.com/video/'.$vimeoID.'?byline=1&portrait=0" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                                        echo '<!-- Caption -->';
                                        echo '<figcaption class="iview-caption caption1" data-x="100" data-y="70" data-width="200" data-transition="expandLeft"><h2>Video</h2></figcaption>';
                                        echo '<figcaption class="iview-caption caption2" data-x="550" data-y="245" data-transition="wipeRight"><h5>'.$videoTitle.'</h5></figcaption>';
                                    echo '</figure>';

                                }

                            }

                            if($youtubeRows){

                                foreach($youtubeRows as $youtubeRow){
                                    $url        = $youtubeRow['cf_youtubeID_field'];
                                    $host       = parse_url($url,  PHP_URL_HOST);
                                    $path       = parse_url($url,  PHP_URL_PATH);
                                    $spiltPath  = substr($path, 1);
                                    $query      = parse_url($url, PHP_URL_QUERY);
                                    $splitQuery = substr($query, 2);
                                    $videoID    = str_split($splitQuery, 11);

                                    if( $host == null ){

                                        $videoFeed = curl('http://gdata.youtube.com/feeds/api/videos?v=2&q=' .$url. '&max-results=1&fields=entry(title)&prettyprint=true');
                                        $xml = simplexml_load_string($videoFeed);

                                        $videoTitle = $xml->entry->title;

                                        echo '<figure data-iview:image="http://img.youtube.com/vi/' .$url. '/0.jpg" data-iview:type="video">';
                                            echo '<!-- Video iFrame -->';
                                            echo '<iframe width="960" height="425" src="http://www.youtube.com/embed/'.$url.'" frameborder="0" allowfullscreen></iframe>';

                                            echo '<!-- Caption -->';
                                            echo '<figcaption class="iview-caption caption1" data-x="100" data-y="70" data-width="200" data-transition="expandLeft"><h2>Video</h2></figcaption>';
                                            echo '<figcaption class="iview-caption caption2" data-x="550" data-y="245" data-transition="wipeRight"><h5>'.$videoTitle.'</h5></figcaption>';
                                        echo '</figure>';

                                    } elseif( $host == 'youtu.be' ){

                                        $videoFeed = curl('http://gdata.youtube.com/feeds/api/videos?v=2&q=' .$spiltPath. '&max-results=1&fields=entry(title)&prettyprint=true');
                                        $xml = simplexml_load_string($videoFeed);

                                        $videoTitle = $xml->entry->title;

                                        echo '<figure data-iview:image="http://img.youtube.com/vi/' .$spiltPath. '/0.jpg" data-iview:type="video">';
                                            echo '<!-- Video iFrame -->';
                                            echo '<iframe width="960" height="425" src="http://www.youtube.com/embed/'.$spiltPath.'" frameborder="0" allowfullscreen></iframe>';

                                            echo '<!-- Caption -->';
                                            echo '<figcaption class="iview-caption caption1" data-x="100" data-y="70" data-width="200" data-transition="expandLeft"><h2>Video</h2></figcaption>';
                                            echo '<figcaption class="iview-caption caption2" data-x="550" data-y="245" data-transition="wipeRight"><h5>'.$videoTitle.'</h5></figcaption>';
                                        echo '</figure>';

                                    } elseif( $host == 'www.youtube.com' ){

                                        $videoFeed = curl('http://gdata.youtube.com/feeds/api/videos?v=2&q=' .$videoID[0]. '&max-results=1&fields=entry(title)&prettyprint=true');
                                        $xml = simplexml_load_string($videoFeed);

                                        $videoTitle = $xml->entry->title;

                                        echo '<figure data-iview:image="http://img.youtube.com/vi/' .$videoID[0]. '/0.jpg" data-iview:type="video">';
                                        //echo '<figure data-iview:image="http://img.youtube.com/vi/' .$videoID[0]. '/maxresdefault.jpg" data-iview:type="video">';
                                            echo '<!-- Video iFrame -->';
                                            echo '<iframe width="960" height="425" src="http://www.youtube.com/embed/'.$videoID[0].'" frameborder="0" allowfullscreen></iframe>';

                                            echo '<!-- Caption -->';
                                            echo '<figcaption class="iview-caption caption1" data-x="100" data-y="70" data-width="200" data-transition="expandLeft"><h2>Video</h2></figcaption>';
                                            echo '<figcaption class="iview-caption" data-x="550" data-y="245" data-transition="wipeRight"><h5>'.$videoTitle.'</h5></figcaption>';
                                        echo '</figure>';

                                    }
                                }

                            }


                            if($projectImages){

                                foreach($projectImages as $projectImage){
                                    $image = wp_get_attachment_image_src( $projectImage->ID, 'full' );
                                    echo '<figure data-iview:image="' .$image[0]. '" data-iview:transition="fade">';
                                    echo '</figure>';
                                }

                            }

                        echo "</div>";

                    }

                }

                echo '<div id="gallery">';
                    echo '<div class="wrapper">';
                        project_gallery();
                    echo '</div>';
                echo '</div>';

            ?>

        </header><!-- end #hero -->

        <div class="wrapper">

            <section id="primary">

                <article class="article project">

                    <div class="content">




                        <?php if(isset($_GET) && isset($_GET['thank']) && $_GET['thank'] = true): ?>

                        <div id="thanks">
                            <h4>Thank You For Your Donation!</h4>
                        </div>

                        <?php endif;  ?>

                        <div class="progress">

                    	<?php
                    	// do the math on manually entered donations
                    	$offline_donations = 0;
                    	$donors = get_field('donor');
						if($donors):
	                        foreach($donors as $donor):
	                        	$offline_donations = $offline_donations+$donor['donation_amount'];
	                        endforeach;
                        endif;
                        //echo 'offline donation total:' . $offline_donations;

						// call the e-donations so we can add those up
						$online_donatations = 0;

						$donations = get_donations_in_project(get_the_ID());
						foreach ($donations as $donation):
						// not really sure why all these specifc ids are removed from query, but rather than mess with original developer's intent - they were left in place
							if(strlen($donation->prodid) !== 0):

							if($donation->id !== '243' && $donation->id !== '244' && $donation->id !== '245' && $donation->id !== '246' && $donation->id !== '247' && $donation->id !== '248' && $donation->id !== '249'):

							$online_donatations = $online_donatations+$donation->totalprice;
							endif;
							endif;
						endforeach;

						//echo '<br/>online donation total:' . $online_donatations;

						// combine offline and online donations
						$total_donations = $online_donatations+$offline_donations;
						//echo '<br/>donation total:' . $total_donations;
						?>

						<?php
                            $meta_data        = get_post_meta(get_the_ID());
                            $current_total    = $meta_data['current_total'][0];
                            $goal             = $meta_data['financial_goal'][0];
                            //if ($goal>0 && $current_total>=0) {
                                //$percent_complete = ($current_total/$goal)*100;
                            if ($goal>0 && $total_donations>=0) {
                                $percent_complete = ($total_donations/$goal)*100;
                            }

                        ?>


                            <h5><?= sprintf("Financial Goal $%s", number_format($goal)); ?></h5>
                            <span id="progress_bar">
                                <span id="total"><?= sprintf("$%s raised", number_format($total_donations)); ?></span>
                                <span id="progress_raised" style="width:<?= sprintf("%s%%", $percent_complete) ?>"></span>
                            </span>
                        </div>
                        <?php
                        // TEXT: Problems addressed by the project
                        $problem_values = get_post_custom_values('problem_addressed');
                        foreach ($problem_values as $key => $value) {

                            if (isset($value[0])) {
                                $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";
                                echo '<div class="first">';

                                echo '<h5>The Problem Addressed by the Project</h5>';
                                echo html_entity_decode("$new_meta_value ");

                                echo '</div>';
                            }
                        }


                        // TEXT: Project Timeline
                        $start_values = get_post_custom_values('timeline_start');
                        foreach ($start_values as $key => $value) {

                            if (isset($value[0])) {

                                echo '<p class="timeline">';
                                echo '<span>Timeline:</span> ';
                                echo "$value ";
                                echo ' - ';
                            }
                        }



                        $end_values = get_post_custom_values('timeline_end');
                        foreach ($end_values as $key => $value) {

                            if (isset($value[0])) {

                                echo "$value ";
                                echo '</p>';
                            }
                        }




                        // TEXT: The Goal of the Project
                        $goal_values = get_post_custom_values('goal');
                        foreach ($goal_values as $key => $value) {

                            if (isset($value[0])) {

                                $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";

                                echo '<div>';

                                echo '<h5>The Goal of the Project</h5>';
                                echo "$new_meta_value ";

                                echo '</div>';
                            }
                        }



                        // TEXT: The Impact of the Project
                        $impact_values = get_post_custom_values('impact');
                        foreach ($impact_values as $key => $value) {

                            if (isset($value[0])) {

                                $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";

                                echo '<div>';

                                echo '<h5>The Impact of the Project</h5>';
                                echo "$new_meta_value ";

                                echo '</div>';
                            }
                        }

                        // TEXT: Updates from the Field
                        $updates_values = get_post_custom_values('updates');
                        foreach ($updates_values as $key => $value) {

                            if (isset($value[0])) {

                                $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";

                                echo '<div>';

                                echo '<h5>Updates from the Field</h5>';
                                echo "$new_meta_value ";

                                echo '</div>';
                            }
                        }




                        // TEXT: Additional Information
                        $info_values = get_post_custom_values('additional_information');
                        foreach ($info_values as $key => $value) {

                            if (isset($value[0])) {

                                $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";

                                echo '<div>';

                                echo "$new_meta_value ";

                                echo '</div>';
                            }
                        }


                        // REPORTS: Public reporting
                        function project_report(){

                            global $wpdb;

                            $reports    = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'report' AND post_status = 'publish'");
                            $project_ID = get_the_ID();

                                echo '<ul>';
                                    foreach($reports as $report){

                                          $reportID          = $report->ID;
                                          $project_selection = get_post_meta($reportID, 'project_selection', true);
                                          $reportDate        = get_post_meta($reportID, 'report_date', true);

                                        if($project_ID == $project_selection){

                                            echo '<h5>Public Reports</h5>';
                                            echo '<li>';
                                                echo '<a href="'.get_permalink($report->ID).'">';
                                                    echo 'Report: '.$report->post_title.' - '.$reportDate;
                                                echo '</a>';
                                            echo '</li>';

                                        }
                                    }
                                echo '</ul>';

                        }

                            project_report();


                            // DOCUMENTS: Public Documents
                            if( function_exists( 'attachments_get_attachments' ) ):
                                $attachments = attachments_get_attachments();
                                $total_attachments = count( $attachments );
                                if( $total_attachments ) :
                                    echo '<ul>';
                                        echo '<h5>Project Documents</h5>';
                                        for( $i=0; $i<$total_attachments; $i++ ) : ?>
                                            <li>
                                                <a href="<?php echo $attachments[$i]['location']; ?>"><?php echo $attachments[$i]['title']; ?></a>
                                                <span>
                                                    <?php echo $attachments[$i]['caption']; ?><br/>
                                                    <strong>File Type:</strong> <?php echo $attachments[$i]['mime']; ?> <span>|</span> <strong>File Size:</strong> <?php echo $attachments[$i]['filesize']; ?>
                                                </span>
                                            </li>
                                            <?php
                                        endfor;
                                    echo '</ul>';
                                endif;
                            endif;


                        ?>

                        <?

                            // TEXT: Donor List
//                            $donors_values = get_post_custom_values('donor_list');
//                            foreach ($donors_values as $key => $value) {
//
//                                if (isset($value[0])) {
//
//                                    $new_meta_value = "<p>" . implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $value ) ) . "</p>";
//
//                                    echo '<div>';
//
//                                    echo '<h5>Donor List</h5>';
//                                    echo html_entity_decode("$new_meta_value ");
//
//                                    echo '</div>';
//                                }
//                            }

                        ?>

                    </div><!-- end .content -->

                </article><!-- end .article -->

                <?php endwhile; ?>

                <?php

                    $donations = get_donations_in_project(get_the_ID());
                   //if(count($donations)):

                ?>
                        <div class="donors">
                            <h5>Thank You E-Donors!</h5>
                            <div class="list" >
                                <table>
                                    <thead>

                                        <tr>
                                            <th class="donor">Donor</th>
                                            <th class="donation">Donation</th>
                                            <th class="date">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        foreach ($donations as $donation):
                                            if(strlen($donation->prodid) !== 0):



                                                if($donation->id !== '243' && $donation->id !== '244' && $donation->id !== '245' && $donation->id !== '246' && $donation->id !== '247' && $donation->id !== '248' && $donation->id !== '249'):

                                                    $date = $donation->date;
                                                    $name = sprintf("%s %s", $donation->first_name, $donation->last_name);
                                                    $totalprice = $donation->totalprice;
                                        ?>
                                                    <tr>
                                                        <td class="donor"><?= $name ?></td>
                                                        <td class="donation">$<?= $totalprice ?></td>
                                                        <td class="date"><?= $date ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <div>
                <?php //endif;?>

                <?php $donors = get_field('donor'); ?>
                <?php if($donors): ?>
                    <div class="donors">
                        <h5>Thank You Donors!</h5>
                        <div class="list">
                            <table>
                                <thead>
                                <tr>
                                    <th class="donor">Donor</th>
                                    <th class="donation">Donation</th>
                                    <th class="date">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($donors as $donor):
                                            echo '<tr>';
                                                echo '<td class="donor">' .$donor['name']. '</td>';
                                                echo '<td class="donation">$' .$donor['donation_amount']. '</td>';
                                                echo '<td class="date">' .$donor['date']. '</td>';
                                            echo '</tr>';
                                        endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>


                <h5>Share this Project:</h5>
                <div id="social">
                    <div id="widget">
                        <h6>Share this widget:</h6>
                        <div id="tpf-widget-<?php echo get_the_ID(); ?>">
                            <style type="text/css"> #tpf-widget-body-<?php echo get_the_ID(); ?> { width: 215px; float: left; display: block; overflow: hidden; margin: 10px; transition: all 1s; -moz-transition: all 1s; -webkit-transition: all 1s; -o-transition: all 1s; } a#tpf-image-<?php echo get_the_ID(); ?>, a#tpf-image-<?php echo get_the_ID(); ?> img { border: none; } p#tpf-project-name-<?php echo get_the_ID(); ?> { font: 14px/1.4em Georgia, 'Times New Roman', sans-serif; letter-spacing: 1px; margin: 5px 0;} p#tpf-project-name-<?php echo get_the_ID(); ?> a, p#tpf-project-name-<?php echo get_the_ID(); ?> a:visited { color: #0098c9;} p#tpf-project-name-<?php echo get_the_ID(); ?> a:hover, p#tpf-project-name-<?php echo get_the_ID(); ?> a:active { color: #59b8d6; } #tpf-widget-body-<?php echo get_the_ID(); ?>:hover, #tpf-widget-body-<?php echo get_the_ID(); ?>:active { cursor: pointer; } #tpf-widget-body-<?php echo get_the_ID(); ?>:hover a#tpf-image-<?php echo get_the_ID(); ?> img, #tpf-widget-body-<?php echo get_the_ID(); ?>:active a#tpf-image-<?php echo get_the_ID(); ?> img { -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"; filter: alpha(opacity=70); opacity: 0.7; } #tpf-widget-body-<?php echo get_the_ID(); ?>:hover p#tpf-project-name-<?php echo get_the_ID(); ?> a, #tpf-widget-body-<?php echo get_the_ID(); ?>:active p#tpf-project-name-<?php echo get_the_ID(); ?> a { color: #59b8d6; } </style>
                            <div id="tpf-widget-body-<?php echo get_the_ID(); ?>"><a id="tpf-image-<?php echo get_the_ID(); ?>" href="<?php echo get_permalink(get_the_ID()); ?>"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/TPF/library/images/logo_widget.png" alt="TPF Donate" /></a><p id="tpf-project-name-<?php echo get_the_ID(); ?>"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php the_title(); ?></a></p></div><!-- end .tpf-widget-body -->
                        </div><!-- end #tpf-widget -->
                        <br/>
                        <label for="copy-text">Click in the text area, select, right click and copy text.</label><br/>
                        <textarea id="copy-text" name="copy-text" rows="2" cols="28">&lt;div id="tpf-widget-<?php echo get_the_ID(); ?>"&gt;&lt;style type="text/css"&gt; #tpf-widget-body-<?php echo get_the_ID(); ?> { width: 215px; float: left; display: block; overflow: hidden; margin: 10px; transition: all 1s; -moz-transition: all 1s; -webkit-transition: all 1s; -o-transition: all 1s; } a#tpf-image-<?php echo get_the_ID(); ?>, a#tpf-image-<?php echo get_the_ID(); ?> img { border: none; } p#tpf-project-name-<?php echo get_the_ID(); ?> { font: 14px/1.4em Georgia, 'Times New Roman', sans-serif; letter-spacing: 1px; margin: 5px 0;} p#tpf-project-name-<?php echo get_the_ID(); ?> a, p#tpf-project-name-<?php echo get_the_ID(); ?> a:visited { color: #0098c9;} p#tpf-project-name-<?php echo get_the_ID(); ?> a:hover, p#tpf-project-name-<?php echo get_the_ID(); ?> a:active { color: #59b8d6; } #tpf-widget-body-<?php echo get_the_ID(); ?>:hover, #tpf-widget-body-<?php echo get_the_ID(); ?>:active { cursor: pointer; } #tpf-widget-body-<?php echo get_the_ID(); ?>:hover a#tpf-image-<?php echo get_the_ID(); ?> img, #tpf-widget-body-<?php echo get_the_ID(); ?>:active a#tpf-image-<?php echo get_the_ID(); ?> img { -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"; filter: alpha(opacity=70); opacity: 0.7; } #tpf-widget-body-<?php echo get_the_ID(); ?>:hover p#tpf-project-name-<?php echo get_the_ID(); ?> a, #tpf-widget-body-<?php echo get_the_ID(); ?>:active p#tpf-project-name-<?php echo get_the_ID(); ?> a { color: #59b8d6; } &lt;/style&gt;&lt;div id="tpf-widget-body-<?php echo get_the_ID(); ?>"&gt;&lt;a id="tpf-image-<?php echo get_the_ID(); ?>" href="<?php echo get_permalink(get_the_ID()); ?>"&gt;&lt;img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/TPF/library/images/logo_widget.png" alt="TPF Donate" /&gt;&lt;/a&gt;&lt;p id="tpf-project-name-<?php echo get_the_ID(); ?>"&gt;&lt;a href="<?php echo get_permalink(get_the_ID()); ?>"&gt;<?php the_title(); ?>&lt;/a&gt;&lt;/p&gt;&lt;/div&gt;&lt;!-- end .tpf-widget-body --&gt;&lt;/div&gt;</textarea>
                    </div>
                    <div id="shareme" data-url="<?php echo get_permalink(get_the_ID()); ?>" data-text="<?php the_title(); ?>"><h6>Share this project via social networks:</h6></div>
                    <div id="link">
                        <h6>Or copy and paste this link:</h6>
                        <a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo get_permalink(get_the_ID()); ?></a>
                    </div>
                </div><!-- end #social -->

            </section><!-- end #primary -->


            <aside id="secondary">

                <?php

                    function campaignsPerProjectCount(){
                        global $wpdb;

                        $title = get_the_title();
                        $campaigns = $wpdb->get_results("SELECT id FROM campaigns WHERE project_title = '$title'");

                        if ( count($campaigns) == 0 ){

                            echo 'No one has started a campaign for this project. <strong>Start a campaign now!</strong>';

                        } else if ( count($campaigns) == 1 ){

                            echo '<span>' .count($campaigns). '</span> campaign has been created for this project. <strong>Start a campaign now!</strong>';

                        } else if ( count($campaigns) > 1 ){

                            echo '<span>' .count($campaigns). '</span> campaigns have been created for this project. <strong>Start a campaign now!</strong>';

                        }

                    }

                    echo '<div class="campaign-count">';
                        campaignsPerProjectCount();
                    echo '</div>';


                    echo '<section class="start-campaign-button">';
                        locate_template(array('members/single/campaigns/start.php'), true);
                    echo '</section>';


                ?>


                <section class="author-info">
                    <?php $authorID = $meta_data['project_author'][0]; ?>
                    <?php $authorInfo = get_userdata( $authorID ); ?>
                    <figure>
                        <a href="<?php bloginfo('url'); ?>/my-tpf/<?php echo $authorInfo->user_login; ?>/profile">
                            <?php echo bp_core_fetch_avatar(array('item_id' => $authorID, 'type' => 'full')); ?>
                        </a>
                    </figure><!-- end .author -->

                    <a href="<?php bloginfo('url'); ?>/my-tpf/<?php echo $authorInfo->user_login; ?>/profile">
                        <h4><?php echo $authorInfo->display_name; ?></h4>
                    </a>

                    <a href="<?php echo $authorInfo->user_url; ?>">
                        <?php echo $authorInfo->user_url; ?>
                    </a>

                </section><!-- end #author-info -->

            </aside><!-- end #secondary -->

        </div><!-- end .wrapper -->

<?php get_template_parts(array('parts/shared/footer', 'parts/shared/html-footer')); ?>
<?php
/**
 * Template Name: Social Media Template
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">

         <section id="primary" class="page">
            <?php if (have_posts()) : while (have_posts()) : the_post();?>

            <p>
                  <?php echo get_the_content(); ?>
            </p>
            <p>
                <?php tpf_social_media(); ?>
            </p>

            <?php endwhile; endif; ?>

        </section><!-- end #primary -->

    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
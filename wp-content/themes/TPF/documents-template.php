<?php
/**
 * Template Name: Documents Template
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
        <div class="column">

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

            <section id="primary" class="page documents">

               <?php 
                    if ( have_posts() ) while ( have_posts() ) : the_post();

                        $documents_blurb = get_field('documents_blurb');
                        $newsletter_blurb = get_field('documents_newsletter');
                        $annualreports_blurb = get_field('documents_annual');
                        $reports_blurb = get_field('documents_reports');


                        echo $documents_blurb;

                        echo '<h5><a href="'.get_permalink(93).'">'. ucwords(get_the_title(93)) .'</a></h5>';
                        echo '<p>';
                            echo $newsletter_blurb;
                        echo '</p>';

                        echo '<h5><a href="'.get_permalink(95).'">'. ucwords(get_the_title(95)) .'</a></h5>';
                        echo '<p>';
                            echo $annualreports_blurb;
                        echo '</p>';

                        echo '<h5><a href="'.get_permalink(97).'">'. ucwords(get_the_title(97)) .'</a></h5>';
                        echo '<p>';
                            echo $reports_blurb;
                        echo '</p>';


                    endwhile;
                ?>



            </section><!-- end #primary -->
        </div>
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
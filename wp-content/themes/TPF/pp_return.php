<?php

/**
 * Template Name: PayPal Confirmation
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>



<?php

if (!empty($_POST)) {
    $data = $_POST;
} else if (!empty($_GET)) {
    $data = $_GET;
}

if ($data['st'] == 'Completed') {

    $post_id = $data['item_number'];
    $amt = $data['amt'];

    $post_meta = get_post_meta($post_id);

    if (isset($_GET['is_campaign']) && $_GET['is_campaign']) {
        $id = $_GET['campaign_id'];
        $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $id");
        $slug = basename($campaign->project_url);

        global $wpdb;
        $currentTotal = $campaign->current_total + $amt;

        if ($wpdb->update('campaigns', array('current_total'=>$currentTotal), array('id'=>$id))) {
            wp_redirect(sprintf("%s/campaigns/campaign/?action=view&id=%s&slug=%s&%s=%s", get_bloginfo('wpurl'), $id, $slug, 'thank', 1));
        }else{
            print_r("didn't work");
        }
    } else {

        if (isset($post_meta['current_total'][0])) {
            $current_total = $post_meta['current_total'][0];
        } else {
            $current_total = 0;
        }

        $new_total = floatval($current_total) + $amt;

        if (update_post_meta($post_id, "current_total", $new_total)) {
            $url = sprintf("%s?%s=%s", get_permalink($post_id), 'thank', 1);
            wp_redirect($url);
        } else {
            print_r("figure out what to do here");
            print_r($data);
        }
    }
} else {
   wp_redirect(get_bloginfo('wpurl'));
}
?>
            

<?php
/**
 * Template Name: Three Column
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts(array('parts/shared/html-header', 'parts/shared/header')); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>

        <?php if( has_post_thumbnail() ) { ?>

            <div id="gallery">
                <figure class="wrapper">
                    <?php the_post_thumbnail(); ?>
                </figure><!-- end .wrapper -->
            </div><!-- end #gallery -->

        <?php } ?>

    </header><!-- end #hero -->

    <div class="wrapper">
        <section id="primary" class="page">

            <?php if (isset($_GET) && isset($_GET['thank']) && $_GET['thank'] = true): ?>

                <div id="thanks">
                    <h4>Thank You For Your Donation!</h4>
                </div>

            <?php endif; ?>


            <section id="column-one" class="column">

                <div class="content">
                    <?php echo get_field('column_one'); ?>
                </div><!-- end .content -->

            </section><!-- end #column-one -->

            <section id="column-two" class="column">

                <div class="content">
                    <?php echo get_field('column_two'); ?>
                </div><!-- end .content -->

            </section><!-- end #column-two -->

            <section id="column-three" class="column">

                <div class="content">
                    <?php echo get_field('column_three'); ?>
                </div><!-- end .content -->

            </section><!-- end #column-three -->

         </section><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

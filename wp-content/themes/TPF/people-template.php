<?php
/**
 * Template Name: People Main Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->
    <div class="wrapper">
        <div class="column">
            <section id="primary">
                <div id="people_page">

                    <?php
                    $tax_terms = get_terms('positions', 
                            array('parent' => 0, 
                                'hide_empty' => false));
                    
                    $tag_extra_fields = get_option(CAUSES_FIELDS);
                    
                    foreach ($tax_terms as $tax_term) {
                        $terms = get_terms('positions', 
                                array('parent' => $tax_term->term_id, 
                                    'hide_empty' => true));
                        
                        $tax_term_id = $tax_term->term_id;
                        $tax_term_name = $tax_term->name;
                        $tax_term_desc = $tax_term->description;
                        ?>
                        <div class="position_group">
                            <h5><?php echo preg_replace('/(\d+)/i', '', $tax_term_name); ?></h5>
                            <p><?php echo $tax_term_desc; ?></p>
                            <?php

                            if ($terms){
                                foreach ($terms as $term) {
                                    echo '<blockquote>';
                                    echo '<h6>'.$term->name.'</h6>';  
                                    echo '<p>'.$term->description.'</p>';
                                    ?>
                                    <div class="people_group">
                                        <?php
                                        $com_persons = query_posts(array(
                                            'post_type' => 'person',
                                            'showposts' => -1,
                                            'tax_query' => array(array(
                                                'taxonomy' => 'positions',
                                                'terms' => $term->term_id,
                                                'field' => 'term_id'
                                            )),
                                            'orderby' => 'title',
                                            'order' => ASC
                                        ));

                                        if ($com_persons) {
                                            echo '<ul>';
                                            foreach ($com_persons as $person){
                                                echo '<li><span><a href="'. get_permalink($person->ID) .'">'. preg_replace('/(\d+)/i', '', $person->post_title) . '</a></span></il>';
                                            }

                                            ?>
                                            </ul>
                                            <?php                        
                                        }
                                        ?>
                                    </div>
                            <?php
                                echo '</blockquote>';
                                }
                            } else { ?>
                               <div class="people_group">
                                        <?php
                                        $persons = query_posts(array(
                                            'post_type' => 'person',
                                            'showposts' => -1,
                                            'tax_query' => array(array(
                                                'taxonomy' => 'positions',
                                                'terms' => $tax_term_id,
                                                'field' => 'term_id'
                                            )),
                                            'orderby' => 'title',
                                            'order' => ASC
                                        ));

                                        if ($persons) {
                                            echo '<ul>';
                                            foreach ($persons as $person){
                                                echo '<li><span><a href="'. get_permalink($person->ID) .'">'. preg_replace('/(\d+)/i', '', $person->post_title) . '</a>';
                                                if ($person->post_excerpt) {
                                                    echo ' - '.$person->post_excerpt;
                                                }
                                                echo '</span></il>';
                                            }

                                            ?>
                                            </ul>
                                            <?php                        
                                        }
                                        ?>
                                    </div> 
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>



                </div><!-- end #people -->
            </section>

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

        </div> <!--- end .column -->
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
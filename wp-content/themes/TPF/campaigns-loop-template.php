<?php
/**
 * Template Name: Campaigns Loop Template
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
        <nav id="campaigns-nav" class="page-sub-nav">
            <div class="wrapper">
                <ul>
                    <li class="active"><a href="<?php echo get_bloginfo('url'); ?>/campaigns/">Active</a></li>
                    <li><a href="<?php echo get_bloginfo('url'); ?>/campaigns/completed/">Completed</a></li>
                </ul>
            </div><!-- end .wrapper -->
        </nav>
    </header><!-- end #hero -->

    <div class="wrapper">

        <div id="primary" class="category">

            <div id="tiles">

                <?php

                    //Display project data from wp database - campaigns
                    global $bp;
                    $campaigns = $wpdb->get_results( "SELECT * FROM campaigns WHERE completed = '' ORDER BY id DESC");

                    if($campaigns) :

                ?>

                    <?php foreach($campaigns as $campaign) : ?>

                        <?php
                            global $wpdb;

                            $user_info = get_userdata( $campaign->user_id );
                            $firstName = $user_info->first_name;
                            $lastName = $user_info->last_name;
                            $user = $user_info->user_login;

                        ?>

                        <article class="campaign">

                            <figure>

                                <a href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                    <?php
                                        global $wpdb;
                                        $slug = basename($campaign->project_url);
                                        $postID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$slug'");

                                        $featuredImg = wp_get_attachment_image( get_post_thumbnail_id( $postID ), 'medium', false, '' );

                                        if( $featuredImg == null ){
                                            echo '<img src="' . get_bloginfo( 'wpurl' ) . '/wp-content/themes/TPF/library/images/logo_feat_medium.png" />';
                                        } else {
                                            echo $featuredImg;
                                        }

                                    ?>

                                </a>

                            </figure>

                            <header>
                                <h4>
                                    <a href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>">
                                        <?php echo $campaign->project_title; ?>
                                    </a>
                                </h4>
                                <span class="author">Campaigner: <a href="<?php echo get_bloginfo('url'); ?>/my-tpf/<?php echo $campaign->user_name; ?>/profile"><?php echo $campaign->user_name; ?></a></span>
                            </header>

                            <div class="content">
                                <p>
                                    <?php echo ttruncat( $campaign->personal_message, 225 ); ?>
                                </p>
                            </div>
                            <p><strong>Raising: $<?php echo $campaign->amount; ?></strong></p>

                            <footer>
                                <a class="visit" href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>">
                                    Visit <?php if($firstName == null && $lastName == null){ echo $user . '&#39;s'; } else{ echo $firstName . ' ' . $lastName . '&#39;s'; } ?> campaign page &raquo;
                                </a>
                            </footer>

                        </article><!-- end .campaign -->

                    <?php endforeach; ?>

                <?php else : ?>

                        <h5>No active campaigns.</h5>

                <?php endif; ?>

            </div><!-- end #titles -->

        </div><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
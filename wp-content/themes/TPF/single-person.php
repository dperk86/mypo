<?php
/**
 * Template Name: Single Person
 *
 * @package     WordPress
 * @subpackage  Starkers
 * @since       Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>


<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->
    <div class="wrapper">

        <section id="primary" class="page">


            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

            <div class="person_content">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                }
                ?>
                <?php the_content(); ?>
            </div>


            <?php endwhile;
            echo '<a class="more" href="'.get_permalink(16).'" class="back">Back to '. ucwords(get_the_title(16)) .'</a>';

            ?>

        </section><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
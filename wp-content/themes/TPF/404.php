<?php
/**
 * Template Name: 404
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1>Page not found</h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
        <div class="column">

            <section id="primary" class="page">
                
                <h4>Our apologies!</h4>
                <p>The page you requested cannot be displayed.</p>

                

            </section><!-- end #primary -->
        </div>
    </div><!-- end .wrapper -->
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
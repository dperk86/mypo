<?php
/**
 * Template Name: Single Person
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>


<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
            <section id="primary" class="page">

                
                <?php 
                if ( have_posts() ) while ( have_posts() ) : the_post();
                $images = get_field('gallery_photos');
                    ?>
                    <div class='photo_gallery'>
                        <div class="photo_gallery_thumbs">
                        <?php
                        foreach ($images as $image){
                            $thumb = $image['gallery_photo']['sizes']['thumbnail'];
                            $photo = $image['gallery_photo']['sizes']['large'];
                            $title = $image['gallery_photo']['title'];
                            ?>
                            <a class="fancybox" href="<?php echo $photo; ?>" data-fancybox-group="gallery<?php the_ID(); ?>" title="<?php echo $title; ?>"><img class="gallerythumb" src="<?php echo $thumb; ?>" alt="" /></a>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <?php
                endwhile;
                    echo '<a class="more" href="'.get_permalink(89).'" class="back">Back to '. ucwords(get_the_title(89)) .'</a>';
                ?>

            </section><!-- end #primary -->
    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
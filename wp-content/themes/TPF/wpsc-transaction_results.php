<?php
	/**
	 * The Transaction Results Theme.
	 *
	 * Displays everything within transaction results.  Hopefully much more useable than the previous implementation.
	 *
	 * @package WPSC
	 * @since WPSC 3.8
	 */
?>

<div class="wpsc-transaction-results-wrap">
	<?php

        global $wpdb;

        $sessionID = $_GET['sessionid'];

        $purchase = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_purchase_logs WHERE sessionid = $sessionID");
        $purchaseID = $purchase->id;
        $totalPrice = $purchase->totalprice;

        $formDataProject = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_submited_form_data WHERE log_id = $purchaseID AND form_id = 22");
        $projectID = $formDataProject->value;
        $formDataCampaign = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_submited_form_data WHERE log_id = $purchaseID AND form_id = 23");
        $campaign = $formDataCampaign->value;

        if($campaign == 'true'):

            $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $projectID");
            $currentTotal = $campaign->current_total;
            $newTotal = floatval($currentTotal) + floatval($totalPrice);

            $wpdb->query("UPDATE campaigns SET current_total = $newTotal WHERE id = $projectID");

        else:

            $post = $wpdb->get_row("SELECT * FROM xzwgy7v_postmeta WHERE post_id = $projectID AND meta_key = 'current_total'");
            $currentTotal = $post->meta_value;

            $newTotal = floatval($currentTotal) + floatval($totalPrice);

            $wpdb->query("UPDATE xzwgy7v_postmeta SET meta_value = $newTotal WHERE post_id = $projectID AND meta_key = 'current_total'");

        endif;

        echo wpsc_transaction_theme();

        session_destroy();
    ?>
</div>
<?php
/**
 * Template Name: Blog
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero" class="blog">
        <div class="wrapper">
            <?php if( get_field('blog_title_image', 56) ){ ?>
               <h1><img src="<?php the_field('blog_title_image', 56); ?>" alt="" /></h1>
            <?php } else { ?>
                <h1><img src="<?php echo get_bloginfo('template_url'); ?>/library/images/philanthrolog.png" /></h1>
            <?php } ?>
            <a class="rss" href="<?php echo bloginfo('rss2_url'); ?>">
                <img src="<?php echo bloginfo('wpurl'); ?>/wp-content/themes/TPF/library/images/rss.png" width="73" height="73"/>
                Subscribe
            </a>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->
    
    <div class="wrapper">
            <div class="column">
            <section id="primary">

                <?php if ( have_posts() ): ?>

                    <?php $count = 0; ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php $count++; ?>
                    <?php if ($count == 1) : ?>

                        <article class="post first post-<?php the_ID(); ?>">

                    <?php else : ?>

                        <article class="post post-<?php the_ID(); ?>">

                    <?php endif; ?>
                        <header>
                            <h3><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                            <time datetime="<?php the_time( 'Y-m-D' ); ?>" pubdate><?php the_date(); ?></time>
                        </header>
                        <div class="content">
    <!--                        <figure>-->
    <!--                            --><?php //the_post_thumbnail( 'medium' ); ?>
    <!--                        </figure>-->
                            <?php //the_excerpt(); ?>
                            <?php the_content(); ?>

                        </div><!-- end .content -->
    <!--                    <a class="button" href="--><?php //esc_url( the_permalink() ); ?><!--" title="Permalink to --><?php //the_title(); ?><!--" rel="bookmark">-->
    <!--                        Continue Reading &rarr;-->
    <!--                    </a>-->
                        <footer>
                            <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
                            <div class="share">
                                <div class="shareme" data-url="<?php echo get_permalink(get_the_ID()); ?>" data-text="<?php the_title(); ?>"></div>
                            <!--            <div class="twitter" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="Tweet"></div>-->
                            <!--            <div class="facebook" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="Like"></div>-->
                            <!--            <div class="googleplus" data-url="--><?php //echo get_permalink(get_the_ID()); ?><!--" data-text="--><?php //the_title(); ?><!--" data-title="+1"></div>-->
                                    <div class="link">
                            </div><!-- end #share -->
                        </footer>

                    </article><!-- end .article -->
                <?php endwhile; ?>

                <div id="posts-nav-link">
                    <?php posts_nav_link( '', '&larr; Previous Page', 'Next Page &rarr;' ); ?>
                </div><!-- end #posts-nav-link -->

                <?php else: ?>

                <h2>No posts to display</h2>

                <?php endif; ?>

            </section><!-- end #primary -->

        <aside id="secondary">
            <?php get_template_parts( array( 'parts/shared/blognav') ); ?>
        </aside><!-- end #secondary -->

            </div>
    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>
<?php
/**
 * Template Name: Login Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div><!-- end .wrapper -->
    </header><!-- end #hero -->

    <div class="wrapper">

        <section id="primary">

            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

            <div class="message">
                <?php the_content(); ?>
            </div><!-- end .content -->

            <?php endwhile; ?>

            <div id="login-form">

                <?php
                if ( ! is_user_logged_in() ) { // Display WordPress login form:
                    $args = array(
                        'form_id' => 'loginform-custom',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in' => __( 'Log In' ),
                        'remember' => false
                    );
                    wp_login_form( $args );
                } else { // If logged in:
                    global $current_user;
                    echo '<div id="logged_in">';
                    get_currentuserinfo();
                    echo 'Welcome '. $current_user->user_login."! </br>";
                    echo '<a href=';
                    echo '/my-tpf/'.$current_user->user_login .' /”> Profile </a> ';
                    echo " | ";
                    wp_loginout( get_permalink() ); // Display "Log Out" link.
                    //echo " | ";
                    //
                    //wp_register('', ''); // Display "Site Admin" link.
                    echo '</div>';
                }
                ?>

            </div><!-- end #login-form -->

        </section><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
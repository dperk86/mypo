<?php

	/* ========================================================================================================================

	Required external files

	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );


    if(function_exists('register_field')){
        register_field('Users_field', dirname(__File__) . '/ACFaddons/users_field.php');
        }

    /* ========================================================================================================================

	Increase upload size

	======================================================================================================================== */



    @ini_set( 'upload_max_size' , '64M' );
    @ini_set( 'post_max_size', '64M');
    @ini_set( 'max_execution_time', '300' );




	/* ========================================================================================================================

	Actions and Filters

	======================================================================================================================== */

    // 01. Register Post Type - Projects / Reports
    add_action( 'init', 'projects' );
    add_action( 'init', 'reports', 0 );
    add_action( 'init', 'galleries', 0 );
    //add_action( 'init', 'statement');
    add_action( 'init', 'newsletter' );
    add_action( 'init', 'applicants' );



    // 02. Adding gallery metabox to projects custom post type
    add_action( 'be_gallery_metabox_post_types', 'be_gallery_metabox_project' );


    // 03.
    add_action( 'causes_edit_form_fields', 'edit_new_fields', 10, 2);


    // 04. when the form gets submitted, and the category gets updated
    add_action( 'edited_causes', 'save_causes', 10, 2);


    // 05. Register Post Type Videos
    add_action('init','video_register');



    // 06. Add Video MetaBox
    add_action('add_meta_boxes','video_meta_box_add');
    add_action('save_post', 'video_meta_box_save');


    // 07.  Adding Scripts
	add_action( 'init', 'script_enqueuer' );


    // 08. Remove style / scripts from header / footer
    add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );
    add_action( 'wp_print_styles', 'my_deregister_styles', 100 );


    // 09. Footer widget for contact info
    add_action( 'widgets_init', 'footer_widgets_init' );


    // 10. Adding theme support for TPF Content Gallery
    add_theme_support('post-thumbnails');


    // 11. Add Slug to Body Class
	add_filter( 'body_class', 'add_slug_to_body_class' );

    // 12. Adding Campaign buttons
    add_filter( 'the_tags', 'px_campaign_link' );

    // 13. Add menu item to profile page
    //add_action( 'bp_setup_nav', 'my_setup_nav' );

    // 14. Registering Sidebar
    register_sidebar();

    // 15. Campaign Page URL Rewrite
    add_rewrite_rule('^campaign/([^/]*)/([^/]*)/?','campaign.php?pagename=campaign&action=view&id=$matches[1]&slug=$matches[2]','top');

    // 16. Adding a Campaign ID input box for Adding A New Video for Campaigns
    add_action( 'wpuf_add_post_form_description', 'wpufe_campaign_video', 10, 2 );

    // 17. Complete Project Page URL Rewrite
    add_rewrite_rule('^causes-page/completed/([^/]*)/([^/]*)/?','causes-category/$matches[1]/completed/completed-projects-loop.php?pagename=completed-projects&catid=$matches[2]&slug=$matches[3]','top');

    remove_action('wp_head', 'rel_canonical');


	/* ========================================================================================================================

	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );

	======================================================================================================================== */



    // 01. Register Post Type - Projects
    function projects() {

        $labels = array(
            'name'               => _x('My Projects', 'post type general name'),
            'singular_name'      => _x('Project', 'post type singular name'),
            'add_new'            => _x('Add New', 'project'),
            'add_new_item'       => __('Add New Project'),
            'edit_item'          => __('Edit Project'),
            'new_item'           => __('New Project'),
            'all_items'          => __('All Projects'),
            'view_item'          => __('View Project'),
            'search_items'       => __('Search Projects'),
            'not_found'          => __('No Projects found'),
            'not_found_in_trash' => __('No Projects found in Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Projects')
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'rewrite'               => true,
            'capability_type'       => 'project',
            'capabilities'          => array(
                'edit_post'                 => 'edit_project',
                'edit_posts'                => 'edit_projects',
                'edit_private_posts'        => 'edit_private_projects',
                'edit_published_posts'      => 'edit_published_projects',
                'edit_others_posts'         => 'edit_others_projects',
                'publish_posts'             => 'publish_projects',
                'read_post'                 => 'read_projects',
                'read_private_posts'        => 'read_private_projects',
                'delete_posts'              => 'delete_projects',
                'delete_private_post'       => 'delete_private_projects',
                'delete_published_posts'    => 'delete_published_projects',
            ),

            'has_archive'   => true,
            'hierarchical'  => false,
            'menu_position' => null,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/megaphone.png',
            'supports'      => array( 'title', 'thumbnail' )
        );

        register_post_type('project', $args);

        register_taxonomy(
            'causes',
            'project',
            array(
                'hierarchical'      => true,
                'label'             => 'Causes',
                'singular_label'    => 'Cause',
                'query_var'         => true,
                'rewrite'           => array( 'slug' => 'causes-category' )
            )
        );

        flush_rewrite_rules();
    }

    // 02. Project - Image Gallery Metabox
    function be_gallery_metabox_project( $post_types ) {
        return array( 'project' );
    }


    function newsletter() {

        $labels = array(
            'name'               => _x('My Newsletters', 'post type general name'),
            'singular_name'      => _x('Newsletter', 'post type singular name'),
            'add_new'            => _x('Add New', 'newsletter'),
            'add_new_item'       => __('Add New Newsletter'),
            'edit_item'          => __('Edit Newsletter'),
            'new_item'           => __('New Newsletter'),
            'all_items'          => __('All Newsletters'),
            'view_item'          => __('View Newsletter'),
            'search_items'       => __('Search Newsletters'),
            'not_found'          => __('No Newsletters found'),
            'not_found_in_trash' => __('No Newsletter found in Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Newsletter')
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'rewrite'               => true,
            'capability_type'       => 'newsletter',
            'capabilities'          => array(
                'edit_post'                 => 'edit_newsletter',
                'edit_posts'                => 'edit_newsletters',
                'edit_private_posts'        => 'edit_private_newsletters',
                'edit_published_posts'      => 'edit_published_newsletters',
                'edit_others_posts'         => 'edit_others_newsletters',
                'publish_posts'             => 'publish_newsletters',
                'read_post'                 => 'read_newsletters',
                'read_private_posts'        => 'read_private_newsletters',
                'delete_posts'              => 'delete_newsletters',
                'delete_private_post'       => 'delete_private_newsletters',
                'delete_published_posts'    => 'delete_published_newsletters',
            ),

            'has_archive'   => true,
            'hierarchical'  => false,
            'menu_position' => null,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/newspapers.png',
            'supports'      => array( 'title', 'editor', 'excerpt' )
        );

        register_post_type('newsletter', $args);

        flush_rewrite_rules();
    }


    add_action('add_meta_boxes','subheader_metabox');
    function subheader_metabox() {
        add_meta_box('subheader', 'Sub Header', 'subheader', 'newsletter', 'normal', 'high');
    }


    //Applicants
    function applicants() {

        $labels = array(
            'name'               => _x('My Applicants', 'post type general name'),
            'singular_name'      => _x('Applicants', 'post type singular name'),
            'add_new'            => _x('Add New', 'applicant'),
            'add_new_item'       => __('Add New Applicant'),
            'edit_item'          => __('Edit Applicant'),
            'new_item'           => __('New Applicant'),
            'all_items'          => __('All Applicants'),
            'view_item'          => __('View Applicant'),
            'search_items'       => __('Search Applicants'),
            'not_found'          => __('No Applicants found'),
            'not_found_in_trash' => __('No Applicant found in Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Applicants')
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'rewrite'               => true,
            'capability_type'       => 'applicant',
            'capabilities'          => array(
                'edit_post'                 => 'edit_applicant',
                'edit_posts'                => 'edit_applicants',
                'edit_private_posts'        => 'edit_private_applicants',
                'edit_published_posts'      => 'edit_published_applicants',
                'edit_others_posts'         => 'edit_others_applicants',
                'publish_posts'             => 'publish_applicants',
                'read_post'                 => 'read_applicants',
                'read_private_posts'        => 'read_private_applicants',
                'delete_posts'              => 'delete_applicants',
                'delete_private_post'       => 'delete_private_applicants',
                'delete_published_posts'    => 'delete_published_applicants',
            ),

            'has_archive'   => true,
            'hierarchical'  => false,
            'menu_position' => null,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/clipboard.png',
            'supports'      => array( 'title')
        );

        register_post_type('applicants', $args);

        flush_rewrite_rules();
    }



    // Sub Header input box
    function subheader() {
        global $post;

        $values = get_post_custom($post->ID);
        $subheader = $values['subheader'][0];


        wp_nonce_field('subhead_frm_nonce', 'subhead_frm_nonce');

        $html = "<input id='subheader' type='text' name='subheader' value='$subheader' style='width: 100%;' />";

        echo $html;
    }

    // Saving Sub Header information
    add_action('save_post', 'subheader_text');

    function subheader_text($post_id) {

        // Bail if we're doing an auto save
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;

        // if our nonce isn't there, or we can't verify it, bail
        if (!isset($_POST['subhead_frm_nonce']) || !wp_verify_nonce($_POST['subhead_frm_nonce'], 'subhead_frm_nonce'))
            return;

        // if our current user can't edit this post, bail
        if (!current_user_can('edit_newsletter'))
            return;


        if (isset($_POST['subheader']))
            update_post_meta($post_id, 'subheader', esc_attr($_POST['subheader']));
    }


// 03.
    function edit_new_fields($tag, $taxonomy){

        // Check/Set the default
         $tag_extra_fields = get_option(CAUSES_FIELDS);

        ?>

        <tr class="form-field">
            <th scope="row" valign="top"><label for="title_partone">Title Part 1 (grey text)</label></th>
            <td>
                <input type="text" name="title_partone" id="title_partone" value="<?php echo  $tag_extra_fields[$tag->term_id]['title_partone'] ?>"/>
                <br />
                <p class="description">This is the first section of the Cause title. It will appear first and in grey on the front page.</p>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="title_parttwo">Title Part 2 (blue text)</label></th>
            <td>
                <input type="text" name="title_parttwo" id="title_parttwo" value="<?php echo $tag_extra_fields[$tag->term_id]['title_parttwo'] ?>"/>
                <br />
                <p class="description">This is the second section of the Cause title. It will appear second after a line break and in blue on the front page.</p>
            </td>
        </tr>

        <?php


    }

    function project_meta_box($post){
        $args = array(
            'post_type' => 'project',
            'numberposts' => -1
            );
        $project_posttype = get_posts($args);
        $selected = get_post_meta($post->ID,'projectid_field' );
        echo '<label for="projectid_field">';
        echo 'Project:';
        echo '</label> ';
        echo '<select id="projectid_field" name="projectid_field">';
            echo '<option value="none" selected="selected">None</option>';
        foreach ( $project_posttype as $project  ){
            $project_title = esc_attr($project->post_title);
            $project_ID = $project->ID;
            if ($project_ID == $selected[0]) {
                echo '<option value="'.$project_ID.'" selected="selected" >'.$project->post_title.'</option>';
            }
            else {
            echo '<option value="'.$project_ID.'">'.$project->post_title.'</option>';
            }
        }
        echo '</select>';

        echo '</form>';
   }



    // 04. when the form gets submitted, and the category gets updated
    function save_causes($term_id, $tt_id){

        if($_POST['taxonomy'] == 'causes'):
            $tag_extra_fields = get_option(CAUSES_FIELDS);
            $tag_extra_fields[$term_id]['title_partone'] = strip_tags($_POST['title_partone']);
            $tag_extra_fields[$term_id]['title_parttwo'] = strip_tags($_POST['title_parttwo']);
            update_option(CAUSES_FIELDS, $tag_extra_fields);
        endif;

    }




   // 05. Register Post Type Videos
    function video_register() {

        $args = array(

            'label'                 => 'Videos',
            'description'           => 'This is a video submitted to TPF.',
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_icon'             => get_bloginfo('template_directory') . '/library/images/youtube_icon.gif',
            'capability_type'       => 'video',
            'capabilities'          => array(
                'edit_post'                 => 'edit_video',
                'edit_posts'                => 'edit_videos',
                'edit_private_posts'        => 'edit_private_videos',
                'edit_published_posts'      => 'edit_published_videos',
                'edit_others_posts'         => 'edit_others_videos',
                'publish_posts'             => 'publish_videos',
                'read_post'                 => 'read_videos',
                'read_private_posts'        => 'read_private_videos',
                'delete_posts'              => 'delete_videos',
                'delete_private_post'       => 'delete_private_videos',
                'delete_published_posts'    => 'delete_published_videos',
            ),
            'hierarchical'          => true,
            'rewrite'               => array('slug' => ''),
            'query_var'             => true,
            'exclude_from_search'   => false,
            'supports'              => array('title'),
            'labels'                => array (
                'name'                  => 'Videos',
                'singular_name'         => 'Video',
                'menu_name'             => 'Videos',
                'add_new'               => 'Add Video Title',
                'add_new_item'          => 'Add New Video',
                'edit'                  => 'Edit',
                'edit_item'             => 'Edit Video',
                'new_item'              => 'New Video',
                'view'                  => 'View Video',
                'view_item'             => 'View Video',
                'search_items'          => 'Search Videos',
                'not_found'             => 'No Videos Found',
                'not_found_in_trash'    => 'No Videos Found in Trash',
                'parent'                => 'Parent Video',
            ),

        );

        register_post_type('video',$args);
    }


    // 06. Add Video MetaBox
    function video_meta_box_add(){
        add_meta_box(
                'youtubeid-metabox',
                'Youtube ID',
                'video_meta_box',
                'video',
                'normal',
                'low'
        );

       add_meta_box(
                'projectid-metabox',
                'Project',
                'project_meta_box',
                'video',
                'normal',
                'high'
       );

       if( current_user_can( 'start_a_campaign' ) ){
           add_meta_box(
                    'campaign_meta_box',
                    'Campaign ID',
                    'campaign_meta_box',
                    'video',
                    'normal',
                    'high'
           );
       }
    }


    // Video metabox for youtube ID
    function video_meta_box($post){

        $value = get_post_meta($post->ID,'cf_youtubeID_field' );

        echo "The Youtube ID of the video (ex: http://www.youtube.com/watch?v=<i><b>oHg5SGFRHA0</b></i>)</br>";
        echo '<label for="cf_youtubeID_field">';

            echo 'Youtube ID:<br/>';
            echo 'User has entered in this value: ' . $value[0];

        echo '</label><br/> ';

        $url = $value[0];
        $host = parse_url($url,  PHP_URL_HOST);
        $path = parse_url($url,  PHP_URL_PATH);
        $spiltPath = substr($path, 1);
        $query = parse_url($url, PHP_URL_QUERY);
        $splitQuery = substr($query, 2);
        $videoID = str_split($splitQuery, 11);


        if( $host == null ){

            echo '<input type="text" id="cf_youtubeID_field" name="cf_youtubeID_field" value="'.$url.'" size="25" />';

            if(strlen(trim($value[0])) > 0){
                echo '</br></br>Preview:</br><iframe width="420" height="315" src="http://www.youtube.com/embed/'.$url.'" frameborder="0" allowfullscreen></iframe>';
            }

        } elseif( $host == 'youtu.be' ){

            echo '<input type="text" id="cf_youtubeID_field" name="cf_youtubeID_field" value="'.$spiltPath.'" size="25" />';

            if(strlen(trim($value[0])) > 0){
                echo '</br></br>Preview:</br><iframe width="420" height="315" src="http://www.youtube.com/embed/'.$spiltPath.'" frameborder="0" allowfullscreen></iframe>';
            }

        } elseif( $host == 'www.youtube.com' ){

            echo '<input type="text" id="cf_youtubeID_field" name="cf_youtubeID_field" value="'.$videoID[0].'" size="25" />';

            if(strlen(trim($value[0])) > 0){
                echo '</br></br>Preview:</br><iframe width="420" height="315" src="http://www.youtube.com/embed/'.$videoID[0].'" frameborder="0" allowfullscreen></iframe>';
            }

        }

    }


    // Save metabox for youtube ID
   function video_meta_box_save( $post_id ) {

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
      //  if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
        if( !current_user_can( 'edit_post' ) ) return;

   	if( isset( $_POST['cf_cf_youtubeID_field'] ) )
		update_post_meta( $post_id, 'cf_cf_youtubeID_field', esc_attr( $_POST['cf_cf_youtubeID_field'] ) );

   	if( isset( $_POST['projectid_field'] ) )
		update_post_meta( $post_id, 'projectid_field', esc_attr( $_POST['projectid_field'] ) );
    }


    function wpufe_add_campaign_id( $post_id ) {
        update_post_meta( $post_id, 'cf_campaign_id', $_POST['cf_campaign_id'] );
    }
    add_action( 'wpuf_add_post_after_insert', 'wpufe_add_campaign_id' );


    // Campaign Meta Box
    if( current_user_can( 'start_a_campaign' ) ){
        function campaign_meta_box($post){

            $value = get_post_meta($post->ID, 'cf_campaign_id' );

            echo '<label for="cf_campaign_id">';
            echo 'Campaign ID:';
            echo '</label> ';

            echo '<input type="text" id="cf_campaign_id" name="cf_campaign_id" value="' .$value[0]. '" size="25" />';


        }
    }


    // Redirect after submitting video
    function custom_redirect( $url ) {
        global $post;
        return get_permalink( get_page_by_path( 'video-submission' ));
    }
    add_filter( 'wpuf_after_post_redirect', 'custom_redirect' );


    //Register custom post type Person
    add_action( 'init', 'person', 0 );

    function person() {

        $labels = array(
            'name'               => _x('My People', 'post type general name'),
            'singular_name'      => _x('Person', 'post type singular name'),
            'add_new'            => _x('Add New', 'person'),
            'add_new_item'       => __('Add New person'),
            'edit_item'          => __('Edit Person'),
            'new_item'           => __('New Person'),
            'all_items'          => __('All People'),
            'view_item'          => __('View People'),
            'search_items'       => __('Search People'),
            'not_found'          => __('No People found'),
            'not_found_in_trash' => __('No People found in Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => __('People')
        );


        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'rewrite'               => true,
            'capability_type'       => 'person',
            'capabilities'          => array(
                'edit_post'                 => 'edit_person',
                'edit_posts'                => 'edit_people',
                'edit_private_posts'        => 'edit_private_people',
                'edit_published_posts'      => 'edit_published_people',
                'edit_others_posts'         => 'edit_others_people',
                'publish_posts'             => 'publish_people',
                'read_post'                 => 'read_person',
                'read_private_posts'        => 'read_private_people',
                'delete_posts'              => 'delete_people',
                'delete_private_post'       => 'delete_private_person',
                'delete_published_posts'    => 'delete_published_people',
            ),

            'has_archive'   => true,
            'hierarchical'  => false,
            'menu_position' => null,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/user-silhouette.png',
            'supports'      => array( 'title','editor', 'excerpt')
        );

        register_post_type('person', $args);

        // Register taxonomy Position
         register_taxonomy(
            'positions',
            'person',
            array(
                'hierarchical'      => true,
                'label'             => 'Positions',
                'singular_label'    => 'Position',
                'query_var'         => true,
                'rewrite'           => array( 'slug' => 'position-category' )
            )
        );

        flush_rewrite_rules();
    }

   //Register custom post type Report

  function reports() {

 $labels = array(
            'name'               => _x('My Reports', 'post type general name'),
            'singular_name'      => _x('Report', 'post type singular name'),
            'add_new'            => _x('Add New', 'report'),
            'add_new_item'       => __('Add New Report'),
            'edit_item'          => __('Edit Report'),
            'new_item'           => __('New Report'),
            'all_items'          => __('All Reports'),
            'view_item'          => __('View Report'),
            'search_items'       => __('Search Report'),
            'not_found'          => __('No Reports found'),
            'not_found_in_trash' => __('No Reports found in Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Reports')
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'rewrite'               => true,
            'capability_type'       => 'report',
            'capabilities'          => array(
                'edit_post'                 => 'edit_report',
                'edit_posts'                => 'edit_reports',
                'edit_private_posts'        => 'edit_private_reports',
                'edit_published_posts'      => 'edit_published_reports',
                'edit_others_posts'         => 'edit_others_reports',
                'publish_posts'             => 'publish_reports',
                'read_post'                 => 'read_reports',
                'read_private_posts'        => 'read_private_reports',
                'delete_posts'              => 'delete_reports',
                'delete_private_post'       => 'delete_private_reports',
                'delete_published_posts'    => 'delete_published_reports',
            ),

            'has_archive'   => true,
            'hierarchical'  => false,
            'menu_position' => null,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/applications.png',
            'supports'      => array( 'title', 'editor', 'thumbnail' )
        );

        register_post_type('report', $args);

        flush_rewrite_rules();
    }



    add_action('add_meta_boxes', 'add_report_metabox');

    function add_report_metabox(){
        add_meta_box('project_selection', 'Project Selection', 'project_selection', 'report', 'normal', 'high');
    }

    function project_selection(){
        global $post;

        global $current_user;
        get_currentuserinfo();

        function get_current_user_role () {
            global $current_user;
            get_currentuserinfo();
            $user_roles = $current_user->roles;
            $user_role = array_shift($user_roles);
            return $user_role;
        }


        if( get_current_user_role () == 'partner' ){

            $values = get_post_custom($post->ID);
            $project_selection = $values['project_selection'][0];

            wp_nonce_field('selection_frm_nonce', 'selection_frm_nonce');

            $currentUserID = $current_user->ID;

            global $wpdb;
            $projects = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'project' AND post_author = '$currentUserID' AND post_status = 'publish'");

            echo "<label for='project_selection'>Project ID</label>";

            echo "<select name='project_selection'>";
                echo "<option value=''>Select a published project</option>";
                foreach( $projects as $project ){

                    echo "<option value='" .$project->ID. "' " .selected( $project_selection, $project->ID ). "' >" .$project->post_title. "</option>";
                }

            echo "</select>";

        } elseif( get_current_user_role () == 'administrator' ){

            $values = get_post_custom($post->ID);
            $project_selection = $values['project_selection'][0];

            wp_nonce_field('selection_frm_nonce', 'selection_frm_nonce');

            global $wpdb;
            $projects = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'project' AND post_status = 'publish'");

            echo "<label for='project_selection'>Project ID</label>";

            echo "<select id='project_selection' name='project_selection'>";
                echo "<option value=''>Select a published project</option>";
                foreach( $projects as $project ){
                    echo "<option value='" .$project->ID. "' " .selected( $project_selection, $project->ID ). "' >" .$project->post_title. "</option>";
                }

            echo "</select>";
        }


    }


    add_action('save_post', 'save_project_selection_details');

    function save_project_selection_details(){
        global $post;
        update_post_meta( $post->ID, 'project_selection', $_POST['project_selection'] );
    }


    //Register custom post type Photos

    function galleries() {

        $labels = array(
            'name'               => _x('My Photo Galleries', 'post type general name'),
            'singular_name'      => _x('Gallery', 'post type singular name'),
            'add_new'            => _x('Add New', 'gallery'),
            'add_new_item'       => __('Add New Gallery'),
            'edit_item'          => __('Edit Gallery'),
            'new_item'           => __('New Gallery'),
            'all_items'          => __('All Galleries'),
            'view_item'          => __('View Gallery'),
            'search_items'       => __('Search Gallery'),
            'not_found'          => __('No Galleries found'),
            'not_found_in_trash' => __('No Galleries found in Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Galleries')
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'rewrite'               => true,
            'capability_type'       => 'gallery',
            'capabilities'          => array(
                'edit_post'                 => 'edit_gallery',
                'edit_posts'                => 'edit_galleries',
                'edit_private_posts'        => 'edit_private_galleries',
                'edit_published_posts'      => 'edit_published_galleries',
                'edit_others_posts'         => 'edit_others_galleries',
                'publish_posts'             => 'publish_galleries',
                'read_post'                 => 'read_galleries',
                'read_private_posts'        => 'read_private_galleries',
                'delete_posts'              => 'delete_galleries',
                'delete_private_post'       => 'delete_private_galleries',
                'delete_published_posts'    => 'delete_published_galleries',
            ),

            'has_archive'   => true,
            'hierarchical'  => false,
            'menu_position' => null,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/images-stack.png',
            'supports'      => array( 'title', 'editor', 'thumbnail' )
        );

        register_post_type('gallery', $args);

        flush_rewrite_rules();
    }





        register_post_type('statement', array(
            'label' => 'Statements',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_icon'     => get_bloginfo('template_directory') . '/library/images/envelope-share.png',
            'rewrite' => array('slug' => 'statements'),
            'query_var' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'trackbacks',
                'custom-fields',
                'comments',
                'revisions',
                'thumbnail',
                'author',
                'page-attributes')
         ) );




/* ========================================================================================================================

    Scripts

    ======================================================================================================================== */

	/**
	 * Add scripts and styles
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

    // 07.
    function script_enqueuer() {

        if( !is_admin() ) {


            /***** Place JS in Header *****/
            /* Modernizr */
            wp_register_script( 'modernizr', get_stylesheet_directory_uri().'/library/js/libs/modernizr.js' );
            wp_enqueue_script( 'modernizr' );


            /***** Place JS in Footer *****/
            /* jQuery 1.7.2 */
            wp_register_script( 'jquery_google_api', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js', false, '1.7.2', true );
            wp_enqueue_script( 'jquery_google_api' );

            wp_register_script( 'jqueryui_google_api', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js', false, '1.8', false );
            wp_enqueue_script( 'jqueryui_google_api' );


               /* FancyBox */
            wp_register_script( 'fancybox', get_stylesheet_directory_uri().'/library/js/libs/jquery.fancybox.pack.js', 'jquery', '2.1.1' , true );
            wp_enqueue_script( 'fancybox' );

            wp_register_style( 'fancyboxcss', get_stylesheet_directory_uri().'/library/css/jquery.fancybox.css');
            wp_enqueue_style( 'fancyboxcss' );

            wp_register_style( 'video-template-tubepress', '/wp-content/plugins/tubepress/sys/ui/themes/default/video-template.css');
            wp_enqueue_style( 'video-template-tubepress' );



            /* Plugins - Place all third party scripts/plugins here. */
            wp_register_script( 'plugins', get_stylesheet_directory_uri().'/library/js/plugins.js', 'jquery', false, true );
            wp_enqueue_script( 'plugins' );

            /* Script - Our js */
            wp_register_script( 'script', get_stylesheet_directory_uri().'/library/js/script.js', 'plugins', '1.0', true );
            wp_enqueue_script( 'script' );


            /* Any Additional Scripts */
            /* wp_register_script( $handle, $src, $deps, $ver, $in_footer ); */
            /* wp_enqueue_script( $handle ); */

        }

    }


    // 08. Remove style/scripts from header/footer
    function my_deregister_javascript() {

        if ( !is_front_page() && !is_page_template('videos-template.php') ) {
            wp_deregister_script( 'tubepress' );
            wp_deregister_script( 'tpf-content-gallery-script' );
        }


//        if ( !is_front_page() && 'project' != get_post_type() ) {
//            wp_deregister_script( 'tpf-content-gallery-script' );
//        }

        if ( 'project' != get_post_type() ) {
            wp_deregister_script( 'tpf-content-gallery-script_api' );
        }

     //   if( !is_front_page() && !is_admin()){
    //        wp_deregister_script( 'jquery' );
     //   }

        if(is_front_page() || is_page_template('videos-template.php')){
            wp_deregister_script('jquery_google_api');
        }

    }

    function my_deregister_styles(){

        if ( !is_front_page() ) {
            wp_deregister_style( 'tubepress' );
            //wp_deregister_style( 'tpf-content-gallery-style' );
        }

        if (!is_page_template('videos-template.php')) {
            wp_deregister_style( 'video-template-tubepress' );
        }

        if ( !is_front_page() && 'project' != get_post_type() ) {
            wp_deregister_style( 'tpf-content-gallery-style' );
        }

        if( !is_admin() ){
            wp_deregister_style( 'taxonomy-image-plugin-public' );
        }

    }



    /* ========================================================================================================================

      Comments

      ======================================================================================================================== */

	/**
	 * Custom callback for outputting comments
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif; ?>
		</li>
		<?php
	}


    if(function_exists('register_nav_menus')){
	    register_nav_menus(
		    array(
			    'main_nav' => 'Main Navigation Menu'
			    )
	    );
    }


    function get_description($category){
        $excerpt = category_description($category);
        $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = substr($excerpt, 0, 200);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
        $excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
        return $excerpt;
    }


/* ========================================================================================================================

    Functions

    ======================================================================================================================== */

    // 09. Footer widget for contact info
    function footer_widgets_init() {

        register_sidebar( array(
            'name' => 'Footer right sidebar',
            'id' => 'footer_right',
            'before_widget' => '<div>',
            'after_widget' => '</div>',
            'before_title' => '<h2 class="rounded">',
            'after_title' => '</h2>',
        ) );
    }

      /**
        * Register twitter widget area for Social Media template.
        *
        */
        function social_widget_init() {

            register_sidebar( array(
                'name' => 'Social Media sidebar',
                'id' => 'socialmedia_side',
                'before_widget' => '<div>',
                'after_widget' => '</div>',
                'before_title' => '<h2 class="rounded">',
                'after_title' => '</h2>',
            ) );
        }
        add_action( 'widgets_init', 'social_widget_init' );


    // 16.
    function wpufe_campaign_video( $post_type, $post = null) {

         echo '<li class="hide">';
            echo '<label for="cf_campaign_id">';
                echo 'Campaign ID';
            echo '</label>';
            echo '<input class="" value="' . $_GET['campaign_id'] . '" type="text" name="cf_campaign_id" id="cf_campaign_id" minlength="2" />';
            echo '<div class="clear"></div>';
        echo '</li>';

    }

    // Only show author's own posts unless admin
    function posts_for_current_author($query) {
        global $user_level;

        if($query->is_admin && $user_level < 5) {
            global $user_ID;
            // Elif: I had to comment out this line because the report custom fields had failed to show for Partners.
            //$query->set('author',  $user_ID);
            unset($user_ID);
        }
        unset($user_level);

        return $query;
    }
    add_filter('pre_get_posts', 'posts_for_current_author');

    // Custom walker for featured nav on the front page, displays nav description

        class description_walker extends Walker_Nav_Menu
    {
        function start_el(&$output, $item, $depth, $args)
        {
            global $wp_query;

            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

            $class_names = $value = '';

            $classes = empty( $item->classes ) ? array() : (array) $item->classes;

            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
            $class_names = ' class="'. esc_attr( $class_names ) . '"';

            $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

            $prepend = '<strong>';
            $append = '</strong>';
            $description  = ! empty( $item->description ) ? '<div class="callout-img"></div><div class="description">'.esc_attr( $item->description ).'</div><a class="button" href="'. $item->url .'">Learn more &rarr;</a>' : '';


            if($depth != 0)
            {
                        $description = $append = $prepend = "";
            }

                $item_output = $args->before;
                $item_output .= '<a'. $attributes .'>';
                $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
                $item_output .= '</a>';
                $item_output .= $description.$args->link_after;
                $item_output .= $args->after;

                $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
                }
    }

        if(!function_exists('get_post_top_ancestor_id')){
        /**
        * Gets the id of the topmost ancestor of the current page. Returns the current
        * page's id if there is no parent.
        *
        * @uses object $post
        * @return int
        */
        function get_post_top_ancestor_id(){
            global $post;

            if($post->post_parent){
                $ancestors = array_reverse(get_post_ancestors($post->ID));
                return $ancestors[0];
            }

            return $post->ID;
    }}



    // Filter for side nav so it can display current page
add_filter('page_css_class', 'custom_page_css_class', 10, 2);

function custom_page_css_class($class, $page) {
	global $id;
    global $post;

	if ($page->ID == $post->ID){
		/* checks if the page ID matches the current page and if it does,
		    adds the current_page_item class
		*/
		$t = array( 'page_item', 'page-item-'.$page->post_name, 'current_page_item');

    } else {
		$t = array( 'page_item', 'page-item-'.$page->post_name);
    }
	return $t;

}

// Hide Custom Forms from others - To be turned on before launch so there are no accidents with the forms

    function hide_admin_menu()
    {
        global $current_user;
        get_currentuserinfo();

        if($current_user->user_login != 'Patrick')
        {
            echo '<style type="text/css">#toplevel_page_edit-post_type-acf{display:none;}</style>';
        }
    }

   // add_action('admin_head', 'hide_admin_menu'); Uncomment to turn on


    //Get all custom fields attached to a page

if ( !function_exists('base_get_all_custom_fields') ) {
	function base_get_all_custom_fields()
	{
		global $post;
		$custom_fields = get_post_custom($post->ID);
		$hidden_field = '_';
		foreach( $custom_fields as $key => $value ){
			if( !empty($value) ) {
				$pos = strpos($key, $hidden_field); //Removes hidden input which start with an underscore
				if( $pos !== false && $pos == 0 ) {
					unset($custom_fields[$key]);
				}
            elseif (empty($value[0])) {  // Removes empty inputs. Because all the inputs are required, any empty one is a leftover and unused
                    unset($custom_fields[$key]);
                }
			}
		}
		return $custom_fields;
	}
}

    function custom_excerpt_length( $length ) {
        return 150;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

    function my_acf_form_pre_save_post( $post_id )
    {
        // create a new post
        $post = array(
            'post_status' => 'draft',
            'post_title' => 'Partner Application',
            'post_type' => 'applicants',
        );


        // insert the post
        $post_id = wp_insert_post( $post );

        return $post_id;
    }

    add_filter('acf_form_pre_save_post', 'my_acf_form_pre_save_post');

    function my_bp_profile_group_tabs() {
        global $bp, $group_name;

        if ( !$groups = wp_cache_get( 'xprofile_groups_inc_empty', 'bp' ) ) {
            $groups = BP_XProfile_Group::get( array( 'fetch_fields' => true ) );
            wp_cache_set( 'xprofile_groups_inc_empty', $groups, 'bp' );
        }

        if ( empty( $group_name ) )
            $group_name = bp_profile_group_name(false);

        $tabs = array();
        for ( $i = 0, $count = count( $groups ); $i < $count; ++$i ) {
            if ( $group_name == $groups[$i]->name )
                $selected = ' class="current ' .$groups[$i]->name. '"';
            else
                $selected = ' class="' .$groups[$i]->name. '"';

            if ( !empty( $groups[$i]->fields ) ) {
                $link = trailingslashit( bp_displayed_user_domain() . $bp->profile->slug . '/edit/group/' . $groups[$i]->id );
                $tabs[] = sprintf( '<li %1$s><a href="%2$s">%3$s</a></li>', $selected, $link, esc_html( $groups[$i]->name ) );
            }
        }

        $tabs = apply_filters( 'xprofile_filter_profile_group_tabs', $tabs, $groups, $group_name );
        foreach ( (array) $tabs as $tab )
            echo $tab;

        do_action( 'xprofile_profile_group_tabs' );
    }

    function get_current_user_role() {
        global $current_user;
        get_currentuserinfo();
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);
        return $user_role;
    }

    add_filter('body_class','my_class_names');
    function my_class_names($classes) {
        // add 'class-name' to the $classes array

        $classes[] = get_current_user_role();

        // return the $classes array
        return $classes;
    }

    add_filter( 'admin_body_class', 'jn_admin_body_class' );
    function jn_admin_body_class( $classes ) {
        $classes .= get_current_user_role();
        return $classes;
    }


    function remove_notifications_subnav(){
        global $bp;
        if ( $bp->current_component == $bp->settings->slug ) {
            bp_core_remove_subnav_item($bp->settings->slug, 'notifications');
        }
    }
    add_action( 'wp', 'remove_notifications_subnav', 2 );
    remove_action( 'bp_adminbar_menus', 'bp_adminbar_notifications_menu', 8 );



    add_action('admin_menu', 'fundraiser_guide');

    function fundraiser_guide(){
        add_options_page( 'Edit Being a Fundraiser Rockstar Guide', 'Edit Being a Fundraiser Rockstar Guide', 'manage_options', 'fundraiser-guide', 'fundraiser_guide_admin');
    }

    function fundraiser_guide_admin(){
        include('admin/edit-fundraiser-guide.php');
    }



    global $bp;
    $displayed_user = $bp->displayed_user->id;

    function get_user_roles( $displayed_user )  {

        $userInfo = new WP_User( $displayed_user ); // this gives us access to all the useful methods and properties for this user

        if ( $userInfo ) {
            $roles = $userInfo->roles; // returns an array of roles
            return $roles;
        }  else {
            return false;
        }

    }

    // This is Temporary: removes quick edit from custom post type list
    function remove_quick_edit( $actions ) {
        global $post;
        if( $post->post_type == 'project' ) {
            unset($actions['inline hide-if-no-js']);
        }
        return $actions;
    }
    add_filter('post_row_actions','remove_quick_edit',10,2);

    function get_donations_in_project($project_id) {
        global $wpdb;

        //$objectIDs = $wpdb->get_results("SELECT * FROM xzwgy7v_wpsc_meta WHERE meta_value = '$project_id' AND meta_key = 'sku'");
        $objectIDs = $wpdb->get_results("SELECT * FROM xzwgy7v_postmeta WHERE meta_value = $project_id AND meta_key = '_wpsc_sku'");


        if($objectIDs):

            for($i = 0; $i < count($objectIDs); $i++){
                $objectID = $objectIDs[$i];

                //$oID = $objectID->object_id;
                $oID = $objectID->post_id;

                //$cartID = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_cart_contents WHERE id = '$oID'");
                $cartID = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_cart_contents WHERE prodid = '$oID'");
                $purchaseID = $cartID->purchaseid;
                $purchaseLog = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_purchase_logs WHERE id = '$purchaseID' AND transactid IS NOT NULL AND TRIM(transactid) <> ''");
                $formData_firstName = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_submited_form_data WHERE log_id = '$purchaseID' AND form_id = '2'");
                $formData_lastName = $wpdb->get_row("SELECT * FROM xzwgy7v_wpsc_submited_form_data WHERE log_id = '$purchaseID' AND form_id = '3'");

                $results[$i] = new stdClass;
                $results[$i]->id = $purchaseID;
                $results[$i]->prodid = $purchaseLog->transactid;
                $results[$i]->first_name = $formData_firstName->value;
                $results[$i]->last_name = $formData_lastName->value;
                $results[$i]->totalprice = $purchaseLog->totalprice;
                $results[$i]->date = gmdate('F d, Y', $purchaseLog->date);

            }

        endif;

        return $results;

    }
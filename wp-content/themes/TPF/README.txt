

	TPF Theme Template

	Created by Media Hive

	- - - - - - - - - - - - - - - - - - - - - - -

	TPF Theme Template is built on Starkers version 4.0

	Starkers is a bare-bones WordPress theme
	created to act as a starting point for the 
	theme designer. Free of all style, 
	presentational elements, and non-semantic 
	markup, Starkers is the perfect ‘blank slate’ 
	for all your WordPress projects.

	Best of all: it’s free and fully GPL-licensed, 
	so you can use it for whatever you like — even 
	commercial work.

	Up until now, Starkers has merely been a 
	stripped-back version of the default theme 
	that ships with WordPress, and as a result has 
	never been quite as ‘naked’ as it could be. 
	Now we’ve brought it under the Viewport 
	Industries banner and have rebuilt it from the 
	ground up. Literally. Not one line of code is 
	the same.

	For more details, please visit:
	http://viewportindustries.com/products/starkers/

	- - - - - - - - - - - - - - - - - - - - - - -

	To add additional JS:
	Go to functions.php

	All third party js/plugins to plugins.js
	All of our js will be developed in script.js or in any other additional js file.



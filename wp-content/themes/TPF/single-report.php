<?php
/**
 * Template Name: Single Report
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>


<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
            <section id="primary" class="page">

                
                    <?php

                        if ( have_posts() ) while ( have_posts() ) : the_post();

                            $report_project = get_field('report_project');
                            $project_ID = $report_project[0]->ID;
                            $custom_fields = base_get_all_custom_fields();

                            if( !empty($custom_fields) ) {
                                $field_keys = array_keys($custom_fields);
                                foreach ($field_keys as $field){
                                    if ($field == 'report_project') {
                                        echo '';
                                    } else {

                                        echo '<p>';
                                            $field_object = get_field_object($field);
                                        echo '<h5>';
                                            echo $field_object['label'].': ';
                                        echo '</h5>';

                                    }

                                    if (!empty($field_object['instructions'])) {
                                        echo '<i>';
                                        echo $field_object['instructions'];
                                        echo '</i></br>';
                                    } else{
                                        echo $custom_fields[$field][0];
                                        echo '</p>';
                                    }
                                }


                        }

                
                    ?>
                    <div class='report'>
                        
                    </div>
                    <?php
                endwhile;
                    echo '<a class="more" href="'.get_permalink($project_ID).'" class="back">Back to '. ucwords(get_the_title($project_ID)) .'</a>';
                ?>

            </section><!-- end #primary -->
    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

<?php
/**
 * Template Name: Photo Gallery Main Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
        <div class="column">

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

            <section id="primary" class="page">


                <?php
                $args = array( 'post_type' => 'gallery');
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
                    $images = get_field('gallery_photos');
                    ?>
                    <div class='photo_gallery'>
                        <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Link to <?php the_title_attribute(); ?>">
                            <?php the_title(); ?></a></h2>
                        <div class="photo_gallery_thumbs">
                            <?php
                            $i = 0;
                            foreach ($images as $image){
                                $thumb = $image['gallery_photo']['sizes']['thumbnail'];
                                $photo = $image['gallery_photo']['sizes']['large'];
                                $title = $image['gallery_photo']['title'];
                                ?>
                                <a class="fancybox" href="<?php echo $photo; ?>" data-fancybox-group="gallery<?php the_ID(); ?>" title="<?php echo $title; ?>"><img class="gallerythumb" src="<?php echo $thumb; ?>" alt="" /></a>
                                <?php
                                if (++$i == 4) break;
                            }
                            ?>
                        </div>
                    </div>
                    <a  class="more" href="<?php the_permalink() ?>" rel="bookmark" title="View More Photos">View More Photos</a>
                    <?php

                endwhile;
                ?>

            </section><!-- end #primary -->
        </div>
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
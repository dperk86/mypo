<?php
    /**
     * Template Name: Start a Campaign Main Page
     *
     * Selectable from a dropdown menu on the edit page screen.
     */
?>

<?php get_template_parts(array('parts/shared/html-header', 'parts/shared/header')); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
            <section id="primary" class="page start-a-campaign">

                <?php if (have_posts()) while (have_posts()) : the_post(); ?>

                    <div class="content">
                        <?php the_content(); ?>
                    </div><!-- end .content -->

                    <div id="featured-projects">

                        <header>
                            <h2>Featured Projects</h2>
                            <a class="viewall" href="<?php echo get_bloginfo( 'wpurl' ); ?>/causes-page/">View all projects &raquo;</a>
                        </header>

                        <div id="tiles">

                            <?php

                            $args = array(
                                'numberposts'     => 9,
                                'offset'          => 0,
                                'orderby'         => 'rand',
                                'order'           => 'DESC',
                                'meta_key'        => 'complete',
                                'meta_value'      => ' ',
                                'post_type'       => 'project',
                                'post_status'     => 'publish',
                                'suppress_filters' => true
                            );

                                $posts = get_posts($args);
                                    foreach($posts as $post) {
                            ?>
                                        <article class="project">
                                            <figure>
                                                <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">

                                                    <?php

                                                        $featuredImg = wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), 'medium', false, '' );

                                                        if( $featuredImg == null ){
                                                            echo '<img src="' . get_bloginfo( 'wpurl' ) . '/wp-content/themes/TPF/library/images/logo_feat_medium.png" />';
                                                        } else {
                                                            echo $featuredImg;
                                                        }

                                                    ?>

                                                </a>
                                            </figure>
                                            <header>

                                                <h4>
                                                    <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                                        <?php the_title(); ?>
                                                    </a>
                                                </h4>

                                                <span class="author">
                                                    Organization:
                                                    <a href="<?php bloginfo( 'url' ); ?>/my-tpf/<?php echo the_author_meta( 'user_login', $post->post_author ); ?>/?component=projects">
                                                        <?php echo the_author_meta( 'display_name', $post->post_author ); ?>
                                                    </a>

                                                </span>

                                            </header>
                                            <div class="content">
                                                <p>
                                                    <?php

                                                        $goal_values = get_post_custom_values('goal');

                                                        foreach ( $goal_values as $key => $value ) {

                                                            //echo "$value";
                                                            echo ttruncat( $value, 225 );
                                                        }

                                                    ?>
                                                </p>
                                            </div><!-- end .content -->
                                            <footer>
                                                <p>
                                                    <a href="<?php the_permalink(); ?>">Learn more about this project &rarr;</a>
                                                </p>

                                                <?php

                                                    echo '<section class="start-campaign-button">';

                                                        locate_template(array('members/single/campaigns/start-loop-campaign-button.php'), true, false);

                                                    echo '</section>';

                                                ?>
                                            </footer>
                                        </article><!-- end .project -->
                            <?php
                                    }
                            ?>

                        </div><!-- end #tiles -->

                    </div><!-- end #featured-projects -->

                <?php endwhile; ?>

            </section><!-- end #primary -->

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
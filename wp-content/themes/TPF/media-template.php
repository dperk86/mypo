<?php
/**
 * Template Name: Media Page Template
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

    <header id="hero">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </header><!-- end #hero -->

    <div class="wrapper">
        <div class="column">

            <?php get_template_parts( array( 'parts/shared/sidenav') ); ?>

            <section id="primary" class="page">
                <?php if (have_posts()) : while (have_posts()) : the_post();?>

                <p id="mediacontent">
                      <?php echo get_the_content(); ?>
                </p>
                <div id="morephotos">
                     <?php
                        echo '<a class="more" href="'.get_permalink(89).'" class="back">';
                     ?>
                        <h4>Photos</h4>
                        <img src="<?php bloginfo('template_directory'); ?>/library/images/photos.png" alt="More Photos" height="150" width="190">
                    </a>
                </div>
                
                <div id="morevideos">
                     <?php
                        echo '<a class="more" href="'.get_permalink(86).'" class="back">';
                     ?>
              
                    <h4>Videos</h4>
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/videos.png" alt="More Videos" height="150" width="190"> 
                   </a>
                </div>
                <?php endwhile; endif; ?>
                
            </section><!-- end #primary -->
        </div>
    </div><!-- end .wrapper -->


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
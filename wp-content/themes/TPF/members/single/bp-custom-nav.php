<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $displayed_user_role = get_user_roles( $displayed_user );
?>

    <?php
        //
        // For users who can start_a_campaign - Campaign & Administrator
        //
    ?>
    <?php if( user_can($displayed_user, 'start_a_campaign' ) ) : ?>
        <li id="campaigns-personal-li" <?php if($_GET['component']) : ?> class="current selected"<?php endif; ?>>
            <a href="<?php echo $bp->displayed_user->domain; ?>?component=campaigns">Campaigns</a>
        </li>
    <?php endif; ?>




    <?php
        //
        // Only For Administrators
        //
    ?>
    <?php if( $displayed_user_role[0] == 'administrator' ) : ?>

        <?php global $wpdb; ?>

        <?php // Display Videos tab if user has uploaded videos. ?>
        <?php
            $campaigns = $wpdb->get_results( "SELECT * FROM campaigns WHERE user_id = $displayed_user" );

            if($campaigns) :
                foreach($campaigns as $campaign) :

                    $campaignID = $campaign->id;

                    $args = array(
                        'post_type'      => 'video',
                        'post_status'    => 'publish',
                        'posts_per_page' => -1, // = all of 'em
                        'meta_query'     => array(
                            array(
                                'key'   => 'cf_campaign_id',
                                'value' => $campaignID
                            )
                        )
                    );

                    $loop = new WP_Query( $args );

                    if ( $loop->have_posts() ) :
        ?>
                        <li id="videos-personal-li" <?php if($_GET['mediacampaign']) : ?> class="current selected"<?php endif; ?>>
                            <a href="<?php echo $bp->displayed_user->domain; ?>?mediacampaign=videos">Campaign Videos</a>
                        </li>

        <?php
                    endif;

                endforeach;
            endif;
        ?>

    <?php endif; ?>




    <?php
        //
        // Only For Campaigners
        //
    ?>
    <?php if( $displayed_user_role[0] == 'campaigner' || $displayed_user_role[0] == 'donor' || $displayed_user_role[0] == 'fundowner' ) : ?>

        <?php global $wpdb; ?>

        <?php // Display Videos tab if Campainger has uploaded videos. ?>
        <?php
            $campaigns = $wpdb->get_results( "SELECT * FROM campaigns WHERE user_id = $displayed_user" );

            if($campaigns) :
                foreach($campaigns as $campaign) :

                    $campaignID = $campaign->id;

                    $args = array(
                        'post_type'      => 'video',
                        'post_status'    => 'publish',
                        'posts_per_page' => -1, // = all of 'em
                        'meta_query'     => array(
                            array(
                                'key'   => 'cf_campaign_id',
                                'value' => $campaignID
                            )
                        )
                    );

                    $loop = new WP_Query( $args );

                     if ( $loop->have_posts() ) :
            ?>

                         <li id="videos-personal-li" <?php if($_GET['mediacampaign']) : ?> class="current selected"<?php endif; ?>>
                             <a href="<?php echo $bp->displayed_user->domain; ?>?mediacampaign=videos">Videos</a>
                         </li>

            <?php
                     endif;

                endforeach;
            endif;

        ?>

        <li id="fundraiser-personal-li" <?php if($_GET['guide']) : ?> class="current selected"<?php endif; ?>>
            <a href="<?php echo $bp->displayed_user->domain; ?>?guide=fundraiser">Being a Fundraiser Rockstar</a>
        </li>

    <?php endif; ?>




    <?php
        //
        // For users who can edit_projects - Partners & Administrators
        //
    ?>
    <?php if(user_can($displayed_user, 'edit_projects' )): ?>

        <li id="projects-personal-li" <?php if($_GET['component']) : ?> class="current selected"<?php endif; ?>>
            <a href="<?php echo $bp->displayed_user->domain; ?>?component=projects">Projects</a>
        </li>

    <?php endif; ?>




    <?php
        //
        // Only For Administrators
        //
    ?>
    <?php if( $displayed_user_role[0] == 'administrator' ) : ?>

        <?php global $wpdb; ?>

        <?php // Display Photos tab if Administrator has uploaded photos. ?>
        <?php $attachments = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_author = $displayed_user AND post_type = 'attachment'"); ?>
        <?php if($attachments): ?>
            <li id="photos-personal-li" <?php if($_GET['mediapics']) : ?> class="current selected" <?php endif; ?>>
                <a href="<?php echo $bp->displayed_user->domain; ?>?mediapics=photos">Project Photos</a>
            </li>
        <?php endif; ?>


        <?php // Display Videos tab if Administrator has uploaded videos. ?>
        <li id="videos-personal-li" <?php if($_GET['mediavids']) : ?> class="current selected"<?php endif; ?>>
            <a href="<?php echo $bp->displayed_user->domain; ?>?mediavids=videos">Project Videos</a>
        </li>

    <?php endif; ?>





    <?php
        //
        // Only For Partners
        //
    ?>
    <?php if( $displayed_user_role[0] == 'partner' || $displayed_user_role[0] == 'trustedpartner' ) : ?>

        <?php global $wpdb; ?>

        <?php // Display Photos tab if Partner has uploaded photos. ?>
        <?php $attachments = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_author = $displayed_user AND post_type = 'attachment'"); ?>
        <?php if($attachments): ?>
            <li id="photos-personal-li" <?php if($_GET['mediapics']) : ?> class="current selected"<?php endif; ?>>
                <a href="<?php echo $bp->displayed_user->domain; ?>?mediapics=photos">Photos</a>
            </li>
        <?php endif; ?>


        <li id="videos-personal-li" <?php if($_GET['mediavids']) : ?> class="current selected"<?php endif; ?>>
            <a href="<?php echo $bp->displayed_user->domain; ?>?mediavids=videos">Videos</a>
        </li>
    
    <?php endif; ?>

    <?php if( $displayed_user_role[0] == 'fundowner' ) : ?>

        <?php global $wpdb; ?>

        <li id="statements-personal-li" <?php if($_GET['statements']) : ?> class="current selected"<?php endif; ?>>
            <a href="<?php echo $bp->displayed_user->domain; ?>?statements=statements">Statements</a>
        </li>

    <?php endif; ?>

        
        
       
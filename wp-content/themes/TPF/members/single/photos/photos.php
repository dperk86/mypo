<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $user_info = get_userdata( $displayed_user );
?>

    <h3>Photos</h3>

    <div id="photos">

        <?php

            $args = array(
                'post_type'      => 'project',
                'author'         => $displayed_user,
                'post_status'    => 'publish',
                'posts_per_page' => -1, // = all of 'em
                'meta_value'     => ' '
            );

            $loop = new WP_Query( $args );
            //$loop = new WP_Query( 'author=9' );

            while ( $loop->have_posts() ) : $loop->the_post();


                $args = array(
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'post_status' => null,
                    'post_parent' => $post->ID
                );

                $attachments = get_posts( $args );
                if ( $attachments ) {

                    echo '<article class="project">';

                        echo '<h4>';

                            echo '<a href="';
                            the_permalink();
                            echo '" title="';
                            the_title_attribute();
                            echo '">';

                                echo the_title();

                            echo '</a>';

                        echo '</h4>';

                        echo '<ul>';

                        foreach ( $attachments as $attachment ) {

                            $imgSrc = wp_get_attachment_image_src( $attachment->ID, 'full' );

                            echo '<li>';
                                echo '<a href="' .$imgSrc[0]. '" rel="group-'.$post->ID.'">';
                                    echo wp_get_attachment_image( $attachment->ID, 'thumbnail' );
                                echo '</a>';
                            echo '</li>';
                        }

                        echo '</ul>';

                    echo '</article>';
                }

            endwhile;

        ?>

    </div><!-- end #photos -->
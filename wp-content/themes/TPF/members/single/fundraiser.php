
    <h3>Being a Fundraiser Rockstar</h3>

    <div id="fundraiser">

        <header>
            <?php
                $fundraiserGuideEditor = get_option('fundraiser_guide_editor');
                $fundraiserGuideContent = "<p>" .implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $fundraiserGuideEditor ) ). "</p>";

                echo $fundraiserGuideContent;
            ?>
        </header>

        <div id="columns">


            <section id="column-one" class="column">

                <?php
                    $fundraiserGuideColumnOne = get_option('fundraiser_guide_ColumnOne');
                    $fundraiserColumnOneContent = "<p>" .implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $fundraiserGuideColumnOne ) ). "</p>";

                    echo $fundraiserColumnOneContent;
                ?>

            </section><!-- end #column-one.column -->


            <section id="column-two" class="column">

                <?php
                    $fundraiserGuideColumnTwo = get_option('fundraiser_guide_ColumnTwo');
                    $fundraiserColumnTwoContent = "<p>" .implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $fundraiserGuideColumnTwo ) ). "</p>";

                    echo $fundraiserColumnTwoContent;
                ?>

            </section><!-- end #column-two.column -->


            <section id="column-three" class="column">

                <?php
                    $fundraiserGuideColumnThree = get_option('fundraiser_guide_ColumnThree');
                    $fundraiserColumnThreeContent = "<p>" .implode( "</p>\n\n<p>", preg_split( '/\n(?:\s*\n)+/', $fundraiserGuideColumnThree ) ). "</p>";

                    echo $fundraiserColumnThreeContent;
                ?>

            </section><!-- end #column-three.column -->


        </div><!-- end #columns -->


    </div><!-- end #fundraiser -->
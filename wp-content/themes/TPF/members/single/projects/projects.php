<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $user_info = get_userdata( $displayed_user );
?>

    <div class="item-list-tabs no-ajax" id="subnav" role="navigation">
        <ul>
            <li id="active-personal-li" class="current selected">
                <a id="active" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $user_info->user_login; ?>/?component=projects">Active</a>
            </li>

            <li id="completed-personal-li">
                <a id="completed" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $user_info->user_login; ?>/?component=completed-projects">Completed</a>
            </li>

        </ul>
    </div><!-- .item-list-tabs -->

    <h3>Projects</h3>

    <div id="projects">

        <?php

            $args = array(
                'post_type'      => 'project',
                'author'         => $displayed_user,
                'post_status'    => 'publish',
                'posts_per_page' => -1, // = all of 'em
                'meta_key'       => 'complete',
                'meta_value'     => ' '
            );

            $loop = new WP_Query( $args );
            //$loop = new WP_Query( 'author=9' );

            while ( $loop->have_posts() ) : $loop->the_post();

                echo '<article class="article project">';

                    echo '<header>';
                        echo '<h4>';
                            echo '<a href="';
                            the_permalink();
                            echo '" title="';
                            the_title_attribute();
                            echo '">';

                                the_title();

                            echo '</a>';
                        echo '</h4>';

                    echo '</header>';

                    echo '<figure>';

                        $featuredImg = wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), 'thumbnail', false, '' );

                        if( $featuredImg == null ){
                            echo '<img src="' . get_bloginfo( 'wpurl' ) . '/wp-content/themes/TPF/library/images/turkPhilFunds.jpg" />';
                        } else {
                            echo $featuredImg;
                        }

                    echo '</figure>';

                    echo '<div class="content">';

                        echo '<p>';
                            $start_values = get_post_custom_values('goal');
                            foreach ( $start_values as $key => $value ) {
                                echo ttruncat( $value, 350 );
                            }
                        echo '</p>';

                    echo '</div>';

                    echo '<footer>';

                        $start_values = get_post_custom_values('timeline_start');
                        foreach ( $start_values as $key => $value ) {

                            if( isset($value[0]) ){

                                echo '<p>';

                                echo '<span>Timeline:</span> ';
                                echo "$value ";
                                echo ' - ';

                            }

                        }


                        $end_values = get_post_custom_values('timeline_end');
                        foreach ( $end_values as $key => $value ) {

                            if( isset($value[0]) ){

                                echo "$value ";
                                echo '</p>';

                            }
                        }

                        echo '<a href="';
                        echo the_permalink();
                        echo '" class="button">Visit Project</a>';


                        global $current_user;
                        get_currentuserinfo();

                        if( $displayed_user == $current_user->ID ){

                            echo '<a href="' .get_bloginfo('wpurl'). '/wp-admin/post.php?post=' .get_the_ID(). '&action=edit" class="edit">Edit Project</a>';

                        }

                        $meta_data = get_post_meta(get_the_ID());
                        $current_total = $meta_data['current_total'][0];
                        $goal = $meta_data['financial_goal'][0];
                        if ($goal>0 && $current_total>=0) {
                            $percent_complete = ($current_total/$goal)*100;
                        }
                        

                        echo '<div class="progress">';
                        echo '<h6>' .sprintf("Financial Goal $%s", number_format($goal)). '</h6>';
                            echo '<span class="progress_bar">';
                                echo '<span class="total">' .sprintf("$%s raised", number_format($current_total)). '</span>';
                                echo '<span class="progress_raised" style="width:' .sprintf("%s%%", $percent_complete). '"></span>';
                            echo '</span>';


                        echo '</div>';


                    echo '</footer>';

                echo '</article>';

            endwhile;

        ?>

    </div><!-- end #projects -->
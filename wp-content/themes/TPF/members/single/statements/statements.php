<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $user_info = get_userdata( $displayed_user );
?>

    <h3>Statements</h3>

    <div id="statements">

            <section id="primary" class="page project-reports">

                <?php
                $args = array( 'post_type' => 'statement', 'post_status' => 'publish',);
                $loop = new WP_Query( $args );

                if( !$loop->have_posts() ){
                    echo '<div>';
                        echo 'There are currently no statements to display.';
                    echo '</div>';
                } else{
                    echo '<table class="fundowner_statements">';
                        echo '<tr>';
                            echo '<th>';
                                echo 'ID';
                            echo '</th>';
                              echo '<th>';
                                echo 'Date';
                            echo '</th>';
                            echo '<th>';
                                echo 'Title';
                            echo '</th>';
                            echo '<th>';
                                echo 'File';
                            echo '</th>';
                        echo '</tr>';
                        while ( $loop->have_posts() ) : $loop->the_post();
                            if (get_field('fundowner_statement_user') == $displayed_user) {
                                $statement_file = get_field('fundowner_statement_file');
                                echo '<tr>';
                                    echo '<td>';    
                                        echo $statement_file['id'];
                                    echo '</td>';
                                    echo '<td>';    
                                        echo get_the_date();
                                    echo '</td>';
                                  
                                    echo '<td>';    
                                        echo the_title();
                                    echo '</td>';
                                    echo '<td>';    
                                        echo '<a href="'.$statement_file['url'].'">'.$statement_file['title'].'</a>';
                                    echo '</td>';
                                 
                                echo '</td>';
                            } ;          
                        endwhile;
                        
                    echo '</table>';
                    wp_reset_query();
                }
                ?>

            </section><!-- end #primary -->

    </div><!-- end #statements -->
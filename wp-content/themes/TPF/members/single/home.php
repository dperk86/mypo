<?php get_template_parts( array( 'parts/profile/html-header', 'parts/profile/header' ) ); ?>

    <?php do_action( 'bp_before_member_home_content' ); ?>

    <header id="hero">

        <div class="wrapper">

            <div id="item-header" role="complementary">

                <?php locate_template( array( 'members/single/member-header.php' ), true ); ?>

            </div><!-- #item-header -->

        </div><!-- end .wrapper -->

    </header><!-- end #hero -->

    <div class="wrapper">

        <aside id="secondary">
            <div id="item-nav">
                <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                    <ul>

                        <?php bp_get_displayed_user_nav(); ?>

                        <?php do_action( 'bp_member_options_nav' ); ?>

                        <?php locate_template( array( 'members/single/bp-custom-nav.php'  ), true ); ?>

                    </ul>
                </div>
            </div><!-- #item-nav -->
        </aside><!-- end #secondary -->


        <section id="primary" class="page">

            <div class="content">

                <div id="item-body">

                    <?php do_action( 'bp_before_member_body' );

                        global $post;

                    if ( $_GET['component'] == 'campaigns' ){
                        locate_template( array( 'members/single/campaigns/view.php'  ), true );
                    }
                    elseif ( $_GET['component'] == 'personal-message-campaign' ){
                        locate_template( array( 'members/single/campaigns/personal.php'  ), true );
                    }
                    elseif ( $_GET['component'] == 'completed-campaigns' ){
                        locate_template( array( 'members/single/campaigns/completed-campaigns.php'  ), true );
                    }
                    elseif ( $_GET['component'] == 'projects' ){
                        locate_template( array( 'members/single/projects/projects.php'  ), true );
                    }
                    elseif ( $_GET['component'] == 'completed-projects' ){
                        locate_template( array( 'members/single/projects/completed-projects.php'  ), true );
                    }
                    elseif ( $_GET['mediapics'] == 'photos' ){
                        locate_template( array( 'members/single/photos/photos.php'  ), true );
                    }
                    elseif ( $_GET['mediavids'] == 'videos' ){
                        locate_template( array( 'members/single/videos/partners/videos.php'  ), true );
                    }
                    elseif ( $_GET['mediacampaign'] == 'videos' ){
                        locate_template( array( 'members/single/videos/campaigners/videos.php'  ), true );
                    }
                    elseif ( $_GET['statements'] == 'statements' ){
                        locate_template( array( 'members/single/statements/statements.php'  ), true );
                    }
                    elseif ( $_GET['guide'] == 'fundraiser' ){
                        locate_template( array( 'members/single/fundraiser.php'  ), true );
                    }
                    elseif ( bp_is_user_activity() || !bp_current_component() ){
                        locate_template( array( 'members/single/activity.php'  ), true );
                    }
                    elseif ( bp_is_user_blogs() ){
                        locate_template( array( 'members/single/blogs.php'     ), true );
                    }
                    elseif ( bp_is_user_friends() ){
                        locate_template( array( 'members/single/friends.php'   ), true );
                    }
                    elseif ( bp_is_user_groups() ){
                        locate_template( array( 'members/single/groups.php'    ), true );
                    }
                    elseif ( bp_is_user_messages() ){
                        locate_template( array( 'members/single/messages.php'  ), true );
                    }
                    elseif ( bp_is_user_profile() ){
                        locate_template( array( 'members/single/profile.php'   ), true );
                    }
                    elseif ( bp_is_user_forums() ){
                        locate_template( array( 'members/single/forums.php'    ), true );
                    }
                    elseif ( bp_is_user_settings() ){
                        locate_template( array( 'members/single/settings.php'  ), true );
                    }
                    // If nothing sticks, load a generic template
                    else {
                        locate_template( array( 'members/single/plugins.php'   ), true );
                    }

                    do_action( 'bp_after_member_body' ); ?>

                </div><!-- #item-body -->
            </div><!-- end .content -->
            
           

                <?php do_action( 'bp_after_member_home_content' ); ?>

        </section><!-- end #primary -->

        <?php //get_sidebar( 'buddypress' ); ?>

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

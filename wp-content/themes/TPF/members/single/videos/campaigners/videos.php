<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $user_info = get_userdata( $displayed_user );
?>

    <h3>Videos</h3>

    <div id="videos">

        <?php

            $campaigns = $wpdb->get_results( "SELECT * FROM campaigns WHERE user_id = $displayed_user AND completed = '' ORDER BY id DESC");

            if($campaigns) :

                foreach($campaigns as $campaign) :

                    $user_info     = get_userdata( $campaign->user_id );
                    $user          = $user_info->user_login;
                    $firstName     = $user_info->first_name;
                    $lastName      = $user_info->last_name;
                    $financialGoal = $campaign->amount;

                        $args = array(
                            'post_type'      => 'video',
                            'post_status'    => 'publish',
                            'posts_per_page' => -1, // = all of 'em
                            'meta_query' => array(
                                array(
                                    'key' => 'cf_campaign_id',
                                    'value' => $campaign->id
                                )
                            )
                        );

                        $loop = new WP_Query( $args );


                        if ( $loop->have_posts() ) :

                            echo '<article class="campaign">';

                                echo '<h4>';

                                    echo '<a href="';
                                        echo get_bloginfo('url');
                                        echo '/campaigns/campaign.php?pagename=campaign&action=view&id=';
                                        echo $campaign->id;
                                        echo '&slug=';
                                        echo basename($campaign->project_url);
                                    echo '" title="';
                                        echo  'Help ';
                                        if($firstName == null && $lastName== null){
                                            echo $user;
                                        } else {
                                            echo $firstName;
                                        }
                                    echo ' raise $' .$financialGoal. ' for ' .$campaign->project_title;
                                    echo '">';

                                        echo  'Help ';
                                        if($firstName == null && $lastName== null){
                                            echo $user;
                                        } else {
                                            echo $firstName;
                                        }
                                        echo ' raise $' .$financialGoal. ' for ' .$campaign->project_title;

                                    echo '</a>';

                                echo '</h4>';

                                echo '<ul>';

                                    while ( $loop->have_posts() ) : $loop->the_post();

                                        $yt_url     = get_post_meta($post->ID, 'cf_youtubeID_field', true);
                                        $host       = parse_url($yt_url,  PHP_URL_HOST);
                                        $query      = parse_url($yt_url, PHP_URL_QUERY);
                                        $path       = parse_url($yt_url,  PHP_URL_PATH);
                                        $spiltPath  = substr($path, 1);
                                        $videoID    = substr($query, 2);
                                        $splitQuery = substr($query, 2);
                                        $videoID    = str_split($splitQuery, 11);

                                        echo '<li>';

                                            if( $host == null ){

                                                $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$yt_url. '/mqdefault.jpg" width="230" height="129" />';

                                                echo '<a class="iframe" href="http://www.youtube.com/embed/' .$yt_url. '">';
                                                    echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                                    echo $youtubeThumb;
                                                echo '</a>';

                                            } elseif( $host == 'youtu.be' ){

                                                $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$spiltPath. '/mqdefault.jpg" width="230" height="129" />';

                                                echo '<a class="iframe" href="http://www.youtube.com/embed/' .$spiltPath. '">';
                                                    echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                                    echo $youtubeThumb;
                                                echo '</a>';

                                            } elseif( $host == 'www.youtube.com' ){

                                                $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$videoID[0]. '/mqdefault.jpg" width="230" height="129" />';

                                                echo '<a class="iframe" href="http://www.youtube.com/embed/' .$videoID[0]. '">';
                                                    echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                                    echo $youtubeThumb;
                                                echo '</a>';

                                            }

                                        echo '</li>';

                                    endwhile;

                                echo '</ul>';

                            echo '</article>';

                        else :

                            // No Videos
                            echo '<p class="info">This user has not added any videos to their campaign.</p>';

                        endif;

                endforeach;

            else :

            endif;

        ?>

    </div><!-- end #videos -->
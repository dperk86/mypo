<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $user_info = get_userdata( $displayed_user );
?>

    <h3>Videos</h3>

    <div id="videos">

        <?php

            $args = array(
                'post_type'      => 'project',
                'author'         => $displayed_user,
                'post_status'    => 'publish',
                'posts_per_page' => -1, // = all of 'em
                'meta_value'     => ' '
            );

            $loop = new WP_Query( $args );
            //$loop = new WP_Query( 'author=9' );

            while ( $loop->have_posts() ) : $loop->the_post();

                echo '<article class="project">';

                    echo '<h4>';

                        echo '<a href="';
                        the_permalink();
                        echo '" title="';
                        the_title_attribute();
                        echo '">';

                            echo the_title();

                        echo '</a>';

                    echo '</h4>';

                    echo '<ul>';

                        global $post;

                        $args = array(
                            'post_type' => 'video',
                            'meta_key' => 'projectid_field',
                            'meta_value' => $post->ID,
                            'post_status' => 'publish',
                            'posts_per_page' => '-1',
                            'order' => 'ASC',
                            'orderby' => 'title'
                        );

                        $videos = new WP_Query($args);

                        if (!$videos->have_posts()) {
                            echo '<p class="info">No videos.</p>';
                        } else {

                            while ($videos->have_posts()): $videos->the_post();
                                global $post;
                                $video = get_post($post->ID);
                                $youtubeID = get_post_meta($post->ID, 'cf_youtubeID_field');
                                $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$youtubeID[0]. '/mqdefault.jpg" width="320" height="180" />';

                                echo '<li>';
                                    echo '<a class="iframe" href="http://www.youtube.com/embed/' .$youtubeID[0]. '">';
                                        echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="320" height="180" />';
                                        echo $youtubeThumb;
                                    echo '</a>';
                                echo '</li>';

    //                            echo '<h6>';
    //                            echo $video->post_title;
    //                            echo '</h6>';
    //                            echo '<iframe width="420" height="315" src="http://www.youtube.com/embed/' . $youtubeID[0] . '" frameborder="0" allowfullscreen></iframe>';

                            endwhile;

                            wp_reset_query();
                        }

                    echo '</ul>';

                echo '</article>';

            endwhile;

        ?>

    </div><!-- end #photos -->
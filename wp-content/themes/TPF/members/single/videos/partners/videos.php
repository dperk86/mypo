<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;
    $user_info = get_userdata( $displayed_user );
?>

    <h3>Videos</h3>

    <div id="videos">

        <?php

            $args = array(
                'post_type'      => 'project',
                'author'         => $displayed_user,
                'post_status'    => 'publish',
                'posts_per_page' => -1, // = all of 'em
                'meta_value'     => ' '
            );

            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ) : $loop->the_post();

                echo '<article class="project">';

                    echo '<h4>';

                        echo '<a href="';
                        the_permalink();
                        echo '" title="';
                        the_title_attribute();
                        echo '">';

                            echo the_title();

                        echo '</a>';

                    echo '</h4>';

                    echo '<ul>';

                        global $post;

                        $youtubeRows = get_field('youtube_videos');
                        $vimeoRows   = get_field('vimeo_videos');


                        if($youtubeRows || $vimeoRows ){

                            if($vimeoRows){

                                foreach($vimeoRows as $vimeoRow){

                                    $url     = $vimeoRow['cf_vimeoID_field'];
                                    $host    = parse_url($url);
                                    $path    = parse_url($url,  PHP_URL_PATH);
                                    $vimeoID = substr($path, 1);

                                    $videoFeed  = file_get_contents('http://vimeo.com/api/v2/video/' .$vimeoID. '.xml');
                                    $xml        = simplexml_load_string($videoFeed);
                                    $videoTitle = $xml->video->title;
                                    $videoImg   = $xml->video->thumbnail_large;

                                    $vimeoThumb = '<img class="image" src="'.$videoImg.'" width="230" height="129" />';

                                    echo '<li>';
                                        echo '<a class="iframe" href="http://player.vimeo.com/video/' .$vimeoID. '?byline=1&portrait=0">';
                                            echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                            echo $vimeoThumb;
                                        echo '</a>';
                                    echo '</li>';

                                }

                            }

                            if($youtubeRows){

                                foreach($youtubeRows as $youtubeRow){

                                    $url        = $youtubeRow['cf_youtubeID_field'];
                                    $host       = parse_url($url,  PHP_URL_HOST);
                                    $path       = parse_url($url,  PHP_URL_PATH);
                                    $spiltPath  = substr($path, 1);
                                    $query      = parse_url($url, PHP_URL_QUERY);
                                    $splitQuery = substr($query, 2);
                                    $videoID    = str_split($splitQuery, 11);

                                    if( $host == null ){

                                        $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$url. '/mqdefault.jpg" width="230" height="129" />';

                                        echo '<li>';
                                            echo '<a class="iframe" href="http://www.youtube.com/embed/' .$url. '">';
                                                echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                                echo $youtubeThumb;
                                            echo '</a>';
                                        echo '</li>';


                                    } elseif( $host == 'youtu.be' ){

                                        $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$spiltPath. '/mqdefault.jpg" width="230" height="129" />';

                                        echo '<li>';
                                            echo '<a class="iframe" href="http://www.youtube.com/embed/' .$spiltPath. '">';
                                                echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                                echo $youtubeThumb;
                                            echo '</a>';
                                        echo '</li>';

                                    } elseif( $host == 'www.youtube.com' ){

                                        $youtubeThumb = '<img class="image" src="http://img.youtube.com/vi/' .$videoID[0]. '/mqdefault.jpg" width="230" height="129" />';

                                        echo '<li>';
                                            echo '<a class="iframe" href="http://www.youtube.com/embed/' .$videoID[0]. '">';
                                                echo '<img class="overlay" src="'.get_bloginfo('template_url').'/library/images/video_overlay.png" width="230" height="129" />';
                                                echo $youtubeThumb;
                                            echo '</a>';
                                        echo '</li>';

                                    }
                                }

                            }

                        } else {

                            echo '<p class="info">No Videos.</p>';

                        }


                    echo '</ul>';

                echo '</article>';

            endwhile;

        ?>

    </div><!-- end #photos -->
<?php get_template_parts( array( 'parts/profile/html-header', 'parts/profile/header' ) ); ?>


    <header id="hero">
        <?php do_action( 'bp_before_member_settings_template' ); ?>

        <div class="wrapper">

            <div id="item-header">

                <?php locate_template( array( 'members/single/member-header.php' ), true ); ?>

            </div><!-- #item-header -->

        </div><!-- end .wrapper -->
    </header><!-- end #hero -->


    <div class="wrapper">

        <aside id="secondary">
            <div id="item-nav">
                <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                    <ul>

                        <?php bp_get_displayed_user_nav(); ?>

                        <?php do_action( 'bp_member_options_nav' ); ?>

                        <?php locate_template( array( 'members/single/bp-custom-nav.php'  ), true ); ?>

                    </ul>
                </div>
            </div><!-- #item-nav -->
        </aside><!-- end #secondary -->


        <section id="primary" class="page">

            <div class="content">

                <div id="item-body" role="main">

                    <?php do_action( 'bp_before_member_body' ); ?>

                    <div class="item-list-tabs no-ajax" id="subnav">
                        <ul>

                            <?php bp_get_options_nav(); ?>

                            <?php do_action( 'bp_member_plugin_options_nav' ); ?>

                        </ul>
                    </div><!-- .item-list-tabs -->

                    <h3><?php _e( 'Email Notification', 'buddypress' ); ?></h3>

                    <?php do_action( 'bp_template_content' ); ?>

                    <form action="<?php echo bp_displayed_user_domain() . bp_get_settings_slug() . '/notifications'; ?>" method="post" class="standard-form" id="settings-form">
                        <p><?php _e( 'Send a notification by email when:', 'buddypress' ); ?></p>

                        <?php do_action( 'bp_notification_settings' ); ?>

                        <?php do_action( 'bp_members_notification_settings_before_submit' ); ?>

                        <div class="submit">
                            <input type="submit" name="submit" value="<?php _e( 'Save Changes', 'buddypress' ); ?>" id="submit" class="auto" />
                        </div>

                        <?php do_action( 'bp_members_notification_settings_after_submit' ); ?>

                        <?php wp_nonce_field('bp_settings_notifications'); ?>

                    </form>

                    <?php do_action( 'bp_after_member_body' ); ?>

                </div><!-- #item-body -->

            </div><!-- end .content -->

                <?php do_action( 'bp_after_member_settings_template' ); ?>

        </section><!-- end #primary -->

        <?php //get_sidebar( 'buddypress' ); ?>

    </div><!-- end .wrapper -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
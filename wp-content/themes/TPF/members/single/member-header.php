<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>

<div id="item-header-avatar">
	<a href="<?php bp_displayed_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?>

	</a>
</div><!-- #item-header-avatar -->

<?php
    global $bp;
    $displayed_user = $bp->displayed_user->id;

    $displayed_user_role = get_user_roles( $displayed_user );
?>

<?php if( $displayed_user_role[0] == 'partner' || $displayed_user_role[0] == 'trustedpartner' ) { ?>
<?php //if( user_can($displayed_user, 'edit_projects' ) ){ ?>
<div id="item-header-content" class="partner">
<?php } elseif( $displayed_user_role[0] == 'campaigner' || $displayed_user_role[0] == 'donor' || $displayed_user_role[0] == 'fundowner' ) { ?>
<?php //} elseif( user_can($displayed_user, 'start_a_campaign' ) ){ ?>
<div id="item-header-content" class="campaigner">
<?php } else { ?>
<div id="item-header-content">
<?php } ?>

	<h2>
		<a href="<?php bp_displayed_user_link(); ?>"><?php bp_displayed_user_fullname(); ?></a>
	</h2>

    <?php

        global $current_user;
        get_currentuserinfo();

        if($current_user->ID == $displayed_user){
            $gravatarURL = bp_core_fetch_avatar ( array( 'item_id' => $displayed_user, 'html' => false ) );
            $mysteryMan  = '/wp-content/plugins/buddypress/bp-core/images/mystery-man.jpg';
            $gravatar    = strstr($gravatarURL, $mysteryMan);

            if( $gravatar != null ){
                echo '<p class="info"><a href="'.$bp->displayed_user->domain.'profile/change-avatar/">Click here</a> to update your avatar!</p>';
            }
        }

    ?>

    <!-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post"> -->
    <?php if( $displayed_user_role[0] == 'partner' || $displayed_user_role[0] == 'trustedpartner' ) : ?>
    <?php //if( user_can($displayed_user, 'edit_projects' ) ): ?>

    <?php

            $user = get_user_by('id', $displayed_user);
            $userName = $user->user_login;

        ?>

    <?php endif; ?>

	<?php do_action( 'bp_before_member_header_meta' ); ?>

	<div id="item-meta">

		<?php if ( bp_is_active( 'activity' ) ) : ?>

			<div id="latest-update">

				<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>

			</div>

		<?php endif; ?>

		<div id="item-buttons">

			<?php do_action( 'bp_member_header_actions' ); ?>

		</div><!-- #item-buttons -->

		<?php
            /***
             * If you'd like to show specific profile fields here use:
             * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
             */
             do_action( 'bp_profile_header_meta' );
		 ?>

	</div><!-- #item-meta -->

</div><!-- #item-header-content -->

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>
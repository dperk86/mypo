<?php if(isset($_GET['action']) && isset($_GET['id'])) : ?>

    <?php
        $id = $_GET['id'];
        global $wpdb;
        global $bp;
        $query = $wpdb->query("DELETE FROM campaigns WHERE id = $id");
    ?>

    <?php if($query) : ?>
        <div id="message" class="updated">
            <p>Campaign deleted.</p>
        </div>
    <?php else : ?>
        <div id="message" class="error">
            <p>There was an error.</p>
        </div>
    <?php endif; ?>

<?php endif; ?>
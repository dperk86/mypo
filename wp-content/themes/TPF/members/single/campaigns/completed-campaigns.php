        <?php
            global $bp;
            $displayed_user = $bp->displayed_user->id;
            $user_info = get_userdata( $displayed_user );
        ?>


        <div class="item-list-tabs no-ajax" id="subnav" role="navigation">
            <ul>

                <li id="active-personal-li">
                    <a id="active" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $user_info->user_login; ?>/?component=campaigns">Active</a>
                </li>

                <li id="completed-personal-li" class="current selected">
                    <a id="completed" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $user_info->user_login; ?>/?component=completed-campaigns">Completed</a>
                </li>

            </ul>
        </div><!-- .item-list-tabs -->

        <h2>Campaigns</h2>

        <div id="campaigns" class="completed">


            <?php

                //Display project data from wp database - campaigns
                $campaigns = $wpdb->get_results( "SELECT * FROM campaigns WHERE user_id = $displayed_user AND completed = 'completed' ORDER BY id DESC");

                if($campaigns) :

            ?>

                <?php
                    global $current_user;
                    get_currentuserinfo();
                    $current_user = $current_user->user_login;
                ?>

                <?php foreach($campaigns as $campaign) : ?>

                    <article class="campaign">
                        <header>
                            <h4>
                                <a href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>">
                                    <?php echo $campaign->project_title; ?>
                                </a>
                            </h4>
                            <h6 class="raise-amount">I raised $<?php echo $campaign->amount; ?>.00</h6>
                        </header>

                        <p>
                            <?php echo $campaign->personal_message; ?>
                        </p>

                        <footer>
                            <a class="button" href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>">
                                Visit my campaign page &raquo;
                            </a>
                            <br/>

                            <?php if( $current_user == $campaign->user_name ) : ?>
                                <a class="delete" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $campaign->user_name; ?>/?component=campaigns&action=delete&id=<?php echo $campaign->id; ?>">
                                    Delete Campaign
                                </a>
                            <?php endif; ?>
                        </footer>
                        <?php if( $current_user == $campaign->user_name ) : ?>
                            <a class="button" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $campaign->user_name; ?>/?component=campaigns&action=reactivate-campaign&id=<?php echo $campaign->id; ?>">
                                Reactivate this campaign
                            </a>
                        <?php endif; ?>
                    </article>

                <?php endforeach; ?>


            <?php else : ?>


                <?php if(bp_is_home()) : ?>

                    <div id="message" class="info">
                        <p>You have not completed a campaign.</p>
                    </div>

                <?php else : ?>

                    <div id="message" class="info">
                        <p>This member hasn't completed a campaign.</p>
                    </div>

                <?php endif; ?>


            <?php endif; ?>

        </div><!-- end #campaigns -->

<?php

    global $wpdb;

    function username(){
        global $current_user;
        get_currentuserinfo();

        echo $current_user->user_login;
    }

    function userid(){
        global $current_user;
        get_currentuserinfo();

        echo $current_user->ID;
    }

    function campaignDesc(){
        $goal_values = get_post_custom_values('goal');
        foreach ( $goal_values as $key => $value ) {

            if( isset($value[0]) ){

                echo "$value ";
            }


        }
    }



    if(is_user_logged_in() && current_user_can( 'start_a_campaign' )) :


        $existing_project_title = $wpdb->get_results("SELECT user_id FROM campaigns WHERE project_title = 'the_title()' " );

        if( $existing_project_title ) :

?>
            <p>This project has been saved as your campaign.</p>

        <?php else : ?>

            <div id="postbox">
                <form id="new_campaign" name="new_campaign" method="post" action="<?php echo get_bloginfo( 'url' )?>/my-tpf/<?php username(); ?>/?component=personal-message-campaign">

                    <input type="hidden" name="action" value="post" />
                    <?php wp_nonce_field( 'new-campaign' ); ?>

                    <input type="text" name="userid" value="<?php userid(); ?>" id="userid" class="text hide"/>

                    <input type="text" name="username" value="<?php username(); ?>" id="username" class="text hide"/>

                    <input type="text" name="projecttitle" value="<?php the_title(); ?>" id="projecttitle" class="text hide"/>

                    <input type="text" id="projecturl" name="projecturl" value="<?php the_permalink(); ?>" class="text hide"/>

                    <input type="text" id="partner" name="partner" value="<?php echo get_the_author() ; ?>" class="text hide" />

                    <input type="text" id="partnerurl" name="partnerurl" value="<?php bloginfo( 'url' ); ?>/my-tpf/<?php the_author_meta( 'user_login' ); ?>/profile" class="text hide" />

                    <input type="hidden" id="projectid" name="projectid" value="<?php echo get_the_ID(); ?>"/>

                    <textarea name="projectcontent" id="projectcontent" rows="3" cols="60" class="hide">
                        <?php campaignDesc(); ?>
                    </textarea>

                    <button id="submit" type="submit" name="startCampaign">Start a Campaign</button>

                </form>
            </div> <!-- // postbox -->

        <?php endif; ?>


<?php endif; ?>

<?php
    if( !current_user_can( 'start_a_campaign' )) :

        function startLink(){
            echo '<a class="button" href="' . get_bloginfo( 'url' ) . '/log-in-to-start-a-campaign">';
                echo 'Start A Campaign';
            echo '</a>';

        }

        return startLink();
    endif;
?>
<?php
    // SAVE CAMPAIGN
?>

<?php if(isset($_GET['action']) && isset($_GET['id'])) : ?>

    <?php

        $id              = $_GET['id'];
        $amount          = $_POST['amount'];
        $personalMessage = $_POST['projectmessage'];
        $updateMessage   = $_POST['campaign_update'];

        global $wpdb;
        $query = $wpdb->update(
            'campaigns',
            array(
                'amount'           => $amount,
                'personal_message' => $personalMessage,
                'update_message'   => $updateMessage
            ),
            array(
                'id' => $id
            ),
            array(
                '%d',
                '%s',
                '%s'
            )
        );
    ?>

    <p>Thank you! Your changes have been saved.</p>

<?php endif; ?>
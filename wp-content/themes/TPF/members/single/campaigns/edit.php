<?php
    // EDIT CAMPAIGN
?>

<?php if(isset($_GET['action']) && isset($_GET['id'])) : ?>

    <?php
        global $wpdb;

        $id       = $_GET['id'];
        $campaign = $wpdb->get_row("SELECT * FROM campaigns WHERE id = $id");

        $title           = $campaign->project_title;
        $amount          = $campaign->amount;
        $personalMessage = $campaign->personal_message;
        $campaign_update = $campaign->update_message;

        function username(){
            global $current_user;
            get_currentuserinfo();

            echo $current_user->user_login;
        }

    ?>

    <h3>Edit Campaign | <?php echo $title; ?></h3>

    <script src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/TPF/library/js/libs/ckeditor/ckeditor.js"></script>

    <form id="edit_campaign" class="campaign_form" name="edit_campaign" method="post" action="<?php echo get_bloginfo('url') ?>/my-tpf/<?php username(); ?>/?component=campaigns&action=save-campaign&id=<?php echo $campaign->id; ?>">

        <div class="raise-amount">
            <label for="amount"><span class="heading">I want to raise: <span class="required">*</span></span></label>
            <span>$</span>
            <input type="text" name="amount" value="<?= $amount; ?>" id="amount" class="text required" required="required" />
        </div>

        <label for="projectmessage"><span class="heading">Personal Message: <span class="required">*</span></span> <span class="info">In your own words, please tell your friends why you want to raise funds for this project.</span> </label>
        <textarea name="projectmessage" id="projectmessage" class="ckeditor required" rows="10" cols="105"><?= $personalMessage; ?></textarea>

        <label for="campaign_update"><span class="heading">Campaign Updates:</span> <span class="info">Update your friends with the progress of your campaign.</span> </label>
        <textarea name="campaign_update" id="campaign_update" class="ckeditor" rows="10" cols="105"><?= $campaign_update; ?></textarea>
        <br/>
        <button id="submit" type="submit" name="editCampaign">Save</button>

    </form>



<?php endif; ?>
<?php
/**
 * Template Name: Personal Campaign Message Page
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 */
?>

<?php
    global $wpdb;

    function username(){
        global $current_user;
        get_currentuserinfo();

        echo $current_user->user_login;
    }

    $user_id          = $_POST['userid'];
    $user_name        = $_POST['username'];
    $project_title    = $_POST['projecttitle'];
    $project_url      = $_POST['projecturl'];
    $partner          = $_POST['partner'];
    $partner_url      = $_POST['partnerurl'];
    $project_id       = $_POST['projectid'];
    $project_content  = $_POST['projectcontent'];

    $query = "SELECT * FROM `campaigns` WHERE `user_id` = ".$user_id;
    $camps = $wpdb->get_results($query);

?>


    <div id="personalize">

        <h5><?php echo $project_title; ?></h5>
        <h6><?php echo $partner; ?></h6>
        <p><?php echo $project_content; ?></p>

        <script src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/TPF/library/js/libs/ckeditor/ckeditor.js"></script>

        <form id="personalize_campaign" class="campaign_form" name="personalize_campaign" method="post" action="<?php echo get_bloginfo('url') ?>/my-tpf/<?php username(); ?>/?component=campaigns">

            <ul id="errorlist">
            </ul>

            <input type="hidden" name="action" value="post" />
            <?php wp_nonce_field('save-campaign'); ?>

            <input type="text" name="userid" value="<?php echo $user_id; ?>" id="userid" class="text hide"/>

            <input type="text" name="username" value="<?php echo $user_name; ?>" id="username" class="text hide"/>

            <input type="text" name="projecttitle" value="<?php echo $project_title; ?>" id="projecttitle" class="text hide"/>

            <input type="text" id="projecturl" name="projecturl" value="<?php echo $project_url; ?>" class="text hide"/>

            <input type="text" id="partner" name="partner" value="<?php echo $partner; ?>" class="text hide" />

            <input type="text" id="partnerurl" name="partnerurl" value="<?php echo $partner_url; ?>" class="text hide" />

            <input type="hidden" id="projectid" name="projectid" value="<?php echo $project_id; ?>"/>

            <textarea name="projectcontent" id="projectcontent" rows="3" cols="60" class="hide">
                <?php echo $project_content; ?>
            </textarea>

            <div class="raise-amount">
                <label for="amount"><span class="heading">I want to raise: <span class="required">*</span></span> <span class="info">You're campaign goal must be $500 or higher.</span> </label><br/>
                <span>$</span>
                <input type="text" name="amount" value="<?= $amount; ?>" id="amount" class="text required" required="required" />
            </div>


            <label for="projectmessage"><span class="heading">Personal Message: <span class="required">*</span></span> <span class="info">In your own words, please tell your friends why you want to raise funds for this project.</span> </label>

            <textarea name="projectmessage" id="projectmessage" rows="10" cols="105" class="ckeditor required"><?= $personal_message; ?></textarea>


            <div class="dupeproject">
                <?php
                    foreach ($camps as $c){
                        if ($c->project_url == $project_url) {
                            echo 'You already have a campaign for this project!';
                        }
                    }
                ?>
            </div>

            <br/>

            <button id="submit" type="submit" name="createCampaign">Create a Campaign</button>

        </form>

    </div> <!-- // personalize -->





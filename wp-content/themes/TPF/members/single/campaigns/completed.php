<?php if(isset($_GET['action']) && isset($_GET['id'])) : ?>

    <?php
        $id = $_GET['id'];
        $completed = $_POST['completed'];
        global $wbdb;
        $query = $wpdb->update( 'campaigns', array( 'completed' => 'completed' ), array( 'id' => $id ), $format = '%s' );
    ?>

    <?php if($query) : ?>
        <div id="message" class="updated">
            <p>Campaign is completed.</p>
        </div>
    <?php else : ?>
        <div id="message" class="error">
            <p>There was an error.</p>
        </div>
    <?php endif; ?>

<?php endif; ?>
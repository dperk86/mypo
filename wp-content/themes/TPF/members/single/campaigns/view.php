<?php

    if( !$_GET['action'] ){

        // Save project data to wp database - campaigns

        if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'post' ) {

            check_admin_referer( 'save-campaign' );

            $user_id          = $_POST['userid'];
            $user_name        = $_POST['username'];
            $amount           = $_POST['amount'];
            $personal_message = stripslashes($_POST['projectmessage']);
            $project_title    = stripslashes($_POST['projecttitle']);
            $project_url      = $_POST['projecturl'];
            $partner          = $_POST['partner'];
            $partner_url      = $_POST['partnerurl'];
            $project_id       = $_POST['projectid'];
            $project_content  = stripslashes($_POST['projectcontent']);

            global $wpdb;

            //Check if the project already exists in the campaigns database table
            $existing_project_title = $wpdb->get_var("SELECT ID FROM campaigns WHERE user_id = '$user_id' AND project_title = '$project_title' AND project_url = '$project_url' AND project_id = '$project_id'" );

            // Get Current User ID
            if($existing_project_title){
                // Write this message if project already exists in the database
                echo 'This project has already been saved as your campaign';
            }
            else{
                // add a new campaign if the project doesn't exists in the database;
                $query = "INSERT INTO campaigns (user_id, user_name, amount, personal_message, project_title, project_url, partner, partner_url, project_content, project_id) VALUES (%d, %s, %d, %s, %s, %s, %s, %s, %s, %d)";
                $wpdb->query($wpdb->prepare($query, $user_id, $user_name, $amount, $personal_message, $project_title, $project_url, $partner, $partner_url, $project_content, $project_id));
            }

            wp_redirect( get_bloginfo( 'url' ) . '/my-tpf/' .$user_name. '/?component=campaigns' );
            exit;

        }


?>

        <?php
            global $bp;
            $displayed_user = $bp->displayed_user->id;
            $user_info = get_userdata( $displayed_user );
        ?>

        <div class="item-list-tabs no-ajax" id="subnav" role="navigation">
            <ul>
                <li id="active-personal-li" class="current selected">
                    <a id="active" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $user_info->user_login; ?>/?component=campaigns">Active</a>
                </li>

                <li id="completed-personal-li">
                    <a id="completed" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $user_info->user_login; ?>/?component=completed-campaigns">Completed</a>
                </li>

            </ul>
        </div><!-- .item-list-tabs -->

        <h3>Campaigns</h3>

        <div id="campaigns">


            <?php

                //Display project data from wp database - campaigns
                $campaigns = $wpdb->get_results( "SELECT * FROM campaigns WHERE user_id = $displayed_user AND completed = '' ORDER BY id DESC");

                if($campaigns) :

            ?>

                <?php
                    global $current_user;
                    get_currentuserinfo();
                    $current_user = $current_user->user_login;
                ?>

                <?php foreach($campaigns as $campaign) : ?>

                    <article class="campaign">
                        <header>
                            <h4>
                                <a class="title" href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>" title="<?php echo $campaign->project_title; ?>">
                                    <?php echo $campaign->project_title; ?>
                                </a>
                            </h4>
                        </header>

                        <div class="content">

                            <p>
                                <?php echo $campaign->personal_message; ?>
                            </p>

                            <a class="button" href="<?php echo get_bloginfo('url'); ?>/campaigns/campaign/?action=view&id=<?php echo $campaign->id; ?>&slug=<?php echo basename($campaign->project_url); ?>">
                                Visit my campaign page &raquo;
                            </a>

                            <?php if( $current_user == $campaign->user_name ) : ?>
                                <section class="add-video-button">
                                    <a class="button" href="<?php echo get_bloginfo('url'); ?>/new-video?&campaign_id=<?php echo $campaign->id; ?>">Add A Video</a>
                                </section>
                                <a href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $campaign->user_name; ?>/?component=campaigns&action=edit-campaign&id=<?php echo $campaign->id; ?>" class="button">Edit Campaign</a>
                            <?php endif; ?>

                        </div><!-- end .content -->

                        <footer>

                            <div class="info">

                                <?php

                                    $financialGoal = $campaign->amount;
                                    // This is for the current Total of donations. This will be updated by paypal.
                                    $currentTotal  = $campaign->current_total;

                                ?>

                                <?php 
                                if ($financialGoal>0 && $currentTotal>=0) {
                                    $percent_complete = ($currentTotal/$financialGoal)*100; 
                                }
                                ?>

                                <div class="progress">
                                    <h6 class="raise-amount">My Campaign Goal: $<?php echo number_format($financialGoal); ?></h6>
                                    <span id="progress_bar">
                                        <?php if( $currentTotal == NULL ){ ?>
                                            <span id="total"><?= sprintf("$0 raised", $currentTotal); ?></span>
                                        <?php } else if( $currentTotal > 0 ) { ?>
                                            <span id="total"><?= sprintf("$%s raised", number_format($currentTotal)); ?></span>
                                        <?php } ?>
                                        <span id="progress_raised" style="width:<?= sprintf("%s%%", $percent_complete) ?>"></span>
                                    </span>

                                    <?php if( $current_user == $campaign->user_name && $currentTotal == 0 ) : ?>

                                        <form id="donate-campaign-<?php echo $campaign->id; ?>" class="donate-campaign">
                                            <?php $params = array ('id' => $campaign->id, 'campaign' => 'true' ); ?>
                                            <a href="<?php echo add_query_arg( $params , get_permalink( get_page_by_title( 'Donations' ) )); ?>">
                                                <input type="submit" value="<?php _e('Add To Cart', 'wpsc'); ?>" name="Buy" class="hidebuybutton" id="product_<?php echo wpsc_the_product_id(); ?>" value="Please make the first donation to your campaign and show people your dedication."/>
                                            </a>
                                        </form>

                                    <?php endif; ?>

                                </div><!-- end .progress -->

                            </div><!-- end .info -->


                            <?php if( $current_user == $campaign->user_name ) : ?>

                                <?php if( $currentTotal == 0 ) : ?>

                                    <div class="delete-campaign">
                                        <p>
                                            Are you sure you want to delete this campaign? You can't delete this campaign once it has begun raising money.<br/>

                                            <a class="delete" href="<?php echo get_bloginfo( 'url' ); ?>/my-tpf/<?php echo $campaign->user_name; ?>/?component=campaigns&action=delete&id=<?php echo $campaign->id; ?>">
                                                Delete Campaign
                                            </a>
                                        </p>
                                    </div><!-- end .delete-campaign -->

                                <?php endif; ?>

                            <?php endif; ?>

                        </footer>

                        <?php
                            if( $current_user == $campaign->user_name ) :
                                echo '<form id="complete-campaigns-' .$campaign->id. '" class="complete-campaign" name="complete-campaign" method="post" action="' .get_bloginfo( 'url' ). '/my-tpf/' .$campaign->user_name .'/?component=campaigns&action=completed-campaign&id=' .$campaign->id.'">';
                                    echo '<input type="checkbox" name="complete-campaign" value="completed"> ';
                                    echo '<label for="complete-campaign">If you have reached your campaign goal, mark this campaign complete</label>';
                                    echo '<button id="submit" type="submit" name="save">Save</button>';
                                echo '</form>';
                            endif;
                        ?>
                    </article>

                <?php endforeach; ?>


            <?php else : ?>


                <?php if(bp_is_home()) : ?>

                    <div id="message" class="info">
                        <p>You don't have an active campaign at the moment.</p>
                    </div>

                <?php else : ?>

                    <div id="message" class="info">
                        <p>This member doesn't have an active campaign at the moment.</p>
                    </div>

                <?php endif; ?>


            <?php endif; ?>

        </div><!-- end #campaigns -->

<?php

    }
    elseif ( $_GET['action'] == 'view' ){
        locate_template( array( 'campaign.php'  ), true );
    }
    elseif ( $_GET['action'] == 'delete' ) {
        locate_template( array( 'members/single/campaigns/delete.php'  ), true );
    }
    elseif ( $_GET['action'] == 'completed-campaign' ){
        locate_template( array( 'members/single/campaigns/completed.php'  ), true );
    }
    elseif ( $_GET['action'] == 'reactivate-campaign' ) {
        locate_template( array( 'members/single/campaigns/reactivate.php' ), true );
    }
    elseif ( $_GET['action'] == 'edit-campaign' ){
        locate_template( array( 'members/single/campaigns/edit.php' ), true );
    }
    elseif ( $_GET['action'] == 'save-campaign' ){
        locate_template( array( 'members/single/campaigns/save.php' ), true );
    }

?>
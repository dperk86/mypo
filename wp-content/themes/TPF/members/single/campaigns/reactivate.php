<?php if(isset($_GET['action']) && isset($_GET['id'])) : ?>

    <?php
        $id = $_GET['id'];
        $completed = $_POST['completed'];
        global $wpdb;
        $query = $wpdb->update( 'campaigns', array( 'completed' => '' ), array( 'id' => $id ), $format = '%s' );
    ?>

    <?php if($query) : ?>
        <div id="message" class="updated">
            <p>This campaign is reactivated.</p>
        </div>
    <?php else : ?>
        <div id="message" class="error">
            <p>There was an error.</p>
        </div>
    <?php endif; ?>

<?php endif; ?>
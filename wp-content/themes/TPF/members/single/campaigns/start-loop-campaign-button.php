<?php

    global $wpdb;

    global $current_user;
    get_currentuserinfo();

    global $post;
    $meta_data = get_post_meta($post->ID);

    if(is_user_logged_in() && current_user_can( 'start_a_campaign' )) :


        $existing_project_title = $wpdb->get_results("SELECT user_id FROM campaigns WHERE project_title = 'the_title()' " );

        if( $existing_project_title ) :

?>
            <p>This project has been saved as your campaign.</p>

        <?php else : ?>

            <div id="postbox">

                <form id="new_campaign-<?php echo $post->ID ?>" name="new_campaign" method="post" action="<?php echo get_bloginfo( 'url' )?>/my-tpf/<?php echo $current_user->user_login; ?>/?component=personal-message-campaign">

                    <input type="hidden" name="action" value="post" />
                    <?php wp_nonce_field( 'new-campaign' ); ?>

                    <input type="text" name="userid" value="<?php echo $current_user->ID; ?>" id="userid-<?php echo $post->ID ?>" class="text hide"/>

                    <input type="text" name="username" value="<?php echo $current_user->user_login; ?>" id="username-<?php echo $post->ID ?>" class="text hide"/>

                    <input type="text" name="projecttitle" value="<?php the_title(); ?>" id="projecttitle-<?php echo $post->ID ?>" class="text hide"/>

                    <input type="text" id="projecturl-<?php echo $post->ID ?>" name="projecturl" value="<?php esc_url( the_permalink() ); ?>" class="text hide"/>

                    <input type="text" id="partner-<?php echo $post->ID ?>" name="partner" value="<?php echo the_author_meta( 'display_name', $post->post_author ); ?>" class="text hide" />

                    <input type="text" id="partnerurl-<?php echo $post->ID ?>" name="partnerurl" value="<?php bloginfo( 'url' ); ?>/my-tpf/<?php echo the_author_meta( 'user_login', $post->post_author ); ?>/profile" class="text hide" />

                    <input type="hidden" id="projectid-<?php echo $post->ID ?>" name="projectid" value="<?php echo $post->ID ?>"/>

                    <textarea name="projectcontent" id="projectcontent-<?php echo $post->ID ?>" rows="3" cols="60" class="hide"><?php echo $meta_data['goal'][0]; ?></textarea>

                    <button id="submit" type="submit" name="startCampaign">Start a Campaign</button>

                </form>
            </div> <!-- // postbox -->

        <?php endif; ?>


<?php endif; ?>

<?php
    if( !current_user_can( 'start_a_campaign' )) :

        echo '<a class="button" href="' . get_bloginfo( 'url' ) . '/log-in-to-start-a-campaign">';
            echo 'Start A Campaign';
        echo '</a>';


    endif;
?>